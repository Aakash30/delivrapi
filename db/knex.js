const development = require('./../config/index.js').development;
const production = require('./../config/index.js').production;
module.exports = {
  local: {
    client: 'pg',
    useNullAsDefault: true,
    migrations: {
      directory: './../src/migrations'
    },
    connection: {
      host: '127.0.0.1',
      user: 'postgres',
      password: 'toor',
      database: 'delivr'
    }
  },

  binary_local: {
    client: 'pg',
    useNullAsDefault: true,
    migrations: {
      directory: './../src/migrations'
    },
    connection: {
      host: '127.0.0.1',
      user: 'hp',
      password: '1234',
      database: 'delivr'
    }
  },

  development: {
    client: 'pg',
    useNullAsDefault: true,
    migrations: {
      directory: './../src/migrations'
    },
    seeds: {
      directory: './../src/seeds'
    },
    connection: {
      host: development.host,
      user: development.username,
      password: development.password,
      database: development.database
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      host: production.host,
      user: production.username,
      password: production.password,
      database: production.database
    },
    pool: {
      min: 2,
      max: 10
    }
  }
};