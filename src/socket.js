const {
    io
} = require('./globals');

const User = require('./models/Users');
const Notification = require('./models/NotificationRecord');
const TrackDriver = require('./models/TrackDriver');

io.on('connection', (socket) => {

    console.log('---------connected-------------');

    // Register "join" events, requested by a connected client
    socket.on("track_driver", async function (driver_tracking) {

        try {
            let token = driver_tracking.token ? driver_tracking.token.replace("Bearer ", "") : "";
            // join channel provided by client
            // console.log(driver_tracking);
            // console.log(typeof driver_tracking);
            // console.log(driver_tracking.device_type);
            // console.log(driver_tracking.device_type.toUpperCase());
            let userData = await User.query().select("user_id", "business_id", "business_branch_id").where("token", token).where("device_type", driver_tracking.device_type.toUpperCase()).first();
            if (userData) {
                let oldTrack = await TrackDriver.query().select("track_id").where("driver_id", userData.user_id).first();

                let latitude = driver_tracking.latitude;
                let longitude = driver_tracking.longitude;

                let trackingData = {
                    driver_latitude: latitude,
                    driver_longitude: longitude,
                    driver_id: userData.user_id,
                    business_id: userData.business_id,
                    business_branch_id: userData.business_branch_id,
                    last_updated_at: new Date().toISOString()
                }
                if (oldTrack) {
                    trackingData.track_id = oldTrack.track_id;
                }

                let updateTrack = await TrackDriver.query().upsertGraph(trackingData);
                if (updateTrack) {

                }
                socket.emit("welcome", "location updated on server for driver.")
            } else {
                console.log("no user")
            }
        } catch (error) {
            console.log(error.message)
        }

    });

    if (socket.handshake.query.token != "undefined") {
        //console.log(socket.handshake.query.token)
        socket.join(socket.handshake.query.token);
    }

    socket.on("checkNotification", async function (data) {
        // console.log("A")
        let getToken = JSON.parse(data);


        let token = getToken.token.replace("Bearer ", "");

        let userdata = await User.query().select("user_id", "has_new_alerts").where("token", token).first();
        //console.log(userdata)
        let notificationList = await Notification.query()
            .select("notification_id", "title", "body", "isRead")
            .where("receiver_id", userdata.user_id)
            .orderBy("created_at", "DESC")
            .offset(0)
            .limit(3);
        io.to(token).emit("message", {
            notification: notificationList,
            notificationDot: userdata.has_new_alerts
        });
    });
    socket.on('close', function () {
        console.log('user disconnected');
    });


});