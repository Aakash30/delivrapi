const cron = require("node-cron");
const moment = require('moment');
const notification = require('./notification').sendNotificationToUser;
const updateNotificationRecord = require('./controllers/NotificationController');
const Users = require('./models/Users');
const batch = require('./models/Batch');

cron.schedule("* * * * *", function () {
    console.log("running at 11:00 am daily");
    fetchExpiringBatches();
    fetchDuePayment();
});
const fetchExpiringBatches = async () => {
    let batchExpired = [];
    batchExpired = await batch.query().select("id", "business_id", "assigned_to_business_to", "slug").mergeNaiveEager("[businessUser, business]").modifyEager("business", builder => {
        builder.select("business_name");
    }).modifyEager("businessUser", builder => {
        builder.select("user_id", "device_token", "token").where("user_type", "vendor").first();
    }).where((builder) => {
        builder.where("assigned_to_business_to", "<=", moment("000000", "HH:mm:ss")).orWhere("assigned_to_business_to", "=", moment("000000", "HH:mm:ss").add(1, 'days')).orWhere("assigned_to_business_to", "=", moment("000000", "HH:mm:ss").add(7, 'days')).orWhere("assigned_to_business_to", "=", moment("000000", "HH:mm:ss").add(3, 'days'));
    }).where("is_renew", false).runAfter((result, builder) => {
        //console.log(builder.toSql());
        //console.log(JSON.stringify(result))
        return result;
    });
    if (batchExpired.length > 0) {

        let notificationMessage = [];
        let token = [];

        let fetchAdmin = await Users.query().select("user_id", "device_token", "token").where("user_id", 1).first();

        superadminId = fetchAdmin.user_id;
        superadminDevice = fetchAdmin.device_token;
        superadminToken = fetchAdmin.token;

        batchExpired.forEach((batchData) => {

            let getMessage = Object.assign({}, global.NOTIFICATION_PAYLOAD.BATCH_EXPIRED);

            let getSAMessage = Object.assign({}, global.NOTIFICATION_PAYLOAD.BATCH_EXPIRED_RESPONSE);

            if (batchData.assigned_to_business_to == moment("000000", "HH:mm:ss")) {
                getMessage.body = getMessage.body.replace("[batchId]", batchData.slug).replace("[DATETIME]", "is expiring  today");
                getSAMessage.body = getSAMessage.body.replace("[batchId]", batchData.slug).replace("[DATETIME]", "is expiring  today").replace("[business]", batchData.business.business_name);

            } else if (batchData.assigned_to_business_to <= moment("000000", "HH:mm:ss")) {
                getMessage.body = getMessage.body.replace("[batchId]", batchData.slug).replace("[DATETIME]", "has been expired  on " + moment(batchData.assigned_to_business_to).format('MMM Do YYYY'));

                getSAMessage.body = getSAMessage.body.replace("[batchId]", batchData.slug).replace("[DATETIME]", "has been expired  on " + moment(batchData.assigned_to_business_to).format('MMM Do YYYY')).replace("[business]", batchData.business.business_name);
            } else {
                console.log("compare", batchData.assigned_to_business_to == moment("000000", "HH:mm:ss").add(1, 'day'));
                console.log("today", moment("000000", "HH:mm:ss").add(1, 'day'));
                console.log("batch", batchData.assigned_to_business_to);

                if (batchData.assigned_to_business_to == moment("000000", "HH:mm:ss").add(1, 'day')) {
                    getMessage.body = getMessage.body.replace("[batchId]", batchData.slug).replace("[DATETIME]", "is expiring tomorrow");
                    getSAMessage.body = getSAMessage.body.replace("[batchId]", batchData.slug).replace("[DATETIME]", "is expiring tomorrow").replace("[business]", batchData.business.business_name);

                } else if (batchData.assigned_to_business_to == moment("000000", "HH:mm:ss").add(7, 'days')) {
                    getMessage.body = getMessage.body.replace("[batchId]", batchData.slug).replace("[DATETIME]", "is expiring on" + moment(batchData.assigned_to_business_to).format('MMM Do YYYY'));
                    getSAMessage.body = getSAMessage.body.replace("[batchId]", batchData.slug).replace("[DATETIME]", "is expiring on" + moment(batchData.assigned_to_business_to).format('MMM Do YYYY')).replace("[business]", batchData.business.business_name);
                } else if ("assigned_to_business_to", "=", moment("000000", "HH:mm:ss").add(3, 'days')) {
                    getMessage.body = getMessage.body.replace("[batchId]", batchData.slug).replace("[DATETIME]", "is expiring in 3 days");
                    getSAMessage.body = getSAMessage.body.replace("[batchId]", batchData.slug).replace("[DATETIME]", "is expiring in 3 days").replace("[business]", batchData.business.business_name);
                }
            }
            let encodedId = new Buffer(batchData.id.toString()).toString("base64");

            notificationMessage.push({
                device_type: 'WEB',
                time: moment(),
                title: getMessage.title,
                body: getMessage.body,
                receiver_id: batchData.businessUser[0].user_id,
                url: "business/vehicle-lease-plan/details/" + encodedId,
                notification_type: "BATCH_EXPIRED",
                unique_id: batchData.id,
                sender_id: superadminId
            });

            let businessId = new Buffer(batchData.business_id.toString()).toString("base64");

            notificationMessage.push({
                device_type: 'WEB',
                time: moment(),
                title: getMessage.title,
                body: getMessage.body,
                receiver_id: superadminId,
                url: "admin/vehicle-lease-plan/details/" + businessId,
                notification_type: "BATCH_EXPIRED_RESPONSE",
                unique_id: batchData.business_id,
                sender_id: superadminId
            });

            if (batchData.businessUser[0].device_token != "" && batchData.businessUser[0].token != "") {
                token.push(batchData.businessUser[0].token);
                // notification(batchData.businessUser.device_token, getMessage);
            }

            if (superadminDevice != "" && superadminToken != "") {
                token.push(superadminToken);
                // notification(superadminDevice, getSAMessage);
            }
            //console.log(getSAMessage)
        });
        if (notificationMessage.length > 0) {
            // console.log(notificationMessage)
            updateNotificationRecord.updateNotification(notificationMessage, token);
        }
    }
};

const fetchDuePayment = async () => {
    let paymentDue = [];
    //  paymentDue;
    try {

        paymentDue = await batch.query().select("id", "business_id", "assigned_to_business_to", "slug", batch.knex().raw("updated_at + INTERVAL '1 month' AS inst1Date")).mergeNaiveEager("[businessUser, business]").modifyEager("business", builder => {
            builder.select("business_name");
        }).modifyEager("businessUser", builder => {
            builder.select("user_id", "device_token", "token").where("user_type", "vendor").first();
        }).where((builder) => {
            builder.where("most_recent_installment", 0).orWhere((builder) => {
                builder.where("installment_type", 1).whereRaw('\"updated_at\" + INTERVAL \'1 month\' = ?', [moment("000000", "HH:mm:ss")]);
            }).orWhere((builder) => {
                builder.where("installment_type", 2).whereRaw('\"updated_at\" + INTERVAL \'3 month\' = ?', [moment("000000", "HH:mm:ss")]);
            }).orWhere((builder) => {
                builder.where("installment_type", 3).whereRaw('\"updated_at\" + INTERVAL \'6 month\' = ?', [moment("000000", "HH:mm:ss")]);
            });
        }).runAfter((result, builder) => {
            //console.log(builder.toSql());
            //console.log(JSON.stringify(result))
            return result;
        });
    } catch (error) {
        //console.log(error)
    }
    //console.log(paymentDue)

}