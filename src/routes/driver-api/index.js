const DriverOrderController = require('./../../controllers/index').DriverOrderController;
const SOSController = require('./../../controllers/index').SOSController;
const driverServiceController = require('./../../controllers/index').DriverServiceController;


const driverProfile = require('./../../controllers/index').DriverProfileController;

const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');
const aws = require('./../../controllers/aws/aws');
require('./../../middlewares/passport')(passport);


/** orders */
router.get('/fetchOrdersForDriver/', [usercheck.checkUser(), roleCheck.driverAccess], DriverOrderController.fetchOrdersForDriver); //, roleCheck.checkRole('driver-management')
router.get('/fetchDriverOrderDetails/:orderId', [usercheck.checkUser(), roleCheck.driverAccess], DriverOrderController.viewOrderDetail);
router.post('/actionOnOrder', [usercheck.checkUser(), roleCheck.driverAccess], DriverOrderController.actionOnOrder);
router.post('/verifyUserOrder', [usercheck.checkUser(), roleCheck.driverAccess], DriverOrderController.verifyUserOrder);
router.post('/deliveredAndSendFeedback', [usercheck.checkUser(), roleCheck.driverAccess, aws.upload.single("signature")], DriverOrderController.orderDelivered);
router.get('/fetchDriverOrderHistory', [usercheck.checkUser(), roleCheck.driverAccess], DriverOrderController.orderHistory);


/** Profile */
router.get('/fetchMyProfile', [usercheck.checkUser(), roleCheck.driverAccess], driverProfile.fetchMyDriverProfile);

router.post('/updateMyAvailability', [usercheck.checkUser(), roleCheck.driverAccess], driverProfile.updateMyAvailability);

router.post('/updatePassword', [usercheck.checkUser()], driverProfile.updatePassword);
router.post('/updateMyProfile', [usercheck.checkUser(), aws.upload.single("profile")], driverProfile.updateMyProfile);
/** SOS */

router.get('/fetchSOSContactList', [usercheck.checkUser(), roleCheck.driverAccess], SOSController.fetchSOSList);
router.post('/addSOSContact', [usercheck.checkUser(), roleCheck.driverAccess], SOSController.addSOSContact);
router.get('/setDefaultContact/:id', [usercheck.checkUser(), roleCheck.driverAccess], SOSController.setDefaultContact);
router.get('/removeContact/:id', [usercheck.checkUser(), roleCheck.driverAccess], SOSController.removeContact);
router.get('/sendSOS', [usercheck.checkUser(), roleCheck.driverAccess], SOSController.sendSOSToBusiness);

/** Service Request */

router.get('/fetchDataForServiceRequest', [usercheck.checkUser(), roleCheck.driverAccess], driverServiceController.fetchDataForServiceRequest);
router.get('/filterServicesForDriver/:id', [usercheck.checkUser(), roleCheck.driverAccess], driverServiceController.filterServicesBasedOnCategory);

router.post('/requestForRepair', [usercheck.checkUser(), roleCheck.driverAccess], driverServiceController.requestForRepair);
router.get('/fetchMyRepairRequest', [usercheck.checkUser(), roleCheck.driverAccess], driverServiceController.fetchHistoryRepairRequest);
router.get('/repairRequestDetails/:id', [usercheck.checkUser(), roleCheck.driverAccess], driverServiceController.repairRequestDetails);


module.exports = router;