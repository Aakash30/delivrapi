/**load the related controller */
const countryCitySettingController = require('./../../controllers/index').CountryCitySettingController;
const serviceManagement = require('./../../controllers/index').ServiceManagement;
const brandModelController = require('./../../controllers/index').BrandModelController;

/** load related checks and router */
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');
require('./../../middlewares/passport')(passport);

router.get('/countries', [usercheck.checkUser(), roleCheck.adminAccess], countryCitySettingController.fetchCountry);
router.post('/addUpdateCountry', [usercheck.checkUser(), roleCheck.adminAccess], countryCitySettingController.addUpdateCountry);
router.post('/activeInActiveCountry/:countryId', [usercheck.checkUser(), roleCheck.adminAccess], countryCitySettingController.activeInActiveCountry);
router.get('/getCountryDetails/:countryId', [usercheck.checkUser(), roleCheck.adminAccess], countryCitySettingController.getCountryDetails);
router.get('/cities/:countryId', [usercheck.checkUser(), roleCheck.adminAccess], countryCitySettingController.fetchCity);
router.get('/getCityDetails/:cityId', [usercheck.checkUser(), roleCheck.adminAccess], countryCitySettingController.getCityDetails);
router.post('/addUpdateCity', [usercheck.checkUser(), roleCheck.adminAccess], countryCitySettingController.addUpdateCity);
router.post('/addUpdateCity/:cityId', [usercheck.checkUser(), roleCheck.adminAccess], countryCitySettingController.addUpdateCity);
router.post('/activeInActiveCity/:cityId', [usercheck.checkUser(), roleCheck.adminAccess], countryCitySettingController.activeInActiveCity);


/***services */

router.get('/fetchServiceCategoryList', [usercheck.checkUser(), roleCheck.adminAccess], serviceManagement.fetchServiceCategoryForAdmin);
router.post('/addUpdateServiceCategory', [usercheck.checkUser(), roleCheck.adminAccess], serviceManagement.addUpdateServiceCategory);
router.post('/updateServiceCategoryStatus', [usercheck.checkUser(), roleCheck.adminAccess],
    serviceManagement.updateStatusForServiceCategory);

router.get('/fetchServices', [usercheck.checkUser(), roleCheck.adminAccess], serviceManagement.fetchServicesForAdmin);
router.get('/fetchDataForServiceForm', [usercheck.checkUser(), roleCheck.adminAccess], serviceManagement.fetchDataForServiceForm);
router.get('/fetchDataForServiceForm/:id', [usercheck.checkUser(), roleCheck.adminAccess], serviceManagement.fetchDataForServiceForm);
router.post('/addUpdateServices', [usercheck.checkUser(), roleCheck.adminAccess], serviceManagement.addUpdateServices);

/*** brands model*/
router.get('/fetchBrandList', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('brand-model-setting')], brandModelController.fetchBrandListForAll);
router.post('/addUpdateBrands', [usercheck.checkUser(), roleCheck.adminAccess], brandModelController.addUpdateBrand);
router.get('/fetchBrandDetails/:id', [usercheck.checkUser(), roleCheck.adminAccess], brandModelController.fetchBrandDetails);
router.post('/addUpdateModel', [usercheck.checkUser(), roleCheck.adminAccess], brandModelController.addUpdateModel);

module.exports = router;