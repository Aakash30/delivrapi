const OrderController = require('./../../controllers/index').OrderController;
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');
require('./../../middlewares/passport')(passport);

router.get('/orderList/', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('order-management')], OrderController.fetchOrderList); //
router.post('/addUpdateOrderData/', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('order-management')], OrderController.addUpdateOrderData);
router.post('/addUpdateOrderData/:orderId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('order-management')], OrderController.addUpdateOrderData);
router.get('/fetchSingleOrder/:orderId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('order-management')], OrderController.fetchSingleOrderDetails);
router.get('/fetchDriverList/', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('order-management')], OrderController.fetchDriverList);
router.post('/assignOrderToDriver', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('order-management')], OrderController.assignOrderToDriver);
router.delete('/deleteData/:orderId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('order-management')], OrderController.deleteData);

router.get('/fetchOrderFormData', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('order-management')], OrderController.fetchDataForAddEditOrders);
router.get('/fetchOrderFormData/:orderId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('order-management')], OrderController.fetchDataForAddEditOrders);

router.get('/fetchDriversToAssignOrder', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('order-management')], OrderController.fetchAvailableDriverForOrder);

module.exports = router;