const BusinessController = require('./../../controllers').BusinessController;
const Fine = require('./../../controllers/driverModule/FineController');
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');

require('./../../middlewares/passport')(passport);

const aws = require('../../controllers/aws/aws');

/** Routes for Business Type Module */

router.get('/businessTypes', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.fetchBusinessType);
router.post('/addUpdateBusinessTypes', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.addUpdateBusinessType);
router.post('/addUpdateBusinessTypes/:businessTypeId', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.addUpdateBusinessType);
router.get('/fetchSingleBusinessType/:businessTypeId', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.fetchSingleBusinessType);
router.post('/updateBusinnessTypeStatus/:businessTypeId', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.updateBusinessTypeStatus);

/** Routes for Business Module */
router.get('/businessList', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.fetchBusinessUsers);
router.post('/addUpdateBusinessUser', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.addUpdateBusiness);
router.post('/blockOrActivateBusiness/:businessId', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.blockOrActivatebusiness);
router.get('/fetchSingleBusinessUser/:businessId', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.fetchSingleBusinessUser);
router.get('/fetchDataForAddEditBusiness', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.fetchDataForAddEditBusiness);
router.get('/fetchDataForAddEditBusiness/:businessId', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.fetchDataForAddEditBusiness);

router.post('/assignVehicleToVendor/:businessId', [usercheck.checkUser(), roleCheck.adminAccess], BusinessController.assignVehicleToVendor);

router.get('/fetchFuelUpdatesForBusiness', [usercheck.checkUser(), roleCheck.vendorAccess], Fine.fetchFuelUpdates);

router.get('/fetchDriverPhotos', [usercheck.checkUser(), roleCheck.vendorAccess], Fine.fetchDriverPhotos);

module.exports = router;