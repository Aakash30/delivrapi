const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');
require('./../../middlewares/passport')(passport);

// load the dependent controller
const AuthController = require('./../../controllers/index').AuthController;

router.post('/login', AuthController.Login);
router.get('/logout', [usercheck.checkUser()], AuthController.Logout);
router.post('/requestForOTP', AuthController.RequestForForgotPassword);
router.post('/verify/', AuthController.VerifyAndUpdatePassword);
router.post('/verifyUserLink/', AuthController.verifyUserLink);
router.post('/activateUser/:code', AuthController.activateBusinessUsers);
router.post('/updateDeviceToken', [usercheck.checkUser()], AuthController.updateDeviceToken);
router.get('/fetchCountryCode', AuthController.fetchCountryCode);
router.post('/resendOTPOrLink', AuthController.resendOTPOrLink);
router.post('/is_driver_verified', AuthController.is_driver_verified);

module.exports = router;