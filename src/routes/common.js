const Aws = require('./../controllers/index').AWSController;
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const usercheck = require('./../middlewares/usercheck');


const NotificationController = require('./../controllers/index').NotificationController;
const Dashboard = require('../controllers').Dashboard;

router.post('/upload/file', Aws.UploadImage);
router.post('/upload/files', Aws.UploadImages);

router.get('/fetchNotifications', [usercheck.checkUser()], NotificationController.fetchNotifications);
router.get('/updateNotifyAlert', [usercheck.checkUser()], NotificationController.updateCheckNoticeAlert);

router.get('/fetchAdminDashboard', [usercheck.checkUser()], Dashboard.fetchAdminDashboard);
router.get('/fetchMyDashboard', [usercheck.checkUser()], Dashboard.fetchBusinessDashboard);
module.exports = router;