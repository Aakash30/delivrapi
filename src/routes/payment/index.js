const PaymentController = require('./../../controllers/index').PaymentController;
const Contact = require('./../../controllers/index').BusinessController;
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');
require('./../../middlewares/passport')(passport);

/** Routes for Vehicle Module Super admin */

router.get('/payment', PaymentController.fetchPayments);
router.post('/payment', passport.authenticate('jwt', {
    session: false
}), PaymentController.updatePayment);
router.put('/payment', PaymentController.updatePayment);

//contact forms --> move to a better route
router.post('/addContactRequest', PaymentController.addContactForm);
router.get('/fetchContactRequestsForAdmin', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('module-request-management')], PaymentController.fetchContactRequestsForAdmin);

module.exports = router;