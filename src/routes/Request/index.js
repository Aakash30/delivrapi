const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');
require('./../../middlewares/passport')(passport);

const RequestRepair = require('./../../controllers/index').RequestForRepair;
const VehicleRequest = require('./../../controllers/index').RequestForVehicles;


/**
 * Admin
 */
/**
 * repair request
 */
router.get('/repairRequestList', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('repair-request-management')], RequestRepair.fetchRequestForRepair);
router.get('/detailsForRequestRepair/:id', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('repair-request-management')], RequestRepair.requestForRepairDetails);
router.post('/actionOnRepairRequest', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('repair-request-management')], RequestRepair.actionOnRepairRequest);
/**
 * vehicle request
 */
router.get('/fetchVehicleRequests', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-request-management')], VehicleRequest.fetchVehicleRequestsFromBusiness);
router.post('/performActionOnRequest/:id', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-request-management')], VehicleRequest.performActionOnRequest);



/**
 * business
 */

/**
 * repair request
 */
router.get('/fetchMyDriverRepairRequest', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('request-for-repair')], RequestRepair.fetchRequestForRepair);
router.get('/fetchMyRepairRequestData/:id', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('request-for-repair')], RequestRepair.requestForRepairDetails);

/**
 * vehicle request
 */
router.post('/askForNewVehicles', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('raise-vehicle-request')], VehicleRequest.addNewVeicleRequest);
router.get('/fetchMyRequestedVehicles', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('raise-vehicle-request')], VehicleRequest.fetchRequestedVehicleList);



module.exports = router;