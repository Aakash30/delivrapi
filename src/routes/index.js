const appVersion = require('./app-version');
const login = require('./login');
const business = require('./business');

const vehicles = require('./vehicle');
const systemSettings = require('./systemSettings');

const driver = require('./driver');
const order = require('./order');
const userArea = require('./business-area-user');

const driverApi = require('./driver-api');
const coupon = require('./coupon');

const payment = require('./payment');
const requestManagement = require('./Request');
const accounts = require('./accounts');

const commonApi = require('./common');
module.exports = [
    appVersion,
    login,
    business,
    vehicles,
    systemSettings,
    driver,
    order,
    userArea,
    driverApi,
    commonApi,
    payment,
    requestManagement,
    coupon,
    accounts
];