const DriverController = require('./../../controllers/index').DriverController;
const Fine = require('./../../controllers/driverModule/FineController')
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');
const aws = require('./../../controllers/aws/aws');
require('./../../middlewares/passport')(passport);

router.get('/driverList/', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.fetchDriverList);
router.get('/addEditDriverFormData', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.fetchDataForAddEditDriver);
router.get('/addEditDriverFormData/:driverId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.fetchDataForAddEditDriver);
router.post('/addUpdateDriver/', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.addUpdateDriver);
router.get('/test_zubear/', DriverController.test_zubear);

router.get('/fetchSingleDriver/:driverId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.fetchSingleDriverDetails);
router.post('/activeBlockDriver', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.activeDeactiveDriver);
router.get('/fetchAvailableVehicleForBusiness', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.fetchAvailableVehicleForBusiness);
router.get('/assignVehicleToDriver/:vehicleId/:driverId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.assignVehicleToDriver);

router.get('/fetchAvailableDriverForVehicles', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('vehicle-management')], DriverController.searchDriversForVehicles);
router.post('/changeVehicleDriver', [usercheck.checkUser(), roleCheck.vendorAccess], DriverController.changeVehicleDriver);


router.get('/fetchDriverFeedback', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.fetchDriverFeedback);
router.get('/getFeedbackDetails/:driverId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.fetchIndividualDriverFeedback);

router.get('/driverTrackingList', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('driver-management')], DriverController.driverTrackingList);

router.post('/addFine', [usercheck.checkUser(), roleCheck.driverAccess, aws.upload.array('files')], Fine.addFine);
router.get('/fetchFines', [usercheck.checkUser(), roleCheck.driverAccess],  Fine.fetchFines);

router.post('/addFuelUpdate', [usercheck.checkUser(), roleCheck.driverAccess, aws.upload.single('image')], Fine.addFuelUpdate);
router.get('/fetchFuelUpdatesForDriver', [usercheck.checkUser(), roleCheck.driverAccess], Fine.fetchFuelUpdates);

router.post('/addDriverPhoto', [usercheck.checkUser(), roleCheck.driverAccess, aws.upload.single('image')], Fine.addDriverPhoto);

module.exports = router;