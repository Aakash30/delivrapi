const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const AppVersionController = require('./../../controllers/index').AppVersionController;

router.post('/appversion', AppVersionController.getAppVersion);

module.exports = router;
