const accounts = require('./../../controllers').Accounts;

const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');

require('./../../middlewares/passport')(passport);

router.get('/fetchMyAccountsDetail', [usercheck.checkUser()], accounts.fetchAccountDetail);
router.post('/accountsUpdate', [usercheck.checkUser()], accounts.updateAccountsForDriver);

router.get('/fetchDriverAccountStatement/:id', [usercheck.checkUser()], accounts.getAccountDetails);

module.exports = router;