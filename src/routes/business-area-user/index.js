const BusinessAreaUser = require('./../../controllers/index').BusinessAreaUser;
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');
const fileupload = require('./../../middlewares/fileupload');
require('./../../middlewares/passport')(passport);
const aws = require('./../../controllers/aws/aws');



router.get('/addEditBusinessBranchFormData', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('branch-management')], BusinessAreaUser.fetchBusinessAreaForAddEdit);
router.get('/addEditBusinessBranchFormData/:businessBranchId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('branch-management')], BusinessAreaUser.fetchBusinessAreaForAddEdit);
router.post('/addUpdateBusinessArea', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('branch-management')], BusinessAreaUser.addUpdateBusinessArea);
router.get('/fetchMyBranches', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('branch-management')], BusinessAreaUser.fetchBusinessAreas);
router.get('/fetchSingleBusinessBranch/:businessBranchId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('branch-management')], BusinessAreaUser.fetchSingleBusinessBranch);

router.get('/fetchAddEditBusinessAdminForm', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('user-management')], BusinessAreaUser.fetchForBusinessSubAdminAddEdit);
router.get('/fetchAddEditBusinessAdminForm/:id', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('user-management')], BusinessAreaUser.fetchForBusinessSubAdminAddEdit);
router.post('/addBusinessUsers', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('user-management')], BusinessAreaUser.addUpdateBusinessUser);
router.get('/fetchMyUsers', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('user-management')], BusinessAreaUser.fetchBusinessUsers);
router.get('/fetchMyBusinessUser/:id', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('user-management')], BusinessAreaUser.fetchMyBusinessUser);
router.post('/updateMyUsersStatus', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('user-management')], BusinessAreaUser.updateMyUsersStatus);

router.post('/updateBranchStatus', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('branch-management')], BusinessAreaUser.updateBranchStatus);
router.get('/fetchMyBranchList', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('branch-management')], BusinessAreaUser.fetchMyBusinessBranch);

/**Profile */

router.get('/fetchUserProfile', [usercheck.checkUser()], BusinessAreaUser.fetchMyProfile);
router.post('/updateUserProfile', [usercheck.checkUser(), aws.upload.single("profile")], BusinessAreaUser.updateMyProfile);
router.post('/updateBusinessLogo', [usercheck.checkUser(), aws.upload.single("business_logo")], BusinessAreaUser.updateMyProfile);

module.exports = router;