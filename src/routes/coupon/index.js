const CouponController = require('./../../controllers/index').CouponController;
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');
require('./../../middlewares/passport')(passport);
const aws = require('./../../controllers/aws/aws');


router.get('/fetchCouponsForAdmin/', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('view-coupons')], CouponController.fetchCouponListForAdmin);
router.get('/fetchCouponsForBusiness/', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('coupon-management')], CouponController.fetchCouponListForAdmin);
router.get('/fetchDataForAddEditCoupons/', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('coupon-management')], CouponController.fetchDataForAddEditCoupons);
router.get('/fetchDataForAddEditCoupons/:id', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('coupon-management')], CouponController.fetchDataForAddEditCoupons);
router.post('/addUpdateCoupon/', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('coupon-management'), aws.upload.single("coupon_image")], CouponController.addUpdateCouponData);
router.get('/generateCode/', CouponController.generateCode);


/**driver api */
router.get('/fetchCoupons/', [usercheck.checkUser(), roleCheck.driverAccess], CouponController.fetchCoupons);



//[usercheck.checkUser(), roleCheck.driverAccess],

module.exports = router;