const VehicleController = require('./../../controllers/index').VehicleController;
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const passport = require('passport');
const usercheck = require('./../../middlewares/usercheck');
const roleCheck = require('./../../middlewares/rolecheck');
require('./../../middlewares/passport')(passport);

/** Routes for Vehicle Module Super admin */

router.get('/vehicles', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.fetchVehicleListForSuperAdmin);
router.get('/fetchDataForAddEditVehicle', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.addEditVehicleFormData);
router.get('/fetchDataForAddEditVehicle/:vehicleId', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.addEditVehicleFormData);
router.post('/addUpdateVehicle', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.addUpdateVehicle);
router.post('/checkDuplicateEntry/:fieldToCheck', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.checkDuplicateEntry);
router.post('/checkDuplicateEntry/:fieldToCheck/:vehicleId', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.checkDuplicateEntry);
router.get('/fetchSingleVehicleDetails/:vehicleId', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.fetchSingleVehicleDetails);
router.post('/markAvailablityForVehicle/:vehicleId', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.markAvailablityForVehicle);
router.get('/fetchAvailableVehiclesForAdmin/:businessId', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.fetchAvailableVehiclesForAdmin);

router.get('/fetchBatchDataForRenewal/:businessId/:batchId', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.fetchVehiclesForBatchRenewal);
router.post('/renewBatch/:businessId', [usercheck.checkUser(), roleCheck.adminAccess, roleCheck.checkRole('vehicle-management')], VehicleController.renewBatch);


router.get('/inventoryVehicles', [usercheck.checkUser(), roleCheck.adminAccess], roleCheck.checkRole('vehicle-inventory'), VehicleController.fetchInventoryData);
/** Routes for Vehicle Module Business Module */

router.get('/businessVehicleLists', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('vehicle-details')], VehicleController.fetchAssignedVehicleForBusinessUser);
router.get('/businessVehicleDetails/:vehicleId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('vehicle-details')], VehicleController.businessVehicleDetails);
router.post('/assignVehiclesToMyBranches', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('branch-management')], VehicleController.assignVehiclesToBusinessBranch);

router.get('/fetchBranchAssignBikeList/:branchId', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('branch-management')], VehicleController.fetchAvailableVehicleForBusiness);

router.get('/fetchMyLeasePlan', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('vehicle-lease-plan')], VehicleController.fetchLeasePlan);
router.get('/getBatchDetails/:id', [usercheck.checkUser(), roleCheck.vendorAccess, roleCheck.checkRole('vehicle-details')], VehicleController.getBatchDetails);

module.exports = router;