exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('user_type_roles').del()
    .then(function () {
      // Inserts seed entries
      return knex('user_type_roles').insert([{
          "id": 1,
          "user_type": 'super_admin',
          "module": 'dashboard',
          "roles": 'dashboard',
          "role_label": 'View Dashboard'
        },
        {
          "id": 2,
          "user_type": 'super_admin',
          "module": 'vehicle',
          "roles": 'vehicle-management',
          "role_label": 'Vehicle Management'
        },
        {
          "id": 3,
          "user_type": 'super_admin',
          "module": 'vehicle',
          "roles": 'vehicle-inventory',
          "role_label": 'View Vehicle Inventory'
        },
        {
          "id": 4,
          "user_type": 'super_admin',
          "module": 'business',
          "roles": 'business-category',
          "role_label": 'Manage Business Type'
        },
        {
          "id": 5,
          "user_type": 'super_admin',
          "module": 'business',
          "roles": 'business-management',
          "role_label": 'Business Management'
        },
        {
          "id": 6,
          "user_type": 'super_admin',
          "module": 'payments',
          "roles": 'payments',
          "role_label": 'Payments Management'
        },
        {
          "id": 7,
          "user_type": 'super_admin',
          "module": 'coupons',
          "roles": 'view-coupons',
          "role_label": 'View Coupons'
        },
        {
          "id": 8,
          "user_type": 'super_admin',
          "module": 'request',
          "roles": 'repair-request-management',
          "role_label": 'Manage Repair Requests'
        },
        {
          "id": 9,
          "user_type": 'super_admin',
          "module": 'request',
          "roles": 'vehicle-request-management',
          "role_label": 'Manage Vehicle Requests'
        },
        {
          "id": 10,
          "user_type": 'super_admin',
          "module": 'request',
          "roles": 'module-request-management',
          "role_label": 'Check Front-end Requests'
        },
        {
          "id": 11,
          "user_type": 'super_admin',
          "module": 'reports',
          "roles": 'reports-management',
          "role_label": 'Reports Management'
        },
        {
          "id": 12,
          "user_type": 'super_admin',
          "module": 'settings',
          "roles": 'location-setting',
          "role_label": 'Add Country / City'
        },
        {
          "id": 13,
          "user_type": 'super_admin',
          "module": 'settings',
          "roles": 'service-setting',
          "role_label": 'Service Management'
        },
        {
          "id": 14,
          "user_type": 'super_admin',
          "module": 'settings',
          "roles": 'service-category-setting',
          "role_label": 'Service Category Management'
        },
        {
          "id": 15,
          "user_type": 'super_admin',
          "module": 'settings',
          "roles": 'brand-model-setting',
          "role_label": 'Brand / Model Management'
        },
        {
          "id": 16,
          "user_type": 'vendor',
          "module": 'dashboard',
          "roles": 'dashboard',
          "role_label": 'View Dashboard'
        },
        {
          "id": 17,
          "user_type": 'vendor',
          "module": 'order',
          "roles": 'order-management',
          "role_label": 'Order Management'
        },
        {
          "id": 18,
          "user_type": 'vendor',
          "module": 'driver',
          "roles": 'driver-management',
          "role_label": 'Driver Management'
        },
        {
          "id": 19,
          "user_type": 'vendor',
          "module": 'driver',
          "roles": 'driver-feedback',
          "role_label": 'View Driver Feedback'
        },
        {
          "id": 20,
          "user_type": 'vendor',
          "module": 'vehicle',
          "roles": 'vehicle-details',
          "role_label": 'View Vehicle Details'
        },
        {
          "id": 21,
          "user_type": 'vendor',
          "module": 'vehicle',
          "roles": 'vehicle-lease-plan',
          "role_label": 'View Vehicle Lease Plan'
        },
        {
          "id": 22,
          "user_type": 'vendor',
          "module": 'driver',
          "roles": 'track-your-driver',
          "role_label": 'Track Your Driver'
        },
        {
          "id": 23,
          "user_type": 'vendor',
          "module": 'request',
          "roles": 'request-for-repair',
          "role_label": 'View Repair Requests'
        },
        {
          "id": 24,
          "user_type": 'vendor',
          "module": 'request',
          "roles": 'raise-vehicle-request',
          "role_label": 'Make request for new vehicles.'
        },
        {
          "id": 25,
          "user_type": 'vendor',
          "module": 'accounts',
          "roles": 'accounts-management',
          "role_label": 'Accounts Management'
        },
        {
          "id": 26,
          "user_type": 'vendor',
          "module": 'user',
          "roles": 'user-management',
          "role_label": 'User Management'
        },
        {
          "id": 27,
          "user_type": "vendor",
          "module": 'branch',
          "roles": 'branch-management',
          "role_label": "Branch Management"
        }, {
          "id": 28,
          "user_type": "vendor",
          "module": 'coupons',
          "roles": 'coupon-management',
          "role_label": "Coupon Management"
        }
      ]);
    });
};