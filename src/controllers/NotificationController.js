const Notification = require("./../models/NotificationRecord");
const User = require('../models/Users');
const Knex = require('knex');

const knexConfig = require('./../../db/knex');
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);


const {
  io
} = require("../globals");

const updateNotification = async (data, token) => {

  try {
    console.log("notification", data[0])
    if (data[0].notification_type == "ORDER_DELIVERED") {


      data.forEach(async (element) => {
        let createData = {
          time: element.time,
          title: element.title,
          body: element.body,
          notification_type: element.notification_type,
          url: element.url
        };
        let r = await Notification.query().patch(createData).where("sender_id", element.sender_id).where("unique_id", element.unique_id).where("receiver_id", element.receiver_id)
          .where("notification_type", "ORDER_ACCEPTED");
      });

    } else if (data[0].notification_type == "BATCH_EXPIRED_RESPONSE" || data[0].notification_type == "BATCH_EXPIRED" || data[0].notification_type == "REPAIR_REQUEST_RESPONSE" || data[0].notification_type == "REPAIR_REQUEST_RESPONSE_BUSINESS" || data[0].notification_type == "VEHICLE_REQUEST_RESPONSE") {
      let columnName = Object.keys(data[0]).join(",");
      let columnCount = Object.keys(data[0]).length;
      let valueString = "(";
      for (j = 0; j < columnCount; j++) {
        valueString += "?";
        if (j != columnCount - 1)
          valueString += ",";
      }
      valueString += ")";
      questionArray = [];
      valueArray = [];
      data.forEach(async (notify) => {
        questionArray.push(valueString);
        valueArray.push(Object.values(notify));

        await knex.raw("INSERT INTO notifications (" + columnName + ") VALUES " + valueString + "ON CONFLICT ON CONSTRAINT notifications_notification_type_unique_id_sender_id_receiver_id DO UPDATE SET title = notifications.title, body = notifications.body, notification_type = notifications.notification_type", Object.values(notify));
      });

    } else {
      let notificationUpdate = await Notification.query().insertGraph(data);
    }


  } catch (error) {
    console.log(error)
  }

  let i = 0;

  data.forEach(async userData => {

    let notificationDot = await User.query().update({
      "has_new_alerts": true
    }).where("user_id", userData.receiver_id).returning("has_new_alerts");

    if (token.length > 0) {
      let notificationList = await Notification.query()
        .select("notification_id", "title", "body", "isRead")
        .where("receiver_id", userData.receiver_id)
        .orderBy("created_at", "DESC")
        .offset(0)
        .limit(3);

      io.sockets.to(token[i]).emit("message", {
        notification: notificationList,
        notificationDot: notificationDot.has_new_alerts
      });
    }
    i++;
  });


};

const fetchNotifications = async (req, res) => {

  let page = (req.query.page) ? req.query.page : 1;
  let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
  let offset = req.query.offset ? req.query.offset : limit * (page - 1);

  try {
    let notifications = await Notification.query()
      .select(
        "notification_id",
        "notification_type",
        "url",
        "receiver_id",
        "title",
        "body",
        "time",
        "isRead",
        "unique_id"
      )
      .where({
        receiver_id: req.user.user_id
      }).offset(offset).limit(limit)
      .orderBy("time", "desc");
    let totalCount = await Notification.query()
      .count().where({
        receiver_id: req.user.user_id
      }).first();

    let resultData = {
      notifications,
      total: totalCount.count
    };
    // return response
    return global.okResponse(res, {
      ...resultData,
    }, "");
  } catch (error) {
    console.log(error);
    throw global.badRequestError(error.message);
  }

};

const updateCheckNoticeAlert = async (req, res) => {
  let notificationDot = await User.query().update({
    "has_new_alerts": false
  }).where("user_id", req.user.user_id).returning("has_new_alerts");
  return global.okResponse(res, {
    ...notificationDot,
  }, "");
};

const updateNotificationReadStatus = async (id) => {

  let updateNotification = await Notification.query().update({
    'isRead': true
  }).where("notification_id", id);
};

module.exports = {
  updateNotification,
  fetchNotifications,
  updateCheckNoticeAlert,
  updateNotificationReadStatus
};