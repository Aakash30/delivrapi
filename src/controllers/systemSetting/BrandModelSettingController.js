'use strict';

const vehicleBrand = require('../../models/VehicleBrand');
const vehicleModel = require('../../models/VehicleModel');

/**
 * Fetches branch list for setting page
 * @param {*} req 
 * @param {*} res 
 */
const fetchBrandListForAll = async (req, res) => {

    /** Get the pagination data in request query */
    let page = (req.query.page) ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    let fetchBrands = await vehicleBrand.query().select("brand_id", "brand_name").eager("[VehicleModel, Vehicle]").modifyEager("VehicleModel", builder => {
        builder.count("model_id").groupBy("brand_id");
    }).modifyEager("Vehicle", builder => {
        builder.count("vehicle_id").groupBy("brand");
    }).where((builder) => {

        /** If any keyword filter is applied */
        if (req.query.keyword) {
            builder.where('brand_name', 'ilike', '%' + req.query.keyword + '%');
        }
    }).orderBy("created_at", "DESC").offset(offset).limit(limit);

    let totalBrand = await vehicleBrand.query().count("brand_id").where((builder) => {

        /** If any keyword filter is applied */
        if (req.query.keyword) {
            builder.where('brand_name', 'ilike', '%' + req.query.keyword + '%');
        }
    }).first();
    // prepare the return data
    let returnData = {
        count: totalBrand.count,
        fetchBrands: fetchBrands,
    };

    return global.okResponse(res, {
        ...returnData,
    }, "");
};

/**
 * Add / update Brands
 */

const addUpdateBrand = async (req, res) => {
    let data = req.body;

    if (!data.brand_name || data.brand_name.trim() == "") {
        throw global.badRequestError("Please add a brand name.");
    } else {
        data.brand_name = data.brand_name.toLowerCase();
    }

    let message;
    if (data.brand_id == null) {
        delete data.brand_id;
        message = "Brand has been added successfully.";
    } else {
        message = "Brand has been updated successfully.";
    }

    let addUpdateBrand;
    try {
        addUpdateBrand = await vehicleBrand.query().upsertGraph(data);
    } catch (error) {
        if (error.code == "23505") {
            /** duplicate error */
            throw global.badRequestError("This brand alread exists.");
        }
        throw global.badRequestError("Something went wrong.");
    }
    return global.okResponse(res, {
        ...addUpdateBrand,
    }, message);
};

/**
 * Get the branch details and related model
 * @param {*} req 
 * @param {*} res 
 */
const fetchBrandDetails = async (req, res) => {

    /** Get the pagination data in request query */
    let page = (req.query.page) ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    if (!req.params.id) {
        throw global.badRequestError("Invalid Request");
    }

    let brand_id = new Buffer(req.params.id, "base64").toString("ascii");
    if (isNaN(parseInt(brand_id))) {
        throw global.badRequestError("Invalid Request");
    }

    /** execute the query to fetch model list data */

    let modelList = await vehicleModel.query().select("model_id", "model_name").eager("[Vehicle]").modifyEager("Vehicle", builder => {

        builder.count("vehicle_id").groupBy("model");


    }).orderBy("created_at", "DESC").where("brand_id", brand_id).where((builder) => {

        /** If any keyword filter is applied */
        if (req.query.keyword) {
            builder.where('model_name', 'ilike', '%' + req.query.keyword + '%');
        }
    }).offset(offset).limit(limit);


    // get the total count
    let totalBrand = await vehicleModel.query().count('model_id').where("brand_id", brand_id).where((builder) => {

        if (req.query.keyword) {
            builder.where('model_name', 'ilike', '%' + req.query.keyword + '%');
        }
    }).first();

    let brandName = await vehicleBrand.query().select("brand_name").where("brand_id", brand_id).first();

    // prepare the return data
    let returnData = {
        count: totalBrand.count,
        modelList: modelList,
        brandName: brandName.brand_name
    };

    return global.okResponse(res, {
        ...returnData,
    }, "");
};


/**
 * Add / update Models
 */
const addUpdateModel = async (req, res) => {
    let data = req.body;

    if (!data.model_name || data.model_name.trim() == "") {
        throw global.badRequestError("Please add a brand name.");
    } else {
        data.model_name = data.model_name.toLowerCase();
    }

    if (!data.brand_id) {
        throw global.badRequestError("Invalid request");
    }

    let message;
    if (data.model_id == null) {
        delete data.model_id;
        message = "Model has been added successfully.";
    } else {
        message = "Model has been updated successfully.";
    }

    let addUpdateModelData;
    try {
        addUpdateModelData = await vehicleModel.query().upsertGraph(data);
    } catch (error) {
        if (error.code == "23505") {
            /** duplicate error */
            throw global.badRequestError("This model alread exists.");
        }
        throw global.badRequestError("Something went wrong.");
    }
    return global.okResponse(res, {
        ...addUpdateModelData,
    }, message);

};

module.exports = {
    fetchBrandListForAll,
    addUpdateBrand,
    fetchBrandDetails,
    addUpdateModel
};