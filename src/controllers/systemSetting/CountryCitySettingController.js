'use strict';

const Country = require('../../models/Countries');
const City = require('../../models/City');

/**
 * @function fetchCountry
 * @description: This function is used to fetch the country list for admin 
 * @param {*} req Stores the request data
 * @param {*} res Stores the response data
 */
const fetchCountry = async (req, res) => {

    /** Get the pagination data in request query */
    let page = (req.query.page) ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    /** execute the query to fetch country list data */
    let countryList;
    try {
        countryList = await Country.query().select("country_id", "country", "status", "currency", "country_code").eager("[cities, businessBranch]").modifyEager("cities", builder => {
            builder.count("city_id").groupBy("country_id");
        }).modifyEager("businessBranch", builder => {

            builder.count("branch_area_id").where("type", "mainbranch").groupBy("country").groupBy("business_id");

            builder.eager("business").modifyEager("business", builder => {
                builder.select("");

                builder.eager("vehicleMapping").modifyEager("vehicleMapping", builder => {
                    builder.count("map_id").groupBy("business_id");
                });
            });

        }).where((builder) => {

            /** If any keyword filter is applied */
            if (req.query.keyword) {
                builder.where('country', 'ilike', '%' + req.query.keyword + '%');
            }
        }).offset(offset).limit(limit);
    } catch (error) {
        throw global.badRequestError(error.message);
    }

    // get the total count
    let totalCountry = await Country.query().count('country_id').where((builder) => {

        if (req.query.keyword) {
            builder.where('country', 'ilike', '%' + req.query.keyword + '%');
        }
    }).first();


    // prepare the return data
    let returnData = {
        count: totalCountry.count,
        countryList: countryList,
        page: page
    };

    return global.okResponse(res, {
        ...returnData,
    }, "");
};


/**
 * @function fetchCountry
 * @description: This function is used to fetch the country details and related cities for admin 
 * @param {*} req Stores the request data
 * @param {*} res Stores the response data
 */
const getCountryDetails = async (req, res) => {

    /** Get the pagination data in request query */
    let page = (req.query.page) ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    if (!req.params.countryId) {
        throw global.badRequestError("Invalid Request");
    }
    /** execute the query to fetch city list data */
    let cityList;
    try {
        cityList = await City.query().select("city_id", "city", "status").eager("[businessBranch]").modifyEager("businessBranch", builder => {

            builder.count("branch_area_id").where("type", "mainbranch").groupBy("city").groupBy("business_id");

            builder.eager("business").modifyEager("business", builder => {
                builder.select("");

                builder.eager("vehicleMapping").modifyEager("vehicleMapping", builder => {
                    builder.count("map_id").groupBy("business_id");
                });
            });

        }).where("country_id", req.params.countryId).where((builder) => {

            /** If any keyword filter is applied */
            if (req.query.keyword) {
                builder.where('city', 'ilike', '%' + req.query.keyword + '%');
            }
        }).offset(offset).limit(limit);
    } catch (error) {
        throw global.badRequestError(error.message);
    }

    // get the total count
    let totalCity = await City.query().count('city_id').where("country_id", req.params.countryId).where((builder) => {

        if (req.query.keyword) {
            builder.where('city', 'ilike', '%' + req.query.keyword + '%');
        }
    }).first();

    let countryName = await Country.query().select("country").where("country_id", req.params.countryId).first();

    // prepare the return data
    let returnData = {
        count: totalCity.count,
        cityList: cityList,
        page: page,
        country: countryName.country
    };

    return global.okResponse(res, {
        ...returnData,
    }, "");
};

/**
 * @function addUpdateCountry
 * @description: This function is used to add / update (when id is passed in URL as a param) country details.
 * @param {*} req Stores the request data
 * @param {*} res Stores the response data
 */

const addUpdateCountry = async (req, res) => {

    /** Get the data posted in the form */
    const data = req.body;

    /** If country is not added */
    if (!data.country) {
        throw global.badRequestError("Please enter country name");
    }

    /** If currenct is not added */
    if (!data.currency) {
        throw global.badRequestError("Please add currency.");
    }

    /** If currenct is not added */
    if (!data.country_code) {
        throw global.badRequestError("Please add country code for mobile number.");
    }

    /** If currenct is not added */
    if (!data.alpha_code) {
        throw global.badRequestError("Please add short code for the country.");
    }

    let countryInsertUpdate, message;

    if (!data.country_id) {
        data.created_by = req.user.user_id;

        message = "Country has been added successfully.";
    } else {
        data.updated_by = req.user.user_id;
        message = "Country has been updated successfully.";
    }

    countryInsertUpdate = await Country.query().upsertGraph(data);


    // if not executed throw error
    if (!countryInsertUpdate) {
        throw global.badRequestError('Something went wrong.');
    }
    // return response
    return global.okResponse(res, {
        ...countryInsertUpdate,
    }, message);
};

/**
 * @function activeInActiveCountry
 * @description: This function is used to switch a country's status from active to inactive and viceversa
 * @param {*} req Stores the request data
 * @param {*} res Stores the response data
 */

const activeInActiveCountry = async (req, res) => {

    /** If the country id is not passed throw url as city list is filtered on the basis of country. */
    if (!req.params.countryId) {
        throw global.badRequestError("Invalid Url");
    }

    let data = req.body;

    if (!data.status) {
        throw global.badRequestError("");
    }

    let updateStatus = await Country.query().update({
        "status": data.status,
        "updated_by": req.user.user_id
    }).where("country_id", req.params.countryId);

    // if not executed throw error
    if (!updateStatus) {
        throw global.badRequestError('Something went wrong.');
    }

    let updateCityStatus = await City.query().update({
        "status": data.status,
        "updated_by": req.user.user_id
    }).where("country_id", req.params.countryId);

    let message;
    if (data.status == 'active') {
        message = "Country has been activated successfully.";
    } else {
        message = "Country has been deactivated successfully.";
    }
    // return response
    return global.okResponse(res, {
        ...updateStatus,
    }, message);
};
/**
 * @function fetchCity
 * @description: This function is used to fetch city list data for admin
 * @param {*} req Stores the request data
 * @param {*} res Stores the response data
 */

const fetchCity = async (req, res) => {

    /** Get the pagination data in request query */

    let page = (req.query.page) ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
    let offset = limit * (page - 1);

    /** If the country id is not passed throw url as city list is filtered on the basis of country. */
    if (!req.params.countryId) {
        throw global.badRequestError("Invalid Url");
    }

    /** execute the query to fetch country list data */

    let cityList = await City.query().where((builder) => {

        /** If any keyword filter is implemented */
        if (req.query.keyword) {
            builder.where('city', 'ilike', '%' + req.query.keyword + '%');
        }
    }).where("country_id", req.params.countryId).offset(offset).limit(limit);

    // get the total count
    let totalCity = await City.query().count('country_id').where((builder) => {

        if (req.query.keyword) {
            builder.where('city', 'ilike', '%' + req.query.keyword + '%');
        }

    }).where("country_id", req.params.countryId);

    let returnData = {
        count: totalCity,
        cityList: cityList,
        page: page
    };

    return global.okResponse(res, {
        ...returnData,
    }, "");
};

/**
 * @function addUpdateCity
 * @description: This function is used to add / update (when id is passed in URL as a param) city details.
 * @param {*} req Stores the request data
 * @param {*} res Stores the response data
 */

const addUpdateCity = async (req, res) => {

    /** Get the data posted in the form */
    const data = req.body;

    /** If city is not added */
    if (!data.city) {
        throw global.badRequestError("Please enter city name.");
    }

    let cityInsertUpdate, message;

    /** If city id is not passed as param then execute insert query */
    if (!data.city_id) {


        message = "City has been added successfully.";
        data.created_by = req.user.user_id;
    } else {

        message = "City has been updated successfully.";
        data.updated_by = req.user.user_id;
    }

    cityInsertUpdate = await City.query().upsertGraph(data).returning("city_id");

    // if not executed throw error
    if (!cityInsertUpdate) {
        throw global.badRequestError('Something went wrong.');
    }
    // return response
    return global.okResponse(res, {
        ...cityInsertUpdate,
    }, message);
};

/**
 * @function activeInActiveCity
 * @description: This function is used to switch a city's status from active to inactive and viceversa
 * @param {*} req Stores the request data
 * @param {*} res Stores the response data
 */

const activeInActiveCity = async (req, res) => {

    /** If the country id is not passed throw url as city list is filtered on the basis of country. */
    if (!req.params.cityId) {
        throw global.badRequestError("Invalid Url");
    }

    let data = req.body;

    if (!data.status) {
        throw global.badRequestError("");
    }

    let updateStatus = await City.query().update({
        "status": data.status,
        "updated_by": req.user.user_id
    }).where("city_id", req.params.cityId);

    // if not executed throw error
    if (!updateStatus) {
        throw global.badRequestError('Something went wrong.');
    }

    let message;
    if (data.status == 'active') {
        message = "City has been activated successfully.";
    } else {
        message = "City has been inactivated successfully.";
    }
    // return response
    return global.okResponse(res, {
        ...updateStatus,
    }, message);
};

/**
 * @function fetchCountry
 * @description: This function is used to fetch the country list for admin 
 * @param {*} req Stores the request data
 * @param {*} res Stores the response data
 */
const getCityDetails = async (req, res) => {

    if (!req.params.cityId) {
        throw global.badRequestError("Invalid Request");
    }
    /** execute the query to fetch country list data */
    let cityData;
    try {
        cityData = await City.query().select("city_id", "city", "status").eager("[businessBranch, Country]").modifyEager("businessBranch", builder => {

            builder.count("branch_area_id").where("type", "mainbranch").groupBy("city").groupBy("business_id");

            builder.eager("business").modifyEager("business", builder => {
                builder.select("");

                builder.eager("vehicleMapping").modifyEager("vehicleMapping", builder => {
                    builder.count("map_id").groupBy("business_id");
                });
            });

        }).modifyEager('Country', builder => {
            builder.select('country_id', 'country');
        }).where("city_id", req.params.cityId).first();
    } catch (error) {
        throw global.badRequestError(error.message);
    }


    // prepare the return data
    let returnData = {
        cityData: cityData
    };

    return global.okResponse(res, {
        ...returnData,
    }, "");
};

module.exports = {
    fetchCountry,
    fetchCity,
    addUpdateCountry,
    addUpdateCity,
    activeInActiveCountry,
    activeInActiveCity,
    getCountryDetails,
    getCityDetails,
};