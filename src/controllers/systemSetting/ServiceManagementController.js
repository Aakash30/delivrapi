'use strict';

const ServiceCategory = require('../../models/ServiceCategory');
const Services = require('../../models/Services');

const Country = require('../../models/Countries');
const Brand = require('../../models/VehicleBrand');
const Model = require('../../models/VehicleModel');

const {
    transaction
} = require('objection');

/** Service category */

/**
 * fetch service category for admin list
 */

const fetchServiceCategoryForAdmin = async (req, res) => {

    let serviceCategoryList = await ServiceCategory.query().select("service_category_id", "service_category_name", "status").where((builder) => {
        if (req.query.keyword) {
            builder.where("service_category_name", "ilike", req.query.keyword);
        }
    });

    let totalCount = await ServiceCategory.query().count("service_category_id").where((builder) => {
        if (req.query.keyword) {
            builder.where("service_category_name", "ilike", req.query.keyword);
        }
    }).first();

    let responseData = {
        serviceCategoryList,
        count: totalCount.count
    };

    // return response
    return global.okResponse(res, {
        ...responseData,
    }, "");
};
/**
 * add / update service category
 */

const addUpdateServiceCategory = async (req, res) => {

    let data = req.body;

    if (!data.service_category_name || data.service_category_name.trim() == "") {
        throw global.badRequestError("Please enter service category name.");
    }

    let message;
    if (data.service_category_id == null) {
        delete data.service_category_id;
        message = "Service category has been added successfully.";
    } else {
        message = "Service category has been updated successfully.";
    }
    let insertUpdate;
    try {
        insertUpdate = await ServiceCategory.query().upsertGraph(data);
    } catch (error) {
        throw global.badRequestError(error.message);
    }

    if (!insertUpdate) {
        throw global.badRequestError("Something went wrong.");
    }

    // return response
    return global.okResponse(res, {
        ...insertUpdate,
    }, message);
};

/**
 * update status for service category
 */
const updateStatusForServiceCategory = async (req, res) => {

    let data = req.body;
    if (!data.service_category_id) {
        throw global.badRequestError("Invalid Request");
    }

    if (!data.status) {
        throw global.badRequestError("Invalid Request");
    }

    let updateStatus = await ServiceCategory.query().update({
        status: data.status
    }).where("service_category_id", data.service_category_id);

    if (!updateStatus) {
        throw global.badRequestError("Something went wrong.");
    }
    return global.okResponse(res, {
        ...updateStatus
    }, "Status updated successfully.");
};

/***** Services */
const fetchServicesForAdmin = async (req, res) => {

    let serviceData;
    try {
        serviceData = await Services.query().select("list_id", "service_name", "service_category_name").joinRelation("serviceCategory").eager("servicePricing").modifyEager("servicePricing", builder => {
            builder.select("price", "engine_capacity", "countries.country", "brand_name", "model_name", "countries.currency").joinRelation("countries").joinRelation("brandList").joinRelation("modelList");

        }).where((builder) => {
            if (req.query.keyword) {
                builder.where("service_name", "ilike", "%" + req.query.keyword + "%");
            }
        });
    } catch (error) {
        console.log(error);
    }

    let totalCount = await Services.query().count("list_id").joinRelation("serviceCategory").where((builder) => {
        if (req.query.keyword) {
            builder.where("service_name", "ilike", "%" + req.query.keyword + "%");
        }
    }).first();

    let responseData = {
        serviceData,
        total: totalCount.count
    };
    return global.okResponse(res, {
        ...responseData
    }, "Status updated successfully.");
};

const fetchDataForServiceForm = async (req, res) => {

    let country = await Country.query().select("country_id", "country", "currency", "status");

    let vehicleBrand = await Brand.query().select("brand_id", "brand_name");

    let vehicleModel = await Model.query().select("model_id", "model_name", "brand_id");

    let serviceCategory = await ServiceCategory.query().select("service_category_id", "service_category_name", "status");

    let resultData = {
        country,
        vehicleBrand,
        vehicleModel,
        serviceCategory
    };

    if (req.params.id) {
        /** If asked for service data */
        let serviceData = await Services.query().select("list_id", "service_name", "service_category").eager("servicePricing").where("list_id", req.params.id).first();
        resultData.serviceData = serviceData;
    }

    return global.okResponse(res, {
        ...resultData
    }, "");
};

/**
 * Add / update service
 */

const addUpdateServices = async (req, res) => {

    let data = req.body;

    if (!data.service_category) {
        throw global.badRequestError("Please choose service category.");
    }

    if (!data.service_name || data.service_name.trim() == "") {
        throw global.badRequestError("Please provide a name for your service.");
    }

    if (!data.type) {
        throw global.badRequestError("Please choose whether your service is paid or free.");
    } else {
        if (data.type == "paid") {
            if (!data.servicePricing || data.servicePricing.length == 0) {
                throw global.badRequestError("Add pricing for paid services.");
            }
        }
    }

    let message = "Service has been updated successfully.";
    if (data.list_id == null) {
        delete data.list_id;
        message = "Service has been added successfully.";
    }
    let insertUpdate;
    try {
        insertUpdate = await transaction(Services.knex(), trx => {
            return (Services.query(trx).upsertGraph(data, {
                relate: true,
                unrelate: true
            }));
        });
    } catch (error) {
        throw global.badRequestError(error.message);
    }

    if (!insertUpdate) {
        throw global.badRequestError("Something went wrong.");
    }
    return global.okResponse(res, {
        ...insertUpdate
    }, message);

};


module.exports = {
    fetchServiceCategoryForAdmin,
    addUpdateServiceCategory,
    updateStatusForServiceCategory,
    fetchServicesForAdmin,
    fetchDataForServiceForm,
    addUpdateServices
};