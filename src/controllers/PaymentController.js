"use strict";
const Payment = require("../models/Payment");
const Batch = require("../models/Batch");
const Contact = require('./../models/ContactRequest');

const {
  transaction,
  raw
} = require("objection");
const moment = require('moment');

//handle notifications
const sendNotification = require('./../notification').sendNotificationToUser;
const updateNotificationRecord = require('./NotificationController').updateNotification;

const updatePayment = async (req, res) => {
  try {

    var payment = await transaction(Payment.knex(), async trx => {
      const updatePayment = await Payment.query(trx).upsertGraph({
        payment_id: req.body.paymentId,
        amount_paid: req.body.amount,
        is_paid: true,
        payment_date: moment(req.body.date),
        id: req.body.id,
        batch: {
          paid_price: raw("paid_price + ?", req.body.amount),
          most_recent_installment: raw("most_recent_installment + ?", 1),
          id: req.body.batch_id
        }
      }, {
        noDelete: true,
        relate: true,
        unrelate: true
      }).runAfter((result, builder) => {
        console.log(builder.toString());
        return result;
      });

      console.log(updatePayment);
      // const updateBatch = await updatePayment
      //   .$relatedQuery("batch", trx)
      //   .patch({
      //     paid_price: raw("paid_price + ?", req.body.amount),
      //     most_recent_installment: raw("most_recent_installment + ?", 1)
      //   }); //object notation does not seem to work on increment with single key-value pair.

      // console.log(updateBatch);
      return updatePayment;
    });
    console.log(payment, "Success! All tables updated");
    return global.okResponse(res, payment, "payment updated");
  } catch (err) {
    console.log(err, "No tables updated");
    return global.badRequestError(res, {}, "");
  }
};

/**This function has been rendered useless by the changes made in assign vehicles api (creating empty payments according to installment count)
 * @name insertPayment
 * @description: add a new payment
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 */
const insertPayment = async (req, res) => {
  try {
    // req.user.id = 1;
    var payment = await transaction(Payment.knex(), async trx => {
      const updatePayment = await Payment.query(trx).insert({
        amount_paid: req.body.amount,
        batch_id: req.body.batchId,
        created_by: req.user.id
      });
      const updateBatch = await updatePayment
        .$relatedQuery("batch", trx)
        .increment("paid_price", req.body.amount); //object notation does not seem to work on increment with single key-value pair.
      return updateBatch;
    });
    return global.okResponse(res, payment, "Payment successful");
  } catch (err) {
    console.log(err);
    return global.badRequestError(res, {}, "Payment failed");
  }
};

/**
 * @name fetchPayments
 * @description: add a new payment
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 */
const fetchPayments = async (req, res) => {

  let payments;
  try {
    // var businesses = await Batch.query()
    //   .distinct("business_id")
    //   .eager("business")
    //   .modifyEager("business", builder => {
    //     builder.select("business_name", "mobile_number");
    //   });
    payments = await Batch.query()
      .select(
        "batch.id",
        "slug",
        "batch.business_id",
        "installment_counts",
        "installment_type",
        "most_recent_installment",
        "total_price",
        "paid_price",
        "business_name"
      )
      .leftJoinRelation("business") //to fetch the business-specific details in this query
      // .eagerAlgorithm(Batch.JoinEagerAlgorithm)//so that only those batches which have at least 1 payment are shown
      // .eagerOptions({ joinOperation: "innerJoin" })
      .eager("[payments]")
      .modifyEager("payments", builder => {
        builder
          .select(
            "id",
            "payment_id",
            "amount_paid",
            "payment_offset",
            "is_paid",
            "payment_date",
            "updated_at"
          )
          .orderBy("payment_id");
      })
      .where(builder => {
        if (req.query.keyword) {
          builder
            .where("slug", "ilike", "%" + req.query.keyword + "%")
            .orWhere("business_name", "ilike", "%" + req.query.keyword + "%");
        }
        if (req.query.businessId) {
          //why do we need this, again?
          builder.where("batch.business_id", req.query.businessId);
        }
      })
      .orderBy("business_id", "");
  } catch (err) {
    console.log(err);
  }
  return global.okResponse(res, {
    paymentList: payments
  }, "");
};


const addContactForm = async (req, res) => {
  try {
    if (!req.body.name || req.body.name == "") throw global.badRequestError("Name is required");
    if (!req.body.city || req.body.city == "") throw global.badRequestError("City is required");
    if (!req.body.number || req.body.number == "") throw global.badRequestError("Phone number is required");
    if (!req.body.formType || req.body.formType == "") throw global.badRequestError("Form type is required");
    if (!req.body.email || req.body.email == "") throw global.badRequestError("Email is required");

    let row = await Contact.query().insert({
      name: req.body.name,
      city: req.body.city,
      phone_number: req.body.number,
      form_type: req.body.formType,
      email: req.body.email
    });

    if (!row) throw global.badRequestError("Form could not be submitted. Please try again later.");

    let nfData = {
      device_type: 'WEB',
      url: 'admin/request-for-modules/',
      receiver: 1,
      time: moment(),
      title: global.NOTIFICATION_PAYLOAD.REQUEST_FOR_MODULE.title,
      body: global.NOTIFICATION_PAYLOAD.REQUEST_FOR_MODULE.body
    };
    updateNotificationRecord(nfData, [1]);

    return global.okResponse(res, {}, "Request has been submitted. Our team will get in touch with you ");

  } catch (err) {
    console.log(err);
    throw global.badRequestError("Form could not be submitted. Please try again later.");
  }
};

const fetchContactRequestsForAdmin = async (req, res) => {
  try {
    /** Set the Parameters for pagination */
    let page = (req.query.page) ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    let count = await Contact.query().count('id').where(builder => {

      if (req.query.keyword)
        return builder.where('name', 'ilike', "%" + req.query.keyword + "%");

    }).first();
    let rows = await Contact.query().orderBy('created_at', 'desc').where(builder => {

      if (req.query.keyword)
        return builder.where('name', 'ilike', "%" + req.query.keyword + "%");

    }).orderBy("created_at", "DESC").offset(offset).limit(limit);

    if (req.query.readNotice && req.query.readNotice != '') {
      let id = new Buffer(req.query.readNotice, 'base64').toString('ascii');

      updateNotificationRecord.updateNotificationReadStatus(id);
    }
    return global.okResponse(res, {
      totalCount: count.count,
      listData: rows
    }, "");

  } catch (err) {
    console.log(err);
    throw global.badRequestError("Forms could not be fetched");
  }

};

module.exports = {
  insertPayment,
  updatePayment,
  fetchPayments,
  addContactForm,
  fetchContactRequestsForAdmin
};