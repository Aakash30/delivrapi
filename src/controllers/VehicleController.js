'use strict';

const Country = require('../models/Countries');
const City = require('../models/City');
const VehicleBrand = require('../models/VehicleBrand');
const VehicleModel = require('../models/VehicleModel');
const Vehicle = require('../models/Vehicle');
const VehicleMapping = require('../models/VehicleMapping');
const BusinessBranchArea = require('../models/BusinessBranchArea');
const VehicleBatch = require('../models/Batch');
const Business = require('../models/Business');
const updateNotificationRecord = require('./NotificationController');
const Batch = require('../models/Batch');

const User = require('../models/Users');

const { transaction, ref } = require('objection');
const moment = require('moment');

let knexConfig = require('../../db/knex');
const knex = require('knex')(knexConfig[process.env.NODE_ENV || 'development']);

/**
 * @Function {fetchVehicleListForSuperAdmin}
 * @description: fetches the vehicle details list for super admin. It sends vehicle brand and vehicle model, country and city list
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */
const fetchVehicleListForSuperAdmin = async (req, res) => {
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	// fetch data for country
	let country = await Country.query();

	// fetch data for city
	let city = await City.query();

	let vehicleBrand = await VehicleBrand.query();

	let vehicleModel = await VehicleModel.query();

	let vehicle;
	try {
		vehicle = await Vehicle.query()
			.select(
				'vehicle_id',
				'vehicle_number',
				'brand_name',
				'model_name',
				'country_list.country',
				'city_list.city',
				'price',
				'vehicle.currency',
				'is_available',
				'is_on_lease',
				'usage_status'
			)
			.eager('[vehicle_mapping.Business]')
			.modifyEager('[vehicle_mapping]', (builder) => {
				return builder.select('');
			})
			.modifyEager('[vehicle_mapping.Business]', (builder) => {
				return builder.select('business_name');
			})
			.join('country_list', 'country_id', 'vehicle.country')
			.join('city_list', 'city_id', 'vehicle.city')
			.join('vehicle_brand', 'brand_id', 'brand')
			.join('vehicle_model', 'model_id', 'model')
			.where((builder) => {
				if (req.query.brand) {
					builder.where('vehicle.brand', req.query.brand);
				}
				if (req.query.model) {
					builder.where('vehicle.model', req.query.model);
				}

				if (req.query.city) {
					builder.where('vehicle.country', req.query.city);
				}

				if (req.query.country) {
					builder.where('vehicle.country', req.query.country);
				}

				if (req.query.priceFrom) {
					builder.where('vehicle.price', '>=', req.query.priceFrom);
				}

				if (req.query.priceTo) {
					builder.where('vehicle.price', '<=', req.query.priceTo);
				}
				if (req.query.usageStatus) {
					builder.where('vehicle.usage_status', req.query.usageStatus);
				}

				if (req.query.fetchFor) {
					if (req.query.fetchFor == 'ONLEASE') {
						builder.where('vehicle.is_on_lease', 'true');
					} else if (req.query.fetchFor == 'UNAVAILABLE') {
						builder.where('vehicle.is_available', 'false');
					} else {
						builder
							.where('vehicle.is_on_lease', 'false')
							.where('vehicle.is_available', 'true');
					}
				}
			})
			.where((builder) => {
				if (req.query.keyword) {
					builder.where(
						'vehicle_number',
						'ilike',
						'%' + req.query.keyword + '%'
					);
					builder.orWhere('brand_name', 'ilike', '%' + req.query.keyword + '%');
					builder.orWhere('model_name', 'ilike', '%' + req.query.keyword + '%');
				}
			})
			.offset(offset)
			.limit(limit)
			.orderBy('vehicle.created_at', 'DESC');
	} catch (error) {
		console.log(error);
	}

	let totalVehicle = await Vehicle.query()
		.count('vehicle_id')
		.join('country_list', 'country_id', 'vehicle.country')
		.join('city_list', 'city_id', 'vehicle.city')
		.join('vehicle_brand', 'brand_id', 'brand')
		.join('vehicle_model', 'model_id', 'model')
		.where((builder) => {
			if (req.query.brand) {
				builder.where('vehicle.brand', req.query.brand);
			}
			if (req.query.model) {
				builder.where('vehicle.model', req.query.model);
			}

			if (req.query.city) {
				builder.where('vehicle.country', req.query.city);
			}

			if (req.query.country) {
				builder.where('vehicle.country', req.query.country);
			}

			if (req.query.priceFrom) {
				builder.where('vehicle.price', '>=', req.query.priceFrom);
			}

			if (req.query.priceTo) {
				builder.where('vehicle.price', '<=', req.query.priceTo);
			}

			if (req.query.fetchFor) {
				if (req.query.fetchFor == 'ONLEASE') {
					builder.where('vehicle.is_on_lease', 'true');
				} else if (req.query.fetchFor == 'UNAVAILABLE') {
					builder.where('vehicle.is_available', 'false');
				} else {
					builder
						.where('vehicle.is_on_lease', 'false')
						.where('vehicle.is_available', 'true');
				}
			}
		})
		.where((builder) => {
			if (req.query.keyword) {
				builder.where('vehicle_number', 'ilike', '%' + req.query.keyword + '%');
				builder.orWhere('brand_name', 'ilike', '%' + req.query.keyword + '%');
				builder.orWhere('model_name', 'ilike', '%' + req.query.keyword + '%');
			}
		})
		.first();
	let returnData = {
		country: country,
		city: city,
		vehicleBrand: vehicleBrand,
		vehicleModel: vehicleModel,
		vehicle: vehicle,
		count: totalVehicle.count,
		page: page,
	};
	// return response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @Function {systemValueForVehicleForm}
 * @description: fetches default system value (currently these are globally defined data, later can be converted into system value table)
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */
const addEditVehicleFormData = async (req, res) => {
	let cityData = await City.query().select(
		'city_id',
		'city',
		'country_id',
		'status'
	);

	let countryData = await Country.query().select(
		'country_id',
		'country',
		'status',
		'currency'
	);

	let vehicleBrand = await VehicleBrand.query().select(
		'brand_id',
		'brand_name'
	);
	let vehicleModel = await VehicleModel.query().select(
		'model_id',
		'model_name',
		'brand_id'
	);

	let returnData = {
		starting_mechanism: global.STARTING_MECHANISM,
		cityList: cityData,
		countryList: countryData,
		vehicleBrand: vehicleBrand,
		vehicleModel: vehicleModel,
	};

	if (req.params.vehicleId) {
		let vehicleList = await Vehicle.query()
			.select(
				'vehicle_id',
				'vehicle_number',
				'engine_number',
				'brand',
				'model',
				'color',
				'engine_capacity',
				'starting_mechanism',
				'country',
				'city',
				'wheel_type',
				'tyre_type',
				'registration_card',
				'insurance',
				'price',
				'currency',
				'mileage',
				'lease_price',
				'usage_status',
				'chasing_number'
			)
			.eager('vehicle_images')
			.modifyEager('vehicle_images', (builder) => {
				builder.select('image_entry_id', 'vehicle_image');
			})
			.where('vehicle_id', req.params.vehicleId)
			.first();
		returnData.vehicleList = vehicleList;
	}

	// return response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @Function {addUpdateVehicle}
 * @description: used to add / update vehicle data. If brand not found then add new else update the id, same with the model.
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 * @param data saves the form posted data.
 *
 */
const addUpdateVehicle = async (req, res) => {
	let data = req.body;

	let message;

	/** Check for the missing field */
	if (!data.brand) {
		throw global.badRequestError('Please add Vehicle brand.');
	}

	if (!data.model) {
		throw global.badRequestError('Please add Vehicle model.');
	}

	if (!data.color) {
		throw global.badRequestError('Please enter color.');
	}

	if (!data.engine_capacity) {
		throw global.badRequestError('Please enter Engine Capacity.');
	}

	if (!data.engine_number) {
		throw global.badRequestError('Please provide Engine Number.');
	}

	if (!data.starting_mechanism) {
		throw global.badRequestError('Please select Starting Mechanism.');
	}

	if (!data.tyre_type) {
		throw global.badRequestError('Please select Tyre Type Front.');
	}

	if (!data.wheel_type) {
		throw global.badRequestError('Please select Tyre Type Rear.');
	}

	if (!data.country) {
		throw global.badRequestError('Please select country.');
	}

	if (!data.city) {
		throw global.badRequestError('Please select city.');
	}

	if (!data.price) {
		throw global.badRequestError('Please provide price for your vehicle.');
	}

	if (!data.vehicle_number) {
		throw global.badRequestError('Please provide Vehicle Number.');
	}

	if (!data.mileage) {
		throw global.badRequestError('Please enter mileage.');
	}

	/** Bellow commented code is working code waiting for the actual data to be used. */
	// if (!data.registration_card) {
	//     throw global.badRequestError("Please provide Registration Card copy.");
	// }

	// if (!data.insurance) {
	//     throw global.badRequestError("Please provide Insurance copy.");
	// }

	// if (!data.vehicle_images) {
	//     throw global.badRequestError("Please provide atleast one image for the vehicle.");
	// }

	if (!data.lease_price) {
		throw global.badRequestError('Please provide the lease price.');
	}

	if (!data.chasing_number) {
		throw global.badRequestError('Please provide the chassis number.');
	}

	if (!data.vehicle_id) {
		data.created_by = req.user.user_id;
		message = 'Vehicle has been added successfully.';
	} else {
		data.updated_by = req.user.user_id;
		message = 'Vehicle has been updated successfully.';
	}

	let vehicleUpdate;
	try {
		vehicleUpdate = await transaction(Vehicle.knex(), (trx) => {
			return Vehicle.query(trx).upsertGraph(data, {
				relate: true,
				unrelate: false,
			});
		});
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	return global.okResponse(
		res,
		{
			...vehicleUpdate,
		},
		message
	);
};

/**
 * @function {checkDuplicacy} used to check duplicate entry
 * @param {fieldToCheck} fieldToCheck defines the table column that has to be checked for unique
 * @param {valueToCheck} valueToCheck defines value input entered by the user
 * @param {id} id is defined in case of update value which stores unique id
 */
const checkDuplicacy = (fieldToCheck, valueToCheck, id) => {
	let result = Vehicle.query()
		.count('vehicle_id')
		.where(fieldToCheck, valueToCheck)
		.where((builder) => {
			if (!id) {
				builder.whereNot('vehicle_id', id);
			}
		})
		.first();

	if (result) {
		return true;
	} else {
		return false;
	}
};

/**
 * @Function {checkDuplicateEntry}
 * @description: used to check duplicate entry, will throw error when duplicacy found.
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */
const checkDuplicateEntry = async (req, res) => {
	let column, messageText;

	if (req.params.fieldToCheck == 'engine-number') {
		column = 'engine_number';
		messageText = 'Engine Number';
	} else {
		column = 'vehicle_number';
		messageText = 'Vehicle Number';
	}
	if (checkDuplicacy(column, req.query.value, req.params.vehicleId)) {
		throw global.badRequestError(messageText + ' already exists');
	}

	// return response
	return global.okResponse(res, {}, '');
};

/**
 * @Function {fetchSingleVehicleDetails}
 * @description: used to get single vehicle data.
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */
const fetchSingleVehicleDetails = async (req, res) => {
	if (!req.params.vehicleId) {
		throw global.badRequestError('Invalid or broken url');
	}
	const singleVehicle = await Vehicle.query()
		.select(
			'vehicle_id',
			'vehicle_number',
			'engine_number',
			'color',
			'engine_capacity',
			'starting_mechanism',
			'wheel_type',
			'tyre_type',
			'stroke',
			'breakfront',
			'registration_card',
			'insurance',
			'price',
			'currency',
			'mileage',
			'lease_price',
			'usage_status',
			'chasing_number',
			'currency'
		)
		.eager('[countries,cities,vehicle_brand,vehicle_model, vehicle_images]')
		.modifyEager('countries', (builder) => {
			builder.select('country');
		})
		.modifyEager('cities', (builder) => {
			builder.select('city');
		})
		.modifyEager('vehicle_brand', (builder) => {
			builder.select('brand_name');
		})
		.modifyEager('vehicle_model', (builder) => {
			builder.select('model_name');
		})
		.modifyEager('vehicle_images', (builder) => {
			builder.select('vehicle_image');
		})
		.where('vehicle_id', req.params.vehicleId)
		.first();

	// return response
	return global.okResponse(
		res,
		{
			...singleVehicle,
		},
		''
	);
};

/**
 * @Function {markAvailablityForVehicle}
 * @description: Super admin can mark whether that vehicle is available with him or not
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */
const markAvailablityForVehicle = async (req, res) => {
	if (!req.params.vehicleId) {
		throw global.badRequestError('Invalid or broken url');
	}

	let data = req.body;
	if (!data.status) {
		throw global.badRequestError('Something went wrong.');
	}
	let available = data.status == 'available' ? true : false;

	const vehiclelist = await Vehicle.query()
		.update({
			is_available: available,
		})
		.where('vehicle_id', req.params.vehicleId)
		.returning('vehicle_id');

	let returnData = {
		vehiclelist: vehiclelist,
	};

	// return response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @Function {fetchAvailableVehiclesForAdmin}
 * @description: It provides list of available & not on lease vehicle for the admin which he can assign to the business
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */

const fetchAvailableVehiclesForAdmin = async (req, res) => {
	const dateObject = new Date();
	const month =
		parseInt(dateObject.getMonth()) + 1 >= 10
			? parseInt(dateObject.getMonth()) + 1
			: '0' + (parseInt(dateObject.getMonth()) + 1);
	let today =
		dateObject.getFullYear() + '-' + month + '-' + dateObject.getDate();
	let vehicleList;
	let country;

	if (!req.params.businessId) {
		throw global.badRequestError('Invalid Request');
	}

	let businessId = new Buffer(req.params.businessId, 'base64').toString(
		'ascii'
	);

	if (isNaN(parseInt(businessId))) {
		throw global.badRequestError('Invalid Request');
	}

	let currentBusiness = await Business.query()
		.select('business_id', 'business_name')
		.where('business_id', businessId)
		.first();

	if (!currentBusiness) {
		throw global.badRequestError('Invalid Request');
	}
	try {
		if (businessId) {
			country = await BusinessBranchArea.query()
				.select('country')
				.where({
					type: 'mainbranch',
					business_id: businessId,
				})
				.first();
		}

		vehicleList = await Vehicle.query()
			.select(
				'vehicle_id',
				'price',
				'lease_price',
				'vehicle_number',
				'engine_number',
				'brand_name',
				'model_name',
				'currency'
			)
			.join('vehicle_brand', 'brand_id', 'brand')
			.join('vehicle_model', 'model_id', 'model')
			.where('is_available', 'true')
			.where('is_on_lease', 'false')
			.where((builder) => {
				if (country)
					return builder.where({
						country: country.country,
					});
			})
			.where((builder) => {
				if (req.query.keyword) {
					builder.where(
						'vehicle_number',
						'ilike',
						'%' + req.query.keyword + '%'
					);
					builder.orWhere('brand_name', 'ilike', '%' + req.query.keyword + '%');
					builder.orWhere('model_name', 'ilike', '%' + req.query.keyword + '%');
				}
			})
			.whereNotIn('vehicle.vehicle_id', (builder) => {
				builder
					.select('vehicle_mapping.vehicle_id')
					.from('vehicle_mapping')
					.where('assigned_to_business_to', '>=', moment('000000', 'HH:mm:ss'));
			})
			.runAfter((result, builder) => {
				return result;
			});
	} catch (error) {
		throw global.badRequestError('something went wrong');
	}

	let returnData = {
		vehicleList: vehicleList,
		currentBusiness: currentBusiness,
	};

	// return response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @Function {fetchAssignedVehicleForBusinessUser}
 * @description: It provides list of available vehicle for the business user
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */
const fetchAssignedVehicleForBusinessUser = async (req, res) => {
	/** Get parameters for pagination purpose */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	/** Execute query to fetch available vehicles */

	let vehicleList = await Vehicle.query()
		.select('vehicle_number', 'vehicle.vehicle_id')
		.eager('[vehicle_brand, vehicle_model, vehicle_mapping]')
		.modifyEager('vehicle_brand', (builder) => {
			builder.select('brand_name');
		})
		.modifyEager('vehicle_model', (builder) => {
			builder.select('model_name');
		})
		.modifyEager('vehicle_mapping', (builder) => {
			builder.select('');
			builder
				.eager('[Users, businessBranch]')
				.modifyEager('Users', (builder) => {
					builder
						.select(
							builder.knex().raw("CONCAT(first_name, ' ',last_name) AS driver")
						)
						.where('user_type', 'driver');
				})
				.modifyEager('businessBranch', (builder) => {
					builder.select('address_branch_name');
				});
		})
		.join('vehicle_mapping', function () {
			this.on('vehicle_mapping.vehicle_id', '=', 'vehicle.vehicle_id').andOn(
				'vehicle_mapping.business_id',
				this.knex().raw('?', [req.user.business_id])
			);
		})
		.where((builder) => {
			/** If keyword is passed */
			if (req.query.keyword) {
				builder.where('vehicle_number', 'ilike', '%' + req.query.keyword + '%');
			}

			if (req.query.brand) {
				builder.where('brand', +req.query.brand);
			}

			if (req.query.model) {
				builder.where('model', +req.query.model);
			}
		})
		.offset(offset)
		.limit(limit);

	let totalCount = await Vehicle.query()
		.count('vehicle.vehicle_id')
		.join('vehicle_mapping', function () {
			this.on('vehicle_mapping.vehicle_id', '=', 'vehicle.vehicle_id').andOn(
				'vehicle_mapping.business_id',
				this.knex().raw('?', [req.user.business_id])
			);
		})
		.where((builder) => {
			/** If keyword is passed */
			if (req.query.keyword) {
				builder.where('vehicle_number', 'ilike', '%' + req.query.keyword + '%');
			}

			if (req.query.brand) {
				builder.where('brand', +req.query.brand);
			}

			if (req.query.model) {
				builder.where('model', +req.query.model);
			}
		})
		.first();

	/** Prepare response data object */
	let returnData = {
		count: totalCount.count,
		vehicleList: vehicleList,
	};

	if (Object.keys(req.query).length == 0) {
		// fetch data for country
		let vehicleBrand = await VehicleBrand.query().select(
			'brand_id',
			'brand_name'
		);

		// fetch data for city
		let vehicleModel = await VehicleModel.query().select(
			'model_id',
			'model_name',
			'brand_id'
		);

		returnData.brand_list = vehicleBrand;
		returnData.model_list = vehicleModel;
	}

	// send response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @Function {businessVehicleDetails}
 * @description: It provides details of selected vehicle available for the business user
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */
const businessVehicleDetails = async (req, res) => {
	if (!req.params.vehicleId) {
		throw global.badRequestError('Invalid or broken link.');
	}

	let vehicleDetails;
	try {
		vehicleDetails = await Vehicle.query()
			.select(
				'vehicle_number',
				'vehicle.vehicle_id',
				'color',
				'engine_capacity',
				'engine_number',
				'starting_mechanism',
				'tyre_type',
				'wheel_type',
				'stroke',
				'breakfront',
				'price',
				'vehicle.currency',
				'mileage',
				'registration_card',
				'insurance'
			)
			.eager('[vehicle_brand, vehicle_model, vehicle_mapping, vehicle_images]')
			.modifyEager('vehicle_brand', (builder) => {
				builder.select('brand_name');
			})
			.modifyEager('vehicle_model', (builder) => {
				builder.select('model_name');
			})
			.modifyEager('vehicle_images', (builder) => {
				builder.select('vehicle_image');
			})
			.modifyEager('vehicle_mapping', (builder) => {
				builder.select(
					'assigned_to_business_from',
					'assigned_to_business_to',
					'map_id',
					'business_area_id'
				);
				builder
					.eager('[Users, batch]')
					.modifyEager('Users', (builder) => {
						builder.select(
							builder
								.knex()
								.raw(
									"CONCAT(first_name, ' ',last_name) AS driver, mobile_number, email_id"
								)
						);
					})
					.modifyEager('batch', (builder) => {
						builder.select(
							'assigned_to_business_from',
							'assigned_to_business_to'
						);
					});
			})
			.joinRelation('vehicle_mapping', function () {
				this.on('vehicle_mapping.vehicle_id', '=', 'vehicle.vehicle_id').andOn(
					'vehicle_mapping.business_id',
					this.knex().raw('?', [req.user.business_id])
				);
			})
			.where('vehicle.vehicle_id', req.params.vehicleId)
			.first();
	} catch (error) {
		console.log(error);
	}

	let myBranches = await BusinessBranchArea.query()
		.select('branch_area_id', 'address_branch_name')
		.where('business_id', req.user.business_id);

	/** Prepare response */

	let responseData = {
		vehicleDetails,
		myBranches,
	};
	/**send response */
	return global.okResponse(
		res,
		{
			...responseData,
		},
		''
	);
};

/**
 * This function is used to assign vehicles in business internal to their sub branches
 * @param {*} req
 * @param {*} res
 */
const assignVehiclesToBusinessBranch = async (req, res) => {
	let data = req.body;

	if (!data.map_id) {
		throw global.badRequestError('Invalid Request');
	}

	if (!data.business_area_id) {
		throw global.badRequestError('No Branch was selected');
	}

	let updateVehicleAssignement = await VehicleMapping.query()
		.update({
			business_area_id: data.business_area_id,
		})
		.whereIn('map_id', data.map_id)
		.where('business_id', req.user.business_id);

	if (!updateVehicleAssignement) {
		throw global.badRequestError('Something went wrong.');
	}

	/**send response */
	return global.okResponse(
		res,
		{
			...updateVehicleAssignement,
		},
		'Branch assigned successfully'
	);
};

/** data for inventory list
 *
 */
const fetchInventoryData = async (req, res) => {
	/** Get parameters for pagination purpose */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	// fetch prefill data
	let country = await Country.query();
	let city = await City.query();
	let vehicleBrand = await VehicleBrand.query();
	let vehicleModel = await VehicleModel.query();

	let inventoryData = await Vehicle.query()
		.select(
			Vehicle.knex().raw(
				"count(vehicle_id) filter(WHERE is_available='true' and is_on_lease='false') AS quantity, count(vehicle_id) filter(WHERE is_on_lease='true') AS on_lease"
			)
		)
		.eager('[vehicle_brand, vehicle_model, cities, countries]')
		.joinRelation('vehicle_brand')
		.joinRelation('vehicle_model')
		.modifyEager('vehicle_brand', (builder) => {
			builder.select('brand_name');
		})
		.modifyEager('vehicle_model', (builder) => {
			builder.select('model_name');
		})
		.modifyEager('cities', (builder) => {
			builder.select('city');
		})
		.modifyEager('countries', (builder) => {
			builder.select('country');
		})
		.where((builder) => {
			if (req.query.keyword) {
				builder
					.where('brand_name', 'ilike', '%' + req.query.keyword + '%')
					.orWhere('model_name', 'ilike', '%' + req.query.keyword + '%');
			}
			if (req.query.brand) {
				builder.where('brand', req.query.brand);
			}

			if (req.query.model) {
				builder.where('model', req.query.model);
			}

			if (req.query.country) {
				builder.where('vehicle.country', req.query.country);
			}

			if (req.query.city) {
				builder.where('vehicle.city', req.query.city);
			}

			if (req.query.priceFrom) {
				builder.where('vehicle.price', '>=', req.query.priceFrom);
			}

			if (req.query.priceTo) {
				builder.where('vehicle.price', '<=', req.query.priceTo);
			}
			if (req.query.usageStatus) {
				builder.where('vehicle.usage_status', req.query.usageStatus);
			}
		})
		.groupBy('model')
		.groupBy('brand')
		.groupBy('city')
		.groupBy('country')
		.offset(offset)
		.limit(limit);

	let totalCount = await Vehicle.query()
		.eager('[vehicle_brand, vehicle_model, cities, countries]')
		.joinRelation('vehicle_brand')
		.joinRelation('vehicle_model')
		.count('vehicle_id')
		.where((builder) => {
			if (req.query.keyword) {
				builder
					.where('brand_name', 'ilike', '%' + req.query.keyword + '%')
					.orWhere('model_name', 'ilike', '%' + req.query.keyword + '%');
			}
			if (req.query.brand) {
				builder.where('brand', req.query.brand);
			}

			if (req.query.model) {
				builder.where('model', req.query.model);
			}

			if (req.query.country) {
				builder.where('vehicle.country', req.query.country);
			}

			if (req.query.city) {
				builder.where('vehicle.city', req.query.city);
			}

			if (req.query.priceFrom) {
				builder.where('vehicle.price', '>=', req.query.priceFrom);
			}

			if (req.query.priceTo) {
				builder.where('vehicle.price', '<=', req.query.priceTo);
			}
			if (req.query.usageStatus) {
				builder.where('vehicle.usage_status', req.query.usageStatus);
			}
		})
		.groupBy('model')
		.groupBy('brand')
		.groupBy('city')
		.groupBy('country');

	let resultData = {
		country,
		city,
		vehicleBrand,
		vehicleModel,
		inventoryData,
		count: totalCount ? totalCount.length : 0,
	};

	/**send response */
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

/**
 * fetch lease plan for business
 * @param {*} req
 * @param {*} res
 */
const fetchLeasePlan = async (req, res) => {
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	let leasePlanData;
	try {
		leasePlanData = await VehicleBatch.query()
			.select([
				VehicleMapping.query()
					.count('map_id')
					.where('batch_id', ref('batch.id'))
					.groupBy('batch_id')
					.as('vehicleCount'),
				'id',
				'slug',
				'assigned_to_business_from',
				'assigned_to_business_to',
				'total_price',
				'paid_price',
			])
			.mergeNaiveEager('vehicles')
			.modifyEager('vehicles', (builder) => {
				builder.select('currency').offset(0).limit(1);
			})
			.where('business_id', req.user.business_id)
			.where((builder) => {
				if (req.query.keyword) {
					builder.where('slug', 'ilike', '%' + req.query.keyword + '%');
				}

				if (req.query.start_date) {
					let dateObject = moment(req.query.start_date);
					builder.where('assigned_to_business_from', '>=', dateObject);
				}

				if (req.query.end_date) {
					let dateObject = moment(req.query.end_date);
					builder.where('assigned_to_business_to', '<=', dateObject);
				}
			})
			.offset(offset)
			.limit(limit);
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	let totalCount = await VehicleBatch.query()
		.count('id')
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.query.keyword) {
				builder.where('slug', 'ilike', '%' + req.query.keyword + '%');
			}

			if (req.query.start_date) {
				let dateObject = moment(req.query.start_date);
				builder.where('assigned_to_business_from', '>=', dateObject);
			}

			if (req.query.end_date) {
				let dateObject = moment(req.query.end_date);
				builder.where('assigned_to_business_to', '<=', dateObject);
			}
		})
		.first();
	let resultData = {
		leasePlanData,
		total: totalCount.count,
	};

	/**send response */
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

/**
 * get vehicle details for batch id
 * @param {*} req
 * @param {*} res
 */

const getBatchDetails = async (req, res) => {
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	if (!req.params.id) {
		throw global.badRequestError('Invalid Request');
	}
	let batchData = await VehicleMapping.query()
		.select('map_id')
		.eager('vehicle')
		.modifyEager('vehicle', (builder) => {
			builder
				.select(
					'vehicle_number',
					'brand_name',
					'model_name',
					'engine_number',
					'currency'
				)
				.joinRelation('vehicle_brand')
				.joinRelation('vehicle_model');
		})
		.joinRelation('vehicle')
		.where('batch_id', req.params.id)
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.query.keyword) {
				builder.where((builder2) => {
					builder2
						.where('vehicle_number', 'ilike', '%' + req.query.keyword + '%')
						.orWhere('engine_number', 'ilike', '%' + req.query.keyword + '%');
				});
			}

			if (req.query.brand) {
				builder.where('brand', req.query.brand);
			}

			if (req.query.model) {
				builder.where('model', req.query.model);
			}
		});

	let batchDetails = await VehicleBatch.query()
		.select(
			'slug',
			'assigned_to_business_from',
			'assigned_to_business_to',
			'total_price',
			'paid_price'
		)
		.eager('[vehicleMapping]')
		.modifyEager('vehicleMapping', (builder) => {
			builder.count('map_id').groupBy('batch_id');
		})
		.where('id', req.params.id)
		.where('business_id', req.user.business_id)
		.first();

	let resultData = {
		batchData,
		batchDetails,
	};

	if (
		Object.keys(req.query).length == 0 ||
		(req.query.readNotice && req.query.readNotice != '')
	) {
		let vehicleBrand = await VehicleBrand.query().select(
			'brand_id',
			'brand_name'
		);

		let vehicleModel = await VehicleModel.query().select(
			'model_id',
			'model_name',
			'brand_id'
		);

		resultData.brand_list = vehicleBrand;
		resultData.model_list = vehicleModel;
	}

	if (req.query.readNotice && req.query.readNotice != '') {
		let id = new Buffer(req.query.readNotice, 'base64').toString('ascii');

		updateNotificationRecord.updateNotificationReadStatus(id);
	}

	/**send response */
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

/**
 * @Function {fetchAssignedVehicleForBusinessUser}
 * @description: It provides list of available vehicle for the business user which will be used to assign to the branch. It shows vehicles available in main branch
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */
const fetchAvailableVehicleForBusiness = async (req, res) => {
	let vehicleList;
	let country;

	if (!req.params.branchId) {
		throw global.badRequestError('Invalid Request');
	}

	let branchId = new Buffer(req.params.branchId, 'base64').toString('ascii');

	if (isNaN(parseInt(branchId))) {
		throw global.badRequestError('Invalid Requests');
	}

	let currentBranch = await BusinessBranchArea.query()
		.select('branch_area_id', 'address_branch_name')
		.where('business_id', req.user.business_id)
		.where('branch_area_id', branchId)
		.first();

	if (!currentBranch) {
		throw global.badRequestError('Invalid Request2');
	}

	let mainBranch = await BusinessBranchArea.query()
		.select('branch_area_id')
		.where('business_id', req.user.business_id)
		.where('type', 'mainbranch')
		.first();

	vehicleList = await Vehicle.query()
		.select(
			'map_id',
			'vehicle.vehicle_id',
			'price',
			'vehicle_number',
			'engine_number',
			'brand_name',
			'model_name',
			'currency'
		)
		.join('vehicle_brand', 'brand_id', 'brand')
		.join('vehicle_model', 'model_id', 'model')
		.innerJoin('vehicle_mapping', (builder) => {
			builder
				.on('vehicle_mapping.vehicle_id', '=', 'vehicle.vehicle_id')
				.on('vehicle_mapping.business_id', '=', req.user.business_id)
				.on('vehicle_mapping.business_area_id', '=', mainBranch.branch_area_id);
		})
		.whereIn('vehicle.vehicle_id', (builder) => {
			builder
				.select('vehicle_mapping.vehicle_id')
				.from('vehicle_mapping')
				.where('business_id', req.user.business_id)
				.where('business_area_id', mainBranch.branch_area_id);
		})
		.where((builder) => {
			if (req.query.keyword) {
				builder.where('vehicle_number', 'ilike', '%' + req.query.keyword + '%');
				builder.orWhere('brand_name', 'ilike', '%' + req.query.keyword + '%');
				builder.orWhere('model_name', 'ilike', '%' + req.query.keyword + '%');
			}
		})
		.where('driver_id', null)
		.runAfter((result, builder) => {
			return result;
		});

	let returnData = {
		vehicleList: vehicleList,
		currentBranch: currentBranch,
	};

	// send response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @Function fetchVehiclesForBatchRenewal
 * @description: fetches a list of assignable (available & not on lease) vehicles along with the ones assigned in this batch
 **/
const fetchVehiclesForBatchRenewal = async (req, res) => {
	try {
		let vehicleList, country;

		if (!req.params.batchId) {
			throw global.badRequestError('Invalid Request');
		}

		let businessId = new Buffer(req.params.businessId, 'base64').toString(
			'ascii'
		);
		let batchId = new Buffer(req.params.batchId, 'base64').toString('ascii');

		if (isNaN(parseInt(businessId))) {
			throw global.badRequestError('Invalid Request');
		}
		if (isNaN(parseInt(batchId))) {
			throw global.badRequestError('Invalid Request');
		}
		let currentBusiness = await Business.query()
			.select('business_id', 'business_name')
			.where('business_id', businessId)
			.first();

		if (!currentBusiness) {
			throw global.badRequestError('Invalid Request');
		}
		try {
			if (businessId) {
				country = await BusinessBranchArea.query()
					.select('country')
					.where({
						type: 'mainbranch',
						business_id: businessId,
					})
					.first();
			}

			vehicleList = await Vehicle.query()
				.select(
					'vehicle.vehicle_id',
					'price',
					'vehicle_number',
					'engine_number',
					'brand_name',
					'model_name',
					'currency',
					'lease_price',
					knex.raw(
						'COALESCE(vehicle_mapping.batch_id=' +
							batchId +
							', false) as "isBatchVehicle"'
					)
				)
				.leftJoin(
					'vehicle_mapping',
					'vehicle_mapping.vehicle_id',
					'vehicle.vehicle_id'
				)
				.join('vehicle_brand', 'brand_id', 'brand')
				.join('vehicle_model', 'model_id', 'model')
				.where('is_available', 'true')
				//   .where("is_on_lease", "false")
				.where((builder) => {
					if (country)
						return builder.where({
							country: country.country,
						});
				})
				.where((builder) => {
					if (req.query.keyword) {
						builder.where(
							'vehicle_number',
							'ilike',
							'%' + req.query.keyword + '%'
						);
						builder.orWhere(
							'brand_name',
							'ilike',
							'%' + req.query.keyword + '%'
						);
						builder.orWhere(
							'model_name',
							'ilike',
							'%' + req.query.keyword + '%'
						);
					}
				})
				.whereNotIn('vehicle.vehicle_id', (builder) => {
					builder
						.select('vehicle_mapping.vehicle_id')
						.from('vehicle_mapping')
						.whereNot('batch_id', batchId);
					//   .where("assigned_to_business_to",">=",moment("000000", "HH:mm:ss"));
				})
				.runAfter((result, builder) => {
					return result;
				});
		} catch (error) {
			throw global.badRequestError('something went wrong');
		}

		let returnData = {
			vehicleList: vehicleList,
			currentBusiness: currentBusiness,
		};

		// return response
		return global.okResponse(
			res,
			{
				...returnData,
			},
			''
		);
	} catch (error) {
		console.log(error);
		throw global.badRequestError('Bad Implementation');
	}
};

const renewBatch = async (req, res) => {
	let data = req.body;

	if (!data.batch_id) {
		throw global.badRequestError('Invalid Request');
	}
	if (!req.params.businessId) {
		throw global.badRequestError('Invalid request.');
	}

	if (!data.vehicleList) {
		throw global.badRequestError('Select atleast one vehicle.');
	}

	if (!data.startDate) {
		throw global.badRequestError('Select the lease time duration.');
	}

	if (!data.endDate) {
		throw global.badRequestError('Select the lease time duration.');
	}

	if (data.vehicleList.length == 0) {
		throw global.badRequestError('Select atleast one vehicle.');
	}

	let businessId = new Buffer(req.params.businessId, 'base64').toString(
		'ascii'
	);
	let batchId = new Buffer(data.batch_id, 'base64').toString('ascii');

	if (isNaN(parseInt(businessId))) {
		throw global.badRequestError('Invalid Request1');
	}
	if (isNaN(parseInt(batchId))) {
		throw global.badRequestError('Invalid Request2');
	}

	var vehicleInput = [],
		payments = [];
	let vehicleTrackInput = [];

	let defaultAddress = await BusinessBranchArea.query()
		.select('branch_area_id')
		.where('business_id', businessId)
		.where('type', 'mainbranch')
		.first();

	//create empty rows in payments table according to payment count
	for (let i = 0; i < data.installment_count; i++) {
		payments.push({
			amount_paid: 0,
			created_by: req.user.user_id,
			payment_id: 'IN' + Date.now().toString() + i, //Date.now() alone creates the same payment id, so 'i' was appended.
			payment_offset:
				i == data.installment_count - 1 ? data.payment_offset : '',
		});
	}
	data.vehicleList.forEach((element) => {
		vehicleInput.push({
			vehicle_id: element,
			business_id: businessId,
			assigned_to_business_from: data.startDate,
			assigned_to_business_to: data.endDate,
			created_by: req.user.user_id,
			business_area_id: defaultAddress.branch_area_id,
			vehicle: {
				is_on_lease: true,
				updated_by: req.user.user_id,
				vehicle_id: element,
			},
		});
		vehicleTrackInput.push({
			vehicle_id: element,
			business_id: businessId,
			assinged_by: req.user.user_id,
			assigned_on: new Date().toISOString(),
		});
	});

	let batchInsert = {
		business_id: businessId,
		slug: parseInt(Date.now()),
		paid_price: 0,
		installment_type: req.body.installment_type, //1=monthly, 2=quarterly, 3=half-yearly
		installment_counts: req.body.installment_count,
		assigned_to_business_from: req.body.startDate,
		assigned_to_business_to: req.body.endDate,
		total_price: req.body.totalPrice,
		vehicleMapping: vehicleInput,
		payments: payments,
	};

	let vehicleAssign;
	try {
		vehicleAssign = await transaction(Batch.knex(), async (trx) => {
			let vehicleData = await Vehicle.query()
				.update({
					is_on_lease: false,
				})
				.whereIn(
					'vehicle_id',
					VehicleMapping.query().select('vehicle_id').where('batch_id', batchId)
				)
				.whereNotIn('vehicle_id', data.vehicleList);
			let updateOldBatch = await Batch.query()
				.update({
					is_renew: true,
				})
				.where('id', batchId);

			let batch = await Batch.query()
				.upsertGraph(batchInsert, {
					relate: true,
					unrelate: true,
				})
				.eager('[vehicleMapping]')
				.modifyEager('vehicleMapping', (builder) => {
					builder.eager('vehicle');
				});
			return batch;
		});

		if (vehicleAssign) {
			return global.okResponse(res, {}, 'Batch Renewed successfully.');
		}
	} catch (error) {
		throw global.badRequestError(error.message);
	}
};

module.exports = {
	fetchVehicleListForSuperAdmin,
	fetchVehiclesForBatchRenewal,
	addEditVehicleFormData,
	addUpdateVehicle,
	checkDuplicateEntry,
	fetchSingleVehicleDetails,
	markAvailablityForVehicle,
	fetchAvailableVehiclesForAdmin,
	fetchAssignedVehicleForBusinessUser,
	businessVehicleDetails,
	assignVehiclesToBusinessBranch,
	fetchInventoryData,
	fetchLeasePlan,
	getBatchDetails,
	fetchAvailableVehicleForBusiness,
	renewBatch,
};
