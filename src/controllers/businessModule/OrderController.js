'use strict';

const { transaction, ref } = require('objection');
const moment = require('moment');

// const jwt= require('jsonwebtoken');

const Order = require('./../../models/Order');
const Item = require('../../models/OrderItem');
const Users = require('./../../models/Users');
const Country = require('./../../models/Countries');
const City = require('./../../models/City');
const BusinessBranch = require('./../../models/BusinessBranchArea');

const RejectedOrders = require('./../../models/RejectedOrders');

const Notifications = require('../../notification');
const updateNotificationRecord = require('./../NotificationController');
const sendSMS = require('../../middlewares/sms').sendSms;
const sendSms_twilio = require('../../middlewares/sms').sendSms_twilio;

/**
 * @function: addorder
 * @description: add oder data
 * @param {*} req stores the requested parameters
 * @param {*} res stores the response values
 */

const addUpdateOrderData = async (req, res) => {
	/** Get post data from the form */
	let data = req.body;

	/** Check form emty field */

	if (!data.first_name || data.first_name.trim() == '') {
		throw global.badRequestError("Please enter customer's first name.");
	}

	if (!data.last_name || data.last_name.trim() == '') {
		throw global.badRequestError("Please enter customer's last name.");
	}

	if (!data.email || data.email.trim() == '') {
		throw global.badRequestError('Please enter email id.');
	}

	if (!data.channel) {
		throw global.badRequestError('Please enter channel.');
	}

	if (!data.country) {
		throw global.badRequestError('Please select country.');
	}

	if (!data.city) {
		throw global.badRequestError('Please select city.');
	}

	if (!data.address_1) {
		throw global.badRequestError('Please enter street address.');
	}

	if (!data.address_2) {
		throw global.badRequestError('Please enter address.');
	}

	if (!data.postal_code) {
		throw global.badRequestError('Please enter postal code.');
	}

	if (!data.payment_method) {
		throw global.badRequestError('Please select payment method.');
	}

	if (!data.order_date) {
		throw global.badRequestError('Please enter orde date.');
	} else {
		// let dateObject = moment(data.order_date).format("YYYY-MM-DD hh:mm:ss");
		// console.log(dateObject);
		// console.log(data.order_date)
		// data.order_date = dateObject;
	}

	if (!data.OrderItem) {
		throw global.badRequestError('Please add items for your order.');
	}

	if (data.OrderItem.length == 0) {
		throw global.badRequestError('Please add atleast one item for your order.');
	} else {
		for (let i = 0; i < data.OrderItem.length; i++) {
			if (data.OrderItem[i].item_id == null) {
				delete data.OrderItem[i].item_id;
			}
		}
	}

	let insertOrder, message;

	if (!req.params.orderId) {
		let otp = Math.floor(1000 + Math.random() * 9000);

		message = 'Order has been added successfully.';

		data.OTP = otp;
		data.business_id = req.user.business_id;
		data.created_by = req.user.user_id;
	} else {
		data.order_id = req.params.orderId;

		message = 'Order details has been updated successfully';
	}

	try {
		insertOrder = await transaction(Order.knex(), (trx) => {
			return Order.query(trx)
				.context({
					business_id: req.user.business_id,
					created_by: req.user.user_id,
				})
				.upsertGraph(data, {
					relate: true,
					unrelate: false,
				});
		});
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	/** If query is not executed throw error */
	if (!insertOrder) {
		throw global.badRequestError('Something went wrong.');
	}

	//If insert then send sms

	// sendSMS(data.mobile_number.replace("+", "").replace("-", ""), "Your Order Code is " + data.OTP + ". Share this code with your driver to confirm delivery.");
	sendSms_twilio(
		data.mobile_number.replace('+', '').replace('-', ''),
		'Your Order Code is ' +
			data.OTP +
			'. Share this code with your driver to confirm delivery.'
	);
	/** Send response */
	return global.okResponse(
		res,
		{
			...insertOrder,
		},
		message
	);
};

/**
 * @function: fetchSingleOrderDetails
 * @description: fetch the single detail of order
 * @param {*} req
 * @param {*} res
 */

const fetchSingleOrderDetails = async (req, res) => {
	/** If Order id is not passed in the url then throw error. */
	if (!req.params.orderId) {
		throw global.badRequestError('Invalid URL!');
	}

	let orderId = new Buffer(req.params.orderId, 'base64').toString('ascii');

	if (isNaN(parseInt(orderId))) {
		throw global.notFoundError('Page you are requested does not exists');
	}
	// query execute and then get the data of single order

	let singleOrder = await Order.query()
		.select(
			'order_id',
			'invoice_id',
			'first_name',
			'last_name',
			'email',
			'mobile_number',
			'order_date',
			'channel',
			'address_1',
			'address_2',
			'postal_code',
			'order_status',
			'subtotal',
			'payment_method',
			'customer_signature',
			'rating'
		)
		.eager('[countries, cities, OrderItem, driver]')
		.modifyEager('countries', (builder) => {
			builder.select('country', 'currency');
		})
		.modifyEager('cities', (builder) => {
			builder.select('city');
		})
		.modifyEager('OrderItem', (builder) => {
			builder
				.select('item_name', 'quantity', 'price')
				.orderBy('item_id', 'ASC');
		})
		.modifyEager('driver', (builder) => {
			builder
				.select(
					'first_name',
					'last_name',
					'mobile_number',
					'email_id',
					'user_id'
				)
				.eager('orders')
				.modifyEager('orders', (builder) => {
					builder
						.count('order_id')
						.avg('rating')
						.where('order_status', 'delivered')
						.groupBy('driver_id');
				});
		})
		.where('order_id', orderId)
		.where('business_id', req.user.business_id)
		.first();

	if (!singleOrder) {
		throw global.notFoundError('The requested order not found.');
	}

	if (req.query.readNotice && req.query.readNotice != '') {
		let id = new Buffer(req.query.readNotice, 'base64').toString('ascii');

		updateNotificationRecord.updateNotificationReadStatus(id);
	}
	/** prepare response data object */

	let returnData = {
		orderDetail: singleOrder,
	};
	// send response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @function: deleteData
 * @description: delete single data
 * @param {*} req
 * @param {*} res
 */

const deleteData = async (req, res) => {
	let message;
	let deleteorderdata;

	if (req.params.orderId) {
		deleteorderdata = await Order.query()
			.where('order_id', req.params.orderId)
			.del();
		message = 'Order data has been deleted';
	}

	if (!deleteorderdata) {
		throw global.badRequestError('Something went wrong.');
	}

	/** Send response */
	return global.okResponse(
		res,
		{
			...deleteorderdata,
		},
		message
	);
};

/**
 * @function: fetchOrderList
 * @description: Provides order list which belongs to the order
 * @param {*} req
 * @param {*} res
 */
const fetchOrderList = async (req, res) => {
	/** Get parameters for pagination purpose */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	// execute query to fetch data
	let orderList;
	try {
		orderList = await Order.query()
			.select(
				'order_id',
				Order.knex().raw(
					"order_id, invoice_id, CONCAT(first_name, ' ', last_name) AS customer,order_date,channel,address_1,order_status, CASE WHEN order_status = 'new' THEN 0 ELSE 1 END AS order_priority"
				)
			)
			.where('business_id', req.user.business_id)
			.eager('OrderItem')
			.modifyEager('OrderItem', (builder) => {
				builder.sum('quantity').groupBy('item_list.order_id');
			})
			.where((builder) => {
				if (req.user.user_type == 'sub_ordinate') {
					builder.where('business_branch_id', req.user.business_branch_id);
				}

				if (req.query.status) {
					builder.where('order_status', req.query.status);
				}

				if (req.query.keyword) {
					builder.where((builder) => {
						builder
							.where('first_name', 'ilike', '%' + req.query.keyword + '%')
							.orWhere('last_name', 'ilike', '%' + req.query.keyword + '%')
							.orWhere('invoice_id', 'ilike', '%' + req.query.keyword + '%');
					});
				}
				if (req.query.start_date && !req.query.end_date) {
					let dateObject = moment(req.query.start_date).format('YYYY-MM-DD');

					builder.whereRaw('DATE(order_date) >=?', [dateObject]);
				}

				if (req.query.end_date && !req.query.start_date) {
					let dateObject = moment(req.query.end_date).format('YYYY-MM-DD');

					builder.whereRaw('DATE(order_date) <= ? ', [dateObject]);
				}

				if (req.query.end_date && req.query.start_date) {
					if (req.query.end_date == req.query.start_date) {
						let dateObject = moment(req.query.end_date).format('YYYY-MM-DD');

						builder.whereRaw('DATE(order_date) = ?', [dateObject]);
					} else {
						let dateObject = moment(req.query.start_date).format('YYYY-MM-DD');
						let dateObjectEnd = moment(req.query.end_date).format('YYYY-MM-DD');

						builder
							.whereRaw('DATE(order_date) >= ?', [dateObject])
							.whereRaw('DATE(order_date) <= ? ', [dateObjectEnd]);
					}
				}

				if (req.query.channel) {
					builder.where('channel', req.query.channel);
				}
			})
			.offset(offset)
			.limit(limit)
			.orderBy('order_priority')
			.orderBy('order_date', 'DESC')
			.runAfter((result, builder) => {
				return result;
			});
	} catch (error) {
		console.log(error.message);
	}

	// get the total driver count
	let totalOrder = await Order.query()
		.count('order_id')
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.user.user_type == 'sub_ordinate') {
				builder.where('business_branch_id', req.user.business_branch_id);
			}

			if (req.query.status) {
				builder.where('order_status', req.query.status);
			}

			if (req.query.keyword) {
				builder.where((builder) => {
					builder
						.where('first_name', 'ilike', '%' + req.query.keyword + '%')
						.orWhere('last_name', 'ilike', '%' + req.query.keyword + '%')
						.orWhere('invoice_id', 'ilike', '%' + req.query.keyword + '%');
				});
			}
			if (req.query.start_date && !req.query.end_date) {
				let dateObject = moment(req.query.start_date).format('YYYY-MM-DD');

				builder.whereRaw('DATE(order_date) >=?', [dateObject]);
			}

			if (req.query.end_date && !req.query.start_date) {
				let dateObject = moment(req.query.end_date).format('YYYY-MM-DD');

				builder.whereRaw('DATE(order_date) <= ? ', [dateObject]);
			}

			if (req.query.end_date && req.query.start_date) {
				if (req.query.end_date == req.query.start_date) {
					let dateObject = moment(req.query.end_date).format('YYYY-MM-DD');

					builder.whereRaw('DATE(order_date) = ?', [dateObject]);
				} else {
					let dateObject = moment(req.query.start_date).format('YYYY-MM-DD');
					let dateObjectEnd = moment(req.query.end_date).format('YYYY-MM-DD');

					builder
						.whereRaw('DATE(order_date) >= ?', [dateObject])
						.whereRaw('DATE(order_date) <= ? ', [dateObjectEnd]);
				}
			}

			if (req.query.channel) {
				builder.where('channel', req.query.channel);
			}
		})
		.first();

	/** Prepare response data object */
	let returnData = {
		total: totalOrder.count,
		orderList: orderList,
		page: page,
		channelList: global.CHANNEL,
	};

	// send response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

const fetchDataForAddEditOrders = async (req, res) => {
	let country, city, businessBranchList;

	// fetch data for country
	country = await Country.query().select(
		'country_id',
		'country',
		'status',
		'country_code',
		'alpha_code',
		'currency'
	);

	// fetch data for city
	city = await City.query().select('city_id', 'country_id', 'city', 'status');

	businessBranchList = await BusinessBranch.query()
		.select(
			'branch_area_id',
			'address_branch_name',
			'country',
			'city',
			'status'
		)
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.user.user_type == 'sub_ordinate') {
				builder.where('branch_area_id', req.user.business_branch_id);
			}
		});

	/** Prepare response data object */
	let returnData = {
		country,
		city,
		businessBranchList,
		channelList: global.CHANNEL,
		paymentMethods: global.PAYMENTMEHTODS,
	};

	if (req.params.orderId) {
		// query execute and then get the data of single order

		let singleOrder = await Order.query()
			.select(
				'order_id',
				'invoice_id',
				'first_name',
				'last_name',
				'email',
				'mobile_number',
				'order_date',
				'channel',
				'address_1',
				'address_2',
				'postal_code',
				'order_status',
				'subtotal',
				'payment_method',
				'latitude',
				'longitude',
				'business_branch_id',
				'country',
				'city'
			)
			.eager('[OrderItem, driver]')
			.modifyEager('OrderItem', (builder) => {
				builder
					.select('item_id', 'item_name', 'quantity', 'price')
					.orderBy('item_id', 'ASC');
			})
			.modifyEager('driver', (builder) => {
				builder.select('first_name', 'last_name', 'mobile_number', 'email_id');
			})
			.where('order_id', req.params.orderId)
			.where('business_id', req.user.business_id)
			.first();

		returnData.orders = singleOrder;
	}

	// send response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @function: fetchDriverList
 * @description: Provides driver list for business
 * @param {*} req
 * @param {*} res
 */

const fetchDriverList = async (req, res) => {
	const driverList = await Users.query()
		.select(Users.knex().raw('user_id,first_name,last_name,user_type'))
		.join('order', 'order.driver_id', 'user_login.user_id')
		.where('user_type', 'driver')
		.where('order_status', 'accepted')
		.where('order_status', 'assigned')
		.where('user_id', 1);

	/** Prepare response data object */
	let returnData = {
		driverList: driverList,
	};

	// send response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};
/**
 * fetches driver list for assigning orders
 * @param {*} req
 * @param {*} res
 */
const fetchAvailableDriverForOrder = async (req, res) => {
	if (!req.query.order_id) {
		throw global.badRequestError('Invalid Request');
	}
	let driverList, order_data;
	try {
		driverList = await Users.query()
			.select('user_id', 'user_login.first_name', 'user_login.mobile_number')
			.count('order_id as order_count')
			.leftJoinRelation('orders')
			.eager('[tracker, orders]')
			.modifyEager('tracker', (builder) => {
				builder.select('driver_latitude', 'driver_longitude');
			})
			.modifyEager('orders', (builder) => {
				builder
					.select(
						builder
							.knex()
							.raw(
								"count(order_id) filter (where order_status = 'assigned') AS assigned_count, count(order_id) filter (where order_status = 'accepted') AS accepted_count, array_agg(order_id) AS orderlist"
							)
					)
					.where('order_status', '!=', 'delivered')
					.groupBy('driver_id');
			})
			.where('user_type', 'driver')
			.where('user_login.business_id', req.user.business_id)
			.where('user_status', 'active')
			.where((builder) => {
				//builder.where("order_status", "!=", "delivered").orWhereNotExists(Users.relatedQuery("orders"));
			})
			.whereNotIn(
				'user_id',
				RejectedOrders.query()
					.select('driver_id')
					.where('order_id', req.query.order_id)
					.where('driver_id', ref('user_login.user_id'))
			)
			.where('is_available', true)
			.groupBy('user_login.user_id')
			.runAfter((result, builder) => {
				return result;
			});

		order_data = await Order.query()
			.select('latitude', 'longitude')
			.where('order_id', req.query.order_id);
	} catch (error) {
		console.log(error.message);
	}

	/** Prepare response data object */
	let returnData = {
		driverList: driverList,
		order_data,
	};
	// send response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @function: assignOrderToDriver
 * @description: assign order to the driver
 * @param {*} req stores the requested parameters
 * @param {*} res stores the response values
 */

const assignOrderToDriver = async (req, res) => {
	let data = req.body;

	if (!data.driver_id) {
		throw global.badRequestError('Invalid Url!!');
	}

	if (!data.order_id) {
		throw global.badRequestError('Invalid Url!!');
	}

	let assignDriver = await Order.query()
		.update({
			order_status: 'assigned',
			driver_id: data.driver_id,
		})
		.where('order_id', data.order_id)
		.returning('*');

	if (!assignDriver) {
		throw global.badRequestError('Something went wrong');
	}

	let getDriverData = await Users.query()
		.select('device_token', 'device_type')
		.where('user_id', data.driver_id)
		.first();

	let getNotificationMessage = Object.assign(
		{},
		global.NOTIFICATION_PAYLOAD.ORDER_ASSIGNED
	);

	let updatedMessage = getNotificationMessage.body.replace(
		'[order_id]',
		assignDriver[0].invoice_id
	);

	getNotificationMessage.body = updatedMessage;

	/**
	 * prepare notification data to save in database
	 */
	let nfData = [];
	nfData.push({
		device_type: 'Phone',
		url: 'ORDER',
		receiver_id: data.driver_id,
		time: moment(),
		title: getNotificationMessage.title,
		body: getNotificationMessage.body,
		notification_type: 'ORDER_ASSIGNED',
		unique_id: data.order_id,
		sender_id: req.user.user_id,
	});
	updateNotificationRecord.updateNotification(nfData, []);
	if (getDriverData.device_token != '') {
		if (getDriverData.device_type.toUpperCase() == 'ANDROID') {
			Notifications.sendNotificationToAndroid(
				getDriverData.device_token,
				getNotificationMessage
			);
		} else {
			Notifications.sendNotificationToIOS(
				getNotificationMessage,
				getDriverData.device_token
			);
		}
	}

	// return response
	return global.okResponse(
		res,
		{
			...assignDriver,
		},
		'Order has been assigned successfully.'
	);
};

module.exports = {
	addUpdateOrderData,
	fetchOrderList,
	fetchSingleOrderDetails,
	fetchDriverList,
	assignOrderToDriver,
	deleteData,
	fetchAvailableDriverForOrder,
	fetchDataForAddEditOrders,
};
