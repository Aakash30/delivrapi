'use strict';

const jwt = require('jsonwebtoken');

const BusinessBranchArea = require('./../../models//BusinessBranchArea');
const Users = require('./../../models/Users');

const City = require('./../../models/City');
const Country = require('./../../models/Countries');

const Business = require('./../../models/Business');

const BusinessPartner = require('./../../models/BusinessPartner');

const UserRoles = require('./../../models/UserTypeRoles');

require('./../../global_functions');
const Email = require('./../../middlewares/email');

/**
 * Fetches Add updated business branches
 * @param {*} req
 * @param {*} res
 */

const addUpdateBusinessArea = async (req, res) => {
	let data = req.body;
	let message;

	if (!data.branch.country) {
		throw global.badRequestError('Please select country.');
	}

	if (!data.branch.city) {
		throw global.badRequestError('Please select city.');
	}

	if (!data.branch.address_line_1 || data.branch.address_line_1.trim() == '') {
		throw global.badRequestError('Please enter House number.');
	}

	if (!data.branch.address_line_2 || data.branch.address_line_2.trim() == '') {
		throw global.badRequestError('Please enter street address.');
	}

	if (!data.branch.latitude) {
		// throw global.badRequestError("Please provide location.");
	}

	if (!data.branch.longitude) {
		// throw global.badRequestError("Please provide location.");
	}

	if (
		!data.branch.address_branch_name ||
		data.branch.address_branch_name.trim() == ''
	) {
		throw global.badRequestError('Please provide location.');
	}

	if (!data.branch.postal_Code || data.branch.postal_Code.trim() == '') {
		throw global.badRequestError('Please enter pin code.');
	}

	if (data.branch.branch_area_id == null) {
		delete data.branch.branch_area_id;

		data.branch.type = 'subbranch';
		data.branch.created_by = req.user.user_id;

		data.branch.business_id = req.user.business_id;
		message = 'A new branch has been add successfully.';
	} else {
		data.branch.updated_by = req.user.user_id;
		message = 'Branch has been updated successfully.';
	}

	let addUpdateArea;

	if (!data.partner || data.partner.trim() == '') {
		addUpdateArea = await BusinessBranchArea.query().upsertGraph(data.branch, {
			relate: true,
		});
	} else {
		if (data.partner_id == null) {
			data.business_id = req.user.business_id;
			delete data.partner_id;
		} else {
		}

		addUpdateArea = await BusinessPartner.query().upsertGraph(data, {
			relate: true,
		});
	}

	if (!addUpdateArea) {
		throw global.badRequestError('Something went wrong');
	}
	return global.okResponse(
		res,
		{
			...addUpdateArea,
		},
		message
	);
};

/**
 * Fetches Business Branches
 * @param {*} req
 * @param {*} res
 */
const fetchBusinessAreas = async (req, res) => {
	/** set pagination */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	/** execute query to fetch data join with user_login to show the data*/
	let businessareas = await BusinessBranchArea.query()
		.select(
			'branch_area_id',
			'address_branch_name',
			'latitude',
			'longitude',
			'address_line_1',
			'address_line_2',
			'status',
			'type'
		)
		.eager('[countries, cities]')
		.modifyEager('countries', (builder) => {
			builder.select('country_id', 'country');
		})
		.modifyEager('cities', (builder) => {
			builder.select('city_id', 'city');
		})
		.where((builder) => {
			if (req.query.keyword) {
				builder.where(
					'address_branch_name',
					'ilike',
					'%' + req.query.keyword + '%'
				);
			}
		})
		.where('business_id', req.user.business_id)
		.limit(limit)
		.offset(offset);

	console.log('aaakash', businessareas);

	let totalCount = await BusinessBranchArea.query()
		.count('branch_area_id')
		.where((builder) => {
			if (req.query.keyword) {
				builder.where(
					'address_branch_name',
					'ilike',
					'%' + req.query.keyword + '%'
				);
			}
		})
		.where('business_id', req.user.business_id)
		.first();

	let responseData = {
		businessareas,
		total: totalCount.count,
		page,
	};
	return global.okResponse(
		res,
		{
			...responseData,
		},
		totalCount == 0 ? 'No records found.' : ''
	);
};

/**
 * fetch single business area data while adding or editing data
 * @param {*} req
 * @param {*} res
 */
const fetchBusinessAreaForAddEdit = async (req, res) => {
	let cityData = await City.query().select(
		'city_id',
		'city',
		'country_id',
		'status'
	);

	let countryData = await Country.query().select(
		'country_id',
		'country',
		'status',
		'alpha_code'
	);

	let partnerData = await BusinessPartner.query()
		.select('partner_id', 'partner')
		.where('business_id', req.user.business_id);

	let returnData = {
		cityList: cityData,
		countryList: countryData,
		partnerList: partnerData,
	};

	if (req.params.businessBranchId) {
		let businessBranch = await BusinessBranchArea.query()
			.select(
				'branch_area_id',
				'address_branch_name',
				'latitude',
				'longitude',
				'address_line_1',
				'address_line_2',
				'postal_Code',
				'country',
				'city',
				'partner_id'
			)
			.where('branch_area_id', req.params.businessBranchId)
			.first();

		returnData.businessBranch = businessBranch;
	}

	// return response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * fetch single business area data to view
 * @param {*} req
 * @param {*} res
 */
const fetchSingleBusinessBranch = async (req, res) => {
	if (!req.params.businessBranchId) {
		throw global.badRequestError('Invalid request');
	}
	let businessBranch = await BusinessBranchArea.query()
		.select(
			'branch_area_id',
			'address_branch_name',
			'latitude',
			'longitude',
			'address_line_1',
			'address_line_2',
			'postal_Code',
			'country',
			'city'
		)
		.eager('[countries, cities,businessPartner]')
		.modifyEager('businessPartner', (builder) => {
			builder.select('partner');
		})
		.modifyEager('countries', (builder) => {
			builder.select('country');
		})
		.modifyEager('cities', (builder) => {
			builder.select('city');
		})
		.where('branch_area_id', req.params.businessBranchId)
		.first();

	let returnData = {
		businessBranch,
	};
	// return response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

const updateBranchStatus = async (req, res) => {
	let data = req.body;

	if (!data.branch_area_id) {
		throw global.badRequestError('Invalid Request.');
	}

	if (!data.status) {
		throw global.badRequestError('Invalid Request.');
	}

	let updateStatus = await BusinessBranchArea.query()
		.update({
			status: data.status,
		})
		.where('branch_area_id', data.branch_area_id);

	if (!updateStatus) {
		throw global.badRequestError('Something went wrong.');
	}

	let getMainBranch = await BusinessBranchArea.query()
		.select('branch_area_id')
		.where('business_id', req.user.user_id)
		.where('type', 'mainbranch')
		.first();
	await Users.query()
		.update({
			branch_area_id: getMainBranch.branch_area_id,
		})
		.where('business_branch_id', data.branch_area_id);

	await Users.query()
		.update({
			business_area_id: getMainBranch.branch_area_id,
		})
		.where('business_area_id', data.branch_area_id);

	// return response
	return global.okResponse(
		res,
		{
			...updateStatus,
		},
		'Branch Status has been updated.'
	);
};

const fetchMyBusinessBranch = async (req, res) => {
	let branchList = await BusinessBranchArea.query()
		.select('branch_area_id', 'address_branch_name')
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.user.business_branch_id != null) {
				builder.where('branch_area_id', req.user.business_branch_id);
			}
		});

	return global.okResponse(
		res,
		{
			...branchList,
		},
		''
	);
};

const fetchMyProfile = async (req, res) => {
	let profileData;
	try {
		profileData = await Users.query()
			.select(
				'first_name',
				'last_name',
				'email_id',
				'mobile_number',
				'alternate_number',
				'profile_image',
				'emirates_id',
				'passport_document'
			)
			.eager('Business')
			.modifyEager('Business', (builder) => {
				builder
					.select(
						'business_id',
						'business_name',
						'mobile_number',
						'email_id',
						'agreement',
						'license',
						'bank_statement',
						'credit_application_form',
						'other_document',
						'business_logo'
					)
					.eager('businessBranch')
					.modifyEager('businessBranch', (builder) => {
						builder
							.select(
								builder
									.knex()
									.raw(
										"branch_area_id, address_branch_name, CONCAT(address_line_1, ' ', address_line_2, ' ', cities.city, ' ',countries.country) AS address"
									)
							)
							.joinRelation('cities')
							.joinRelation('countries');
					});
			})
			.where('user_id', req.user.user_id)
			.first();
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	return global.okResponse(
		res,
		{
			...profileData,
		},
		''
	);
};

const updateMyProfile = async (req, res) => {
	let data = {};

	let upsertData;
	let message;
	let messageInputs = {
		first_name: 'First Name',
		last_name: 'Last Name',
		mobile_number: 'Mobile Number',
		alternate_number: 'Alternate Number',
		profile_image: 'Profile Image',
	};
	let updateUserTable = false;

	console.log(req);
	if (req.file) {
		if (req.file.fieldname == 'profile') {
			updateUserTable = true;
			data.profile_image = req.file.location;
		} else {
			updateUserTable = false;
			data.business_logo = req.file.location;
		}

		//delete data.branch.profile;
	} else {
		data = req.body;
	}

	if (Object.keys(data).length > 0) {
		if (
			data.first_name ||
			data.last_name ||
			(data.mobile_number && req.user.user_type == 'super_admin') ||
			data.profile_image
		) {
			updateUserTable = true;
		}

		let getKey = Object.keys(data)[0];
		message = messageInputs[getKey] + ' has been updated successfully.';

		try {
			if (updateUserTable) {
				console.log('update user table :: ' + data);
				data.user_id = req.user.user_id;
				upsertData = await Users.query().upsertGraph(data);
			} else {
				console.log('dont update user table :: ');

				data.business_id = req.user.business_id;
				upsertData = await Business.query().upsertGraph(data);
				delete data.business_id;

				if (data.mobile_number) {
					data.user_id = req.user.user_id;
					await Users.query().upsertGraph(data);
				}
			}
		} catch (err) {
			console.log(err);
		}
	}

	if (!upsertData) {
		throw global.badRequestError('Something went wrong.');
	}

	return global.okResponse(
		res,
		{
			...upsertData,
		},
		message
	);
};

/**
 * business sub admins
 */

/**
 * Add Business Users
 * @param {*} req
 * @param {*} res
 */
const addUpdateBusinessUser = async (req, res) => {
	let data = req.body;

	if (!data.first_name || data.first_name.trim() == '') {
		throw global.badRequestError('Please enter first name.');
	}

	if (!data.last_name || data.last_name.trim() == '') {
		throw global.badRequestError('Please enter last name.');
	}

	if (!data.email_id || data.email_id.trim() == '') {
		throw global.badRequestError('Please enter email-id.');
	}

	if (!data.mobile_number || data.mobile_number.trim() == '') {
		throw global.badRequestError('Please enter mobile number.');
	}

	if (!data.business_branch_id) {
		throw global.badRequestError('Please choose the area for the user.');
	}

	if (!data.user_role || data.user_role.trim() == '') {
		throw global.badRequestError('Please choose atleast one role.');
	}

	let message;
	let verification;
	let insertUpdateBusinessUsers;

	try {
		if (data.user_id == null) {
			data.created_by = req.user.user_id;
			data.business_id = req.user.business_id;
			data.user_type = 'sub_ordinate';
			verification = await jwt.sign(
				{
					email: data.email_id,
				},
				global.CONFIG.jwt_encryption
			);

			data.verification = verification;
			delete data.user_id;
		} else {
			data.updated_by = req.user.user_id;
		}
		insertUpdateBusinessUsers = await Users.query().upsertGraph(data);
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	if (!insertUpdateBusinessUsers) {
		throw global.badRequestError('Something went wrong.');
	}

	if (data.user_id == null) {
		message =
			'User has been added successfully. Ask your user to activate his / her account via email.';

		Email.sendEmailT(
			data.email_id,
			'Welcome to Delivr! Confirm Your Email',
			'',
			{
				title: 'Welcome to Delivr!',
				name: data.first_name,
				message1: "You're on your way! Let's confirm your email address.",
				message2:
					'By clicking on the following link, you are confirming your email address.',
				buttonTitle: 'Confirm Email Address',
				buttonLink: global.GLOBAL_CLIENT_URL + 'activate/' + verification,
			}
		);
	} else {
		message = 'User has been updated successfully.';
	}
	return global.okResponse(
		res,
		{
			...insertUpdateBusinessUsers,
		},
		message
	);
};

/**
 * Fetches business subadmin users
 * @param {*} req
 * @param {*} res
 */
const fetchBusinessUsers = async (req, res) => {
	/** set pagination */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = limit * (page - 1);

	let fetchData = await Users.query()
		.select(
			'user_id',
			'first_name',
			'last_name',
			'email_id',
			'mobile_number',
			'user_status'
		)
		.eager('businessArea')
		.modifyEager('businessArea', (builder) => {
			builder.select('address_branch_name');
		})
		.where((builder) => {
			if (req.query.keyword) {
				builder.where('first_name', 'ilike', '%' + req.query.keyword + '%');
			}
			if (req.query.business_branch_id) {
				builder.where('business_branch_id', req.query.business_branch_id);
			}
			if (req.query.user_status) {
				builder.where('user_status', req.query.user_status);
			}
		})
		.where('business_id', req.user.business_id)
		.where('user_type', 'sub_ordinate')
		.limit(limit)
		.offset(offset);

	let totalCount = await Users.query()
		.count('user_id')
		.where((builder) => {
			if (req.query.keyword) {
				builder.where('first_name', 'ilike', '%' + req.query.keyword + '%');
			}
			if (req.query.business_branch_id) {
				builder.where('business_branch_id', req.query.business_branch_id);
			}
			if (req.query.user_status) {
				builder.where('user_status', req.query.user_status);
			}
		})
		.where('business_id', req.user.business_id)
		.where('user_type', 'sub_ordinate')
		.first();

	let responseData = {
		userList: fetchData,
		count: totalCount.count,
	};

	if (Object.keys(req.query).length == 0) {
		let BranchData = await BusinessBranchArea.query()
			.select('branch_area_id', 'address_branch_name')
			.where('business_id', req.user.business_id);

		responseData.myBusinessBranches = BranchData;
	}

	return global.okResponse(
		res,
		{
			...responseData,
		},
		totalCount == 0 ? 'No records found.' : ''
	);
};

/**
 * fetch data for business subadmin add / edit form
 */
const fetchForBusinessSubAdminAddEdit = async (req, res) => {
	let branchAreaList;
	try {
		branchAreaList = await BusinessBranchArea.query()
			.select(
				'branch_area_id',
				'address_branch_name',
				'business_address_area.status',
				'country_code'
			)
			.joinRelation('countries')
			.where('business_id', req.user.business_id);
	} catch (error) {
		console.log(error);
		throw global.badRequestError(error.message);
	}

	let rolesList = await UserRoles.query()
		.select('role_label', 'roles')
		.where('user_type', 'vendor');

	let returnData = {
		branchAreaList: branchAreaList,
		rolesList: rolesList,
		user_title: global.USER_TITLE,
	};

	if (req.params.id) {
		let user_id = new Buffer(req.params.id, 'base64').toString('ascii');
		if (isNaN(parseInt(user_id))) {
			throw global.badRequestError('Invalid Request');
		}

		let fetchSingleUser = await Users.query()
			.select(
				'user_id',
				'first_name',
				'last_name',
				'email_id',
				'mobile_number',
				'user_status',
				'user_role',
				'designation',
				'business_branch_id'
			)
			.eager('businessArea')
			.modifyEager('businessArea', (builder) => {
				builder.select('address_branch_name');
			})
			.where((builder) => {
				if (req.query.branch_id) {
					builder.where('business_branch_id', req.query.branch_id);
				}
			})
			.where('business_id', req.user.business_id)
			.where('user_type', 'sub_ordinate')
			.where('user_id', user_id)
			.first();

		if (!fetchSingleUser) {
			throw global.notFoundError('User not found');
		}

		returnData.fetchSingleUser = fetchSingleUser;
	}

	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * Fetches single user data
 * @param {*} req
 * @param {*} res
 */
const fetchMyBusinessUser = async (req, res) => {
	if (!req.params.id) {
		throw global.badRequestError('Invalid Request.');
	}

	let user_id = new Buffer(req.params.id, 'base64').toString('ascii');
	if (isNaN(parseInt(user_id))) {
		throw global.badRequestError('Invalid Request');
	}

	let fetchSingleUser = await Users.query()
		.select(
			'user_id',
			'first_name',
			'last_name',
			'email_id',
			'mobile_number',
			'user_status',
			'user_role',
			'designation'
		)
		.eager('businessArea')
		.modifyEager('businessArea', (builder) => {
			builder.select('address_branch_name');
		})
		.where((builder) => {
			if (req.query.branch_id) {
				builder.where('business_branch_id', req.query.branch_id);
			}
		})
		.where('business_id', req.user.business_id)
		.where('user_type', 'sub_ordinate')
		.where('user_id', user_id)
		.first();

	if (!fetchSingleUser) {
		throw global.notFoundError('User not found');
	}

	let rolesList = await UserRoles.query()
		.select('role_label', 'roles')
		.where('user_type', 'vendor');

	let returnData = {
		fetchSingleUser: fetchSingleUser,
		rolesList: rolesList,
		user_title: global.USER_TITLE,
	};

	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * Updates User status - active / deactive
 * @param {*} req
 * @param {*} res
 */
const updateMyUsersStatus = async (req, res) => {
	let data = req.body;

	if (!data.user_id) {
		throw global.badRequestError('Invalid Request.');
	}

	let user_id = new Buffer(data.user_id, 'base64').toString('ascii');
	if (isNaN(parseInt(user_id))) {
		throw global.badRequestError('Invalid Request');
	}

	if (!data.user_status) {
		throw global.badRequestError('Invalid Action.');
	}

	let message;
	if (data.user_status == 'active') {
		message = 'User has been activated successfully.';
	} else {
		message = 'User has been deactivated successfully.';
	}

	let userUpdate = await Users.query()
		.update({
			user_status: data.user_status,
		})
		.where('user_id', user_id);

	return global.okResponse(
		res,
		{
			...userUpdate,
		},
		message
	);
};
module.exports = {
	addUpdateBusinessArea,
	fetchBusinessAreas,
	updateBranchStatus,
	fetchMyBusinessBranch,
	fetchBusinessAreaForAddEdit,
	fetchSingleBusinessBranch,
	fetchMyProfile,
	updateMyProfile,
	fetchForBusinessSubAdminAddEdit,
	addUpdateBusinessUser,
	fetchBusinessUsers,
	fetchMyBusinessUser,
	updateMyUsersStatus,
};
