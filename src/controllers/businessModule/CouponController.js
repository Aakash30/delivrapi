'use strict';
let moment = require('moment');
const Coupons = require('./../../models/Coupon');
const BusinessBranch = require('./../../models/BusinessBranchArea');
const Business = require('../../models/Business');

/**
 * Fetches coupons for admin and business
 * @param {*} req
 * @param {*} res
 */
const fetchCouponListForAdmin = async (req, res) => {
	/** Get parameters for pagination purpose */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	let couponList;
	try {
		couponList = await Coupons.query()
			.select(
				'coupon_id',
				'coupon_code',
				'coupon_image',
				'start_date',
				'expired_on as end_date',
				'discount_in_percent'
			)
			.eager('[business, businessBranch]')
			.modifyEager('business', (builder) => {
				builder.select('business_name');
			})
			.modifyEager('businessBranch', (builder) => {
				builder.select('address_branch_name');
			})
			.where((builder) => {
				if (req.user.user_type == 'vendor') {
					builder.where('business_id', req.user.business_id);
				} else if (req.user.user_type == 'sub_ordinate') {
					builder.where('business_branch_id', req.user.business_branch_id);
				}
			})
			.where((builder) => {
				if (req.query.code) {
					builder.where('coupon_code', 'ilike', '%' + req.query.code + '%');
				}
				if (req.query.business) {
					builder.where('business_id', req.query.business);
				}

				if (req.query.start_range_from && !req.query.start_range_to) {
					builder.where(
						'start_date',
						'>=',
						moment(req.query.start_range_from),
						moment(req.query.start_range_to)
					);
				} else if (!req.query.start_range_from && req.query.start_range_to) {
					builder.whereBetween(
						'start_date',
						'<=',
						moment(req.query.start_range_to)
					);
				} else if (req.query.start_range_from && req.query.start_range_to) {
					builder.whereBetween('start_date', [
						moment(req.query.start_range_from),
						moment(req.query.start_range_to),
					]);
				}

				if (req.query.end_range_from && !req.query.end_range_to) {
					builder.where(
						'expired_on',
						'>=',
						moment(req.query.end_range_from),
						moment(req.query.end_range_to)
					);
				} else if (!req.query.end_range_from && req.query.end_range_to) {
					builder.whereBetween(
						'expired_on',
						'<=',
						moment(req.query.end_range_to)
					);
				} else if (req.query.end_range_from && req.query.end_range_to) {
					builder.whereBetween('expired_on', [
						moment(req.query.end_range_from),
						moment(req.query.end_range_to),
					]);
				}
			})
			.runAfter((result, builder) => {
				return result;
			})
			.limit(limit)
			.offset(offset);
	} catch (error) {
		console.log(error);
	}

	let totalCoupons = await Coupons.query()
		.count()
		.where((builder) => {
			if (req.user.user_type == 'vendor') {
				builder.where('business_id', req.user.business_id);
			} else if (req.user.user_type == 'sub_ordinate') {
				builder.where('business_branch_id', req.user.business_branch_id);
			}
		})
		.where((builder) => {
			if (req.query.code) {
				builder.where('coupon_code', 'ilike', '%' + req.query.code + '%');
			}
			if (req.query.business) {
				builder.where('business_id', req.query.business);
			}

			if (req.query.discountFrom && req.query.discountTo) {
				builder.whereBetween('discount_in_percent', [
					req.query.discountFrom,
					req.query.discountTo,
				]);
			}

			if (req.query.expired_on) {
				builder.where('expired_on', moment(req.query.expired_on));
			}
		})
		.first();
	let resultData = {
		couponList,
		total: totalCoupons.count,
	};

	if (
		req.user.user_type == 'super_admin' &&
		Object.keys(req.query).length == 0
	) {
		let businessData = await Business.query().select(
			'business_id',
			'business_name'
		);
		resultData.business = businessData;
	}

	// send response
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

const addUpdateCouponData = async (req, res) => {
	let data = req.body;

	if (!data.description || data.description.trim() == '') {
		throw global.badRequestError('Please add a description.');
	}

	if (!data.start_date || data.start_date.trim() == '') {
		throw global.badRequestError('Add a start date.');
	}

	if (!data.expired_on || data.expired_on.trim() == '') {
		throw global.badRequestError('Add an expiry date.');
	}

	// if (!data.discount_in_percent) {
	//     throw global.badRequestError("Please add a discount in percentage.");
	// }

	if (!data.coupon_code || data.coupon_code.trim() == '') {
		throw global.badRequestError('Please generate a unique code for coupon');
	}
	if (req.file) {
		data.coupon_image = req.file.location;
	}

	let message;
	if (data.coupon_id == undefined) {
		delete data.coupon_id;
		data.business_id = req.user.business_id;

		message = 'Coupon has been generated successfully.';
	} else {
		message = 'Coupon has been updated successfully.';
	}

	let upsertData;
	try {
		upsertData = await Coupons.query().upsertGraph(data);
	} catch (error) {
		throw global.badRequestError(error.message);
	}
	if (!upsertData) {
		throw global.badRequestError('Something went wrong.');
	}

	// send response
	return global.okResponse(
		res,
		{
			...upsertData,
		},
		message
	);
};

/**
 * generateCode - generates a unique coupon_code
 * @param {stores the requested parameter} req
 * @param {stores the respond to be sent to the request} res
 */
const generateCode = async (req, res, caller) => {
	let randomCode = global.randomStrings(6);

	let result = await Coupons.query()
		.select('coupon_id')
		.where('coupon_code', randomCode);

	if (result && result.length > 0) {
		generateCode(req, res);
	} else {
		let coupon_code = {
			randomCode,
		};
		if (caller == 1) return randomCode;
		else {
			return global.okResponse(
				res,
				{
					...coupon_code,
				},
				''
			);
		}
	}
};

const fetchDataForAddEditCoupons = async (req, res) => {
	let businessAreaList = await BusinessBranch.query()
		.select('branch_area_id', 'address_branch_name')
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.user.user_type != 'vendor') {
				builder.where('branch_area_id', req.user.business_branch_id);
			}
		});

	let coupon_code = await generateCode(req, res, 1);
	let resultData = {
		businessAreaList,
		coupon_code,
	};

	if (req.params.id) {
		let couponId = new Buffer(req.params.id, 'base64').toString('ascii');
		if (isNaN(parseInt(couponId))) {
			throw global.notFoundError('Coupon not found');
		}
		let couponData = await Coupons.query()
			.select(
				'coupon_id',
				'coupon_code',
				'business_branch_id',
				'coupon_image',
				'expired_on',
				'start_date',
				'discount_in_percent',
				'description',
				'business_id'
			)
			.where('coupon_id', couponId)
			.first();

		resultData.couponData = couponData;
	}

	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

const viewCouponDetails = async (req, res) => {
	if (!req.params.id) {
		throw global.badRequestError('Invalid requests.');
	}
	let couponData = await Coupons.query()
		.select(
			'coupon_id',
			'coupon_code',
			'coupon_image',
			'description',
			'expired_on',
			'discount_in_percent'
		)
		.eager('[business, businessBranch]')
		.modifyEager('business', (builder) => {
			builder.select('business_name');
		})
		.modifyEager('businessBranch', (builder) => {
			builder.select('address_branch_name');
		})
		.where('coupon_id', req.params.id)
		.first();

	let resultData = {
		couponData,
	};
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

const fetchCoupons = async (req, res) => {
	try {
		let page = req.query.page ? req.query.page : 1;
		let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
		let offset = req.query.offset ? req.query.offset : limit * (page - 1);

		let message = 'Coupons retrieved';
		if (!req.query.status || req.query.status == '') {
			throw global.badRequestError('Bad request. coupon status is required');
		}
		let couponData = await Coupons.query()
			.select(
				'coupon_id',
				'coupon_code',
				'coupon_image',
				'description',
				'start_date',
				'created_at',
				'expired_on',
				'discount_in_percent'
			)
			.eager('[business, businessBranch]')
			.modifyEager('business', (builder) => {
				builder.select('business_name');
			})
			.modifyEager('businessBranch', (builder) => {
				builder.select('address_branch_name');
			})
			.where('business_id', req.user.business_id)
			.where((builder) => {
				if (req.query.status == 'new')
					builder.where('expired_on', '>', moment('000000', 'HH:mm:ss'));

				if (req.query.status == 'expired')
					builder.where('expired_on', '<=', moment('000000', 'HH:mm:ss'));
			})
			.orderBy('expired_on', 'desc')
			.offset(offset)
			.limit(limit)
			.runAfter((result, builder) => {
				return result;
			});

		let count = await Coupons.query()
			.count()
			.where('business_id', req.user.business_id)
			.where((builder) => {
				if (req.query.status == 'new')
					builder.where('expired_on', '>', moment('000000', 'HH:mm:ss'));
				if (req.query.status == 'expired')
					builder.where('expired_on', '<=', moment('000000', 'HH:mm:ss'));
			})
			.first();

		if (!couponData[0]) message = 'No Coupons found';

		let resultData = {
			couponData,
			count: count.count,
		};
		return global.okResponse(
			res,
			{
				...resultData,
			},
			message
		);
	} catch (err) {
		throw global.badRequestError('Bad request');
	}
};

module.exports = {
	fetchCouponListForAdmin,
	addUpdateCouponData,
	generateCode,
	fetchDataForAddEditCoupons,
	viewCouponDetails,
	fetchCoupons,
};
