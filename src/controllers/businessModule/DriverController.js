'use strict';

const { transaction, ref } = require('objection');
const moment = require('moment');

const Users = require('./../../models/Users');
const UserTypeRoles = require('./../../models/UserTypeRoles');
const Country = require('./../../models/Countries');
const City = require('./../../models/City');
const Vehicle = require('./../../models/Vehicle');
const VehicleMapping = require('./../../models/VehicleMapping');
const VehicleMappingHistory = require('./../../models/VehicleMappingHistory');
const BusinessBranch = require('./../../models/BusinessBranchArea');

const Orders = require('./../../models/Order');
const sendSMS = require('../../middlewares/sms').sendSms;
const sendSms_twilio = require('../../middlewares/sms').sendSms_twilio;
/**
 * @function: fetchDriverList
 * @description: Provides driver list which belongs to the logged in user
 * @param {*} req
 * @param {*} res
 */
const fetchDriverList = async (req, res) => {
	/** Get parameters for pagination purpose */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	// execute query to fetch data
	let driverList = await Users.query()
		.select(
			'user_id',
			Users.knex().raw(
				" CONCAT(first_name, ' ', last_name) AS driver, mobile_number, email_id, user_status"
			),
			'vehicle_number'
		)
		.leftJoin(
			'vehicle_mapping',
			'vehicle_mapping.driver_id',
			'=',
			'user_login.user_id'
		)
		.leftJoin(
			'vehicle',
			'vehicle.vehicle_id',
			'=',
			'vehicle_mapping.vehicle_id'
		)
		.mergeNaiveEager('[driverLocation, orders]')
		.modifyEager('orders', (builder) => {
			builder
				.count('order_id')
				.avg('rating')
				.where('order_status', 'delivered')
				.groupBy('driver_id');
		})
		.modifyEager('driverLocation', (builder) => {
			builder
				.select('lat', 'lng', 'image', 'created_at', 'address')
				.orderBy('created_at', 'desc')
				.offset(0)
				.limit(3);
		})
		.where('user_type', 'driver')
		.where(' user_login.business_id', req.user.business_id)
		.where((builder) => {
			if (Object.keys(req.query).length > 0) {
				let queryData = req.query;

				if (queryData.status) {
					builder.where('user_status', queryData.status);
				}

				if (queryData.country) {
					builder.where('user_login.country', queryData.country);
				}

				if (queryData.city) {
					builder.where('user_login.city', queryData.city);
				}

				if (queryData.keyword) {
					builder.where((builder) => {
						builder
							.where('first_name', 'ilike', queryData.keyword)
							.orWhere('last_name', 'ilike', queryData.keyword)
							.orWhere('mobile_number', 'ilike', queryData.keyword);
					});
				}
			}
		})
		.where((builder) => {
			if (req.user.user_type == 'sub_ordinate') {
				builder.where('business_branch_id', req.user.business_branch_id);
			}
		})
		.orderBy('user_login.created_at', 'DESC')
		.offset(offset)
		.limit(limit);

	// get the total driver count
	let totalDriver = await Users.query()
		.count('user_id')
		.leftJoin(
			'vehicle_mapping',
			'vehicle_mapping.driver_id',
			'=',
			'user_login.user_id'
		)
		.leftJoin(
			'vehicle',
			'vehicle.vehicle_id',
			'=',
			'vehicle_mapping.vehicle_id'
		)
		.where('user_type', 'driver')
		.where('user_login.business_id', req.user.business_id)
		.where((builder) => {
			if (Object.keys(req.query).length > 0) {
				let queryData = req.query;

				if (queryData.status) {
					builder.where('user_status', queryData.status);
				}

				if (queryData.country) {
					builder.where('user_login.country', queryData.country);
				}

				if (queryData.city) {
					builder.where('user_login.city', queryData.city);
				}

				if (queryData.keyword) {
					builder.where((builder) => {
						builder
							.where('first_name', 'ilike', queryData.keyword)
							.orWhere('last_name', 'ilike', queryData.keyword)
							.orWhere('mobile_number', 'ilike', queryData.keyword);
					});
				}
			}
		})
		.where((builder) => {
			if (req.user.user_type == 'sub_ordinate') {
				builder.where('business_branch_id', req.user.business_branch_id);
			}
		})
		.first();

	let country, city;

	if (Object.keys(req.query).length == 0) {
		// fetch data for country
		country = await Country.query();

		// fetch data for city
		city = await City.query();
	}
	/** Prepare response data object */
	let returnData = {
		count: totalDriver.count,
		driverList: driverList,
		country: country,
		city: city,
	};

	// send response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @function: addUpdateDriver
 * @description: add or update driver (If driver id is passed then update action is performed)
 * @param {*} req stores the requested parameters
 * @param {*} res stores the response values
 */
const addUpdateDriver = async (req, res) => {
	/** Get the data posted from the form */
	let data = req.body;

	/** Check for the empty fields */
	if (!data.first_name) {
		throw global.badRequestError("Please enter driver's first name.");
	}

	if (!data.last_name) {
		throw global.badRequestError("Please enter driver's last name.");
	}

	// if (!data.email_id) {
	//     throw global.badRequestError("Please enter email id.");
	// }

	if (!data.mobile_number) {
		throw global.badRequestError('Please enter mobile number.');
	}

	if (!data.business_branch_id) {
		throw global.badRequestError('Please select branch.');
	}
	if (!data.country) {
		throw global.badRequestError('Please select country.');
	}

	if (!data.city) {
		throw global.badRequestError('Please select city.');
	}

	if (!data.address_line_1) {
		throw global.badRequestError('Please enter street address.');
	}

	if (!data.address_line_2) {
		throw global.badRequestError('Please enter address.');
	}

	if (!data.vehicles) {
		throw global.badRequestError('Please assign vehicle to the driver.');
	}
	// if (!data.license_document) {
	//     throw global.badRequestError("Please provide license copy.");
	// }

	// if (!data.passport_document) {
	//     throw global.badRequestError("Please provide passport copy.");
	// }

	// if (!data.tollgate_document) {
	//     throw global.badRequestError("Please provide tollgate copy.");
	// }

	delete data.register;
	let insertUpdateDriver, message;
	let smsMessage = '';
	if (!data.user_id) {
		let password = Math.floor(1000 + Math.random() * 9000);
		//let password = 1234;
		data.password = password.toString();
		data.user_type = 'driver';

		/** Execute Query to get default roles of the driver */
		let userRolesResult = await UserTypeRoles.query()
			.select('roles')
			.where('user_type', 'driver')
			.where('role_type', 'default');

		let userRoles = [];

		userRolesResult.forEach((element) => {
			userRoles.push(element.roles);
		});
		data.user_role = JSON.stringify(userRoles);
		data.created_by = req.user.user_id;
		data.business_id = req.user.business_id;

		message =
			'Driver has been added successfully. App link and password has been sent to the registered mobile number.';
		smsMessage =
			'Get login with your mobile number to . Your OTP is -' + password;
	} else {
		data.updated_by = req.user.user_id;
		message = 'Driver has been updated successfully.';
	}

	try {
		insertUpdateDriver = await transaction(Users.knex(), (trx) => {
			return Users.query(trx).upsertGraph(data, {
				relate: true,
				unrelate: true,
			});
		});
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	/** If query is not executed throw error */
	if (!insertUpdateDriver) {
		throw global.badRequestError('Something went wrong.');
	}

	if (!data.user_id) {
		// sendSMS(data.mobile_number.replace("+", "").replace("-", ""), smsMessage);
		sendSms_twilio(
			data.mobile_number.replace('+', '').replace('-', ''),
			smsMessage
		);
	}
	/** Send response */
	return global.okResponse(
		res,
		{
			...insertUpdateDriver,
		},
		message
	);
};

/**
 * Fetches data required while adding/updating driver in form
 * @param {*} req
 * @param {*} res
 */

const fetchDataForAddEditDriver = async (req, res) => {
	let cityData = await City.query().select(
		'city_id',
		'city',
		'country_id',
		'status'
	);

	let countryData = await Country.query().select(
		'country_id',
		'country',
		'status',
		'country_code'
	);

	let businessArea = await BusinessBranch.query()
		.select(
			'branch_area_id',
			'address_branch_name',
			'country',
			'city',
			'status'
		)
		.where('business_id', req.user.business_id);
	let returnData = {
		cityList: cityData,
		countryList: countryData,
		businessArea: businessArea,
	};

	if (req.params.driverId) {
		let singleDriver = await Users.query()
			.select(
				'user_id',
				'first_name',
				'last_name',
				'mobile_number',
				'email_id',
				'user_status',
				'address_line_1',
				'address_line_2',
				'postal_Code',
				'license_document',
				'passport_document',
				'tollgate_document',
				'country',
				'city',
				'business_branch_id',
				'emirates_id'
			)
			.eager('[vehicles]')
			.modifyEager('vehicles', (builder) => {
				builder.select('map_id');
				builder.eager('vehicle').modifyEager('vehicle', (builder) => {
					builder.select(
						'vehicle_number',
						'color',
						'engine_number',
						'starting_mechanism',
						'tyre_type',
						'wheel_type',
						'stroke',
						'mileage',
						'registration_card',
						'insurance'
					);
					builder
						.eager('[vehicle_brand, vehicle_model, vehicle_images]')
						.modifyEager('vehicle_brand', (builder) => {
							builder.select('brand_name');
						})
						.modifyEager('vehicle_model', (builder) => {
							builder.select('model_name');
						})
						.modifyEager('vehicle_images', (builder) => {
							builder.select('vehicle_image');
						});
				});
			})
			.where('user_id', req.params.driverId)
			.where('user_type', 'driver')
			.where('business_id', req.user.business_id)
			.where((builder) => {
				if (req.user.user_type == 'sub_ordinate') {
					builder.where('business_branch_id', req.user.business_branch_id);
				}
			})
			.first();

		returnData.driver = singleDriver;
	}
	//console.log(btoa("1"));
	// console.log(atob(btoa("1")))
	// return response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @function: fetchSingleDriverDetails
 * @description: fetch the details of single driver
 * @param {*} req stores the requested parameters
 * @param {*} res stores the response values
 */

const fetchSingleDriverDetails = async (req, res) => {
	/** If driver id is not passed in the url then throw error. */
	if (!req.params.driverId) {
		throw global.badRequestError('Invalid URL!');
	}

	// execute query to get the data of the driver

	let singleDriver = await Users.query()
		.select(
			'user_id',
			'first_name',
			'last_name',
			'mobile_number',
			'email_id',
			'user_status',
			'address_line_1',
			'address_line_2',
			'postal_Code',
			'license_document',
			'passport_document',
			'tollgate_document',
			'emirates_id'
		)
		.eager('[countries, cities, businessArea, vehicles]')
		.modifyEager('countries', (builder) => {
			builder.select('country_id', 'country');
		})
		.modifyEager('cities', (builder) => {
			builder.select('city_id', 'city');
		})
		.modifyEager('businessArea', (builder) => {
			builder.select('branch_area_id', 'address_branch_name');
		})
		.modifyEager('vehicles', (builder) => {
			builder.select('');
			builder.eager('vehicle').modifyEager('vehicle', (builder) => {
				builder.select(
					'vehicle_number',
					'color',
					'engine_number',
					'starting_mechanism',
					'tyre_type',
					'wheel_type',
					'stroke',
					'mileage',
					'registration_card',
					'insurance'
				);
				builder
					.eager('[vehicle_brand, vehicle_model, vehicle_images]')
					.modifyEager('vehicle_brand', (builder) => {
						builder.select('brand_name');
					})
					.modifyEager('vehicle_model', (builder) => {
						builder.select('model_name');
					})
					.modifyEager('vehicle_images', (builder) => {
						builder.select('vehicle_image');
					});
			});
		})
		.where('user_id', req.params.driverId)
		.where('user_type', 'driver')
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.user.user_type == 'sub_ordinate') {
				builder.where('business_branch_id', req.user.business_branch_id);
			}
		})
		.first();

	console.log(singleDriver);
	// send response
	return global.okResponse(
		res,
		{
			...singleDriver,
		},
		''
	);
};

/**
 * @function: activeDeactiveDriver
 * @description: driver is either activated or deactivated
 * @param {*} req stores the requested parameters
 * @param {*} res stores the response values
 */

const activeDeactiveDriver = async (req, res) => {
	let data = req.body;

	if (!data.user_id) {
		throw global.badRequestError('Invalid request.');
	}

	if (!data.user_status) {
		throw global.badRequestError('Invalid request.');
	}

	let message, updateUser;

	try {
		updateUser = await Users.query()
			.where('user_id', data.user_id)
			.update({
				user_status: data.user_status,
				updated_by: req.user.user_id,
			})
			.returning('user_id');
	} catch (error) {
		console.log(error);
	}

	// if not executed throw error
	if (!updateUser) {
		throw global.badRequestError('Something went wrong.');
	}

	if (data.user_status == 'active') {
		message = 'Driver has been activated successfully.';
	} else {
		message = 'Driver has been blocked successfully.';
	}

	// return response
	return global.okResponse(
		res,
		{
			...updateUser,
		},
		message
	);
};

/**
 * @function: fetchAvailableVehicleForBusiness
 * @description: This is used to search for the available vehicles (which are on lease but not assigned to driver)
 * @param {*} req stores the requested parameters
 * @param {*} res stores the response values
 */
const fetchAvailableVehicleForBusiness = async (req, res) => {
	/** Execute query to fetch available vehicles */
	let vehicleList = await Vehicle.query()
		.select(
			'vehicle.vehicle_id',
			'vehicle_number',
			'color',
			'engine_capacity',
			'engine_number',
			'starting_mechanism',
			'tyre_type',
			'wheel_type',
			'stroke',
			'breakfront',
			'price',
			'currency',
			'mileage',
			'registration_card',
			'insurance',
			'map_id'
		)
		.eager('[vehicle_brand, vehicle_model, vehicle_images]')
		.modifyEager('vehicle_brand', (builder) => {
			builder.select('brand_name');
		})
		.modifyEager('vehicle_model', (builder) => {
			builder.select('model_name');
		})
		.modifyEager('vehicle_images', (builder) => {
			builder.select('vehicle_image');
		})
		.join('vehicle_mapping', function () {
			this.on('vehicle_mapping.vehicle_id', '=', 'vehicle.vehicle_id').andOn(
				'vehicle_mapping.business_id',
				this.knex().raw('?', [req.user.business_id])
			);
		})
		.joinRelation('vehicle_model')
		.where((builder) => {
			/** If keyword is passed */
			if (req.query.keyword) {
				builder.where('model_name', 'ilike', '%' + req.query.keyword + '%');
			}

			builder.where('vehicle_mapping.driver_id', null);
		})
		.where((builder) => {
			if (req.user.user_type == 'sub_ordinate') {
				builder.where(
					'vehicle_mapping.business_area_id',
					req.user.business_branch_id
				);
			}
		})
		.runAfter((result, builder) => {
			return result;
		});

	if (!vehicleList) {
		throw global.badRequestError('Something went wrong.');
	}

	let resultData = {
		vehicleList,
	};
	// return response
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

/**
 * @function: assignVehicleToDriver
 * @description: assign vehicle to the driver
 * @param {*} req stores the requested parameters
 * @param {*} res stores the response values
 */
const assignVehicleToDriver = async (req, res) => {
	if (!req.params.driverId) {
		throw global.badRequestError('Invalid Url!!');
	}

	if (!req.params.vehicleId) {
		throw global.badRequestError('Invalid Url!!');
	}

	/** Execute query to assign vehicles to the driver */
	let assignDriver = await VehicleMapping.query()
		.update({
			driver_id: req.params.driverId,
		})
		.where('business_id', req.user.business_id)
		.where('vehicle_id', req.params.vehicleId)
		.returning('vehicle_id');

	if (assignDriver) {
		let trackHistory = await VehicleMappingHistory.query().insert({
			driver_id: req.params.driverId,
			business_id: req.user.business_id,
			vehicle_id: req.params.vehicleId,
			assinged_by: req.user.user_id,
			assigned_on: new Date().toISOString(),
		});
	}
	// return response
	return global.okResponse(
		res,
		{
			...assignDriver,
		},
		''
	);
};

/**
 * This function provides list of available drivers who is not assigned with any vehicle
 * @param {*} req
 * @param {*} res
 */
const searchDriversForVehicles = async (req, res) => {
	let driverList = await Users.query()
		.select('user_id', 'first_name', 'last_name', 'email_id', 'mobile_number')
		.where('business_id', req.user.business_id)
		.where('user_type', 'driver')
		.where((builder) => {
			if (req.user.business_branch_id != null) {
				builder.where('branch_area_id', req.user.business_branch_id);
			}
		})
		.whereNotExists(Users.relatedQuery('vehicles'));

	// return response
	return global.okResponse(
		res,
		{
			...driverList,
		},
		''
	);
};

/**
 * This function changes the assigned driver
 * @param {*} req
 * @param {*} res
 */
const changeVehicleDriver = async (req, res) => {
	/** get the POST data */
	let data = req.body;

	if (!data.map_id) {
		throw global.badRequestError('Invalid Request');
	}

	if (!data.driver_id) {
		throw global.badRequestError('No Driver is selected');
	}

	let updateDriver = await VehicleMapping.query()
		.update({
			driver_id: data.driver_id,
		})
		.where('map_id', data.map_id)
		.where('business_id', req.user.business_id)
		.returning('vehicle_id');

	if (!updateDriver) {
		throw global.badRequestError('Something went wrong.');
	}

	let trackHistory = await VehicleMappingHistory.query().insert({
		driver_id: data.driver_id,
		business_id: req.user.business_id,
		vehicle_id: updateDriver.vehicle_id,
		assinged_by: req.user.user_id,
		assigned_on: new Date().toISOString(),
	});

	// return response
	return global.okResponse(
		res,
		{
			...updateDriver,
		},
		'Driver has been updated successfully.'
	);
};

/**
 * Fetch list of drivers with their order and average rating
 * @param {*} req
 * @param {*} res
 */
const fetchDriverFeedback = async (req, res) => {
	/** Get parameters for pagination purpose */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	let driverList = await Users.query()
		.select(
			Users.knex().raw(
				"user_id AS driver_id, CONCAT(first_name, ' ', last_name) AS driver, mobile_number, email_id"
			)
		)
		.eager('orders')
		.modifyEager('orders', (builder) => {
			builder
				.count('order_id')
				.avg('rating')
				.where('order_status', 'delivered')
				.groupBy('driver_id');
		})
		.where('business_id', req.user.business_id)
		.where('user_type', 'driver')
		.where((builder) => {
			if (req.query.keyword) {
				builder
					.where('first_name', 'ilike', '%' + req.query.keyword + '%')
					.orWhere('last_name', 'ilike', '%' + req.query.keyword + '%')
					.orWhere('mobile_number', 'ilike', '%' + req.query.keyword + '%')
					.orWhere('email_id', 'ilike', '%' + req.query.keyword + '%');
			}
		})
		.offset(offset)
		.limit(limit);

	let totalCount = await Users.query()
		.count('user_id')
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.query.keyword) {
				builder
					.where('first_name', 'ilike', '%' + req.query.keyword + '%')
					.orWhere('last_name', 'ilike', '%' + req.query.keyword + '%')
					.orWhere('mobile_number', 'ilike', '%' + req.query.keyword + '%')
					.orWhere('email_id', 'ilike', '%' + req.query.keyword + '%');
			}
		})
		.first();
	let resultData = {
		driverList,
		count: totalCount.count,
	};

	// return response
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

const fetchIndividualDriverFeedback = async (req, res) => {
	/** Get parameters for pagination purpose */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	if (!req.params.driverId) {
		throw global.badRequestError('Invalid Request');
	}

	/** Execute query to fetch data */
	let driverListData = await Users.query()
		.select(
			Users.knex().raw(
				"user_id AS driver_id, CONCAT(first_name, ' ', last_name) AS driver, mobile_number, email_id"
			)
		)
		.where('business_id', req.user.business_id)
		.where('user_id', req.params.driverId)
		.first();

	let orderReviewData = await Orders.query()
		.select(
			Orders.knex().raw(
				"invoice_id, CONCAT(first_name, ' ',last_name) AS customer, mobile_number, email, address_1, address_2, rating, subtotal, order_date"
			)
		)
		.where('order_status', 'delivered')
		.where('driver_id', req.params.driverId)
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.query.keyword) {
				builder.where('invoice_id', 'ilike', '%' + req.query.keyword + '%');
			}

			if (req.query.start_date) {
				let dateObject = moment(req.query.start_date);

				builder.where('order_date', '>=', dateObject);
			}

			if (req.query.end_date) {
				let dateObject = moment(req.query.end_date);

				builder.where('order_date', '<=', dateObject);
			}
		})
		.orderBy('order_date', 'DESC')
		.limit(limit)
		.offset(offset);

	let orderReviewDataCount = await Orders.query()
		.count('order_id')
		.where('order_status', 'delivered')
		.where('driver_id', req.params.driverId)
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.query.keyword) {
				builder.where('invoice_id', 'ilike', '%' + req.query.keyword + '%');
			}

			if (req.query.start_date) {
				let dateObject = moment(req.query.start_date);

				builder.where('order_date', '>=', dateObject);
			}

			if (req.query.end_date) {
				let dateObject = moment(req.query.end_date);

				builder.where('order_date', '<=', dateObject);
			}
		})
		.first();
	let resultData = {
		driverListData,
		orderReviewData,
		count: orderReviewDataCount.count,
	};

	// return response
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

const driverTrackingList = async (req, res) => {
	/** Execute query to fetch data */
	let driverList;
	try {
		driverList = await Users.query()
			.select([
				Users.knex().raw(
					"user_id AS driver_id, CONCAT(first_name, ' ', last_name) AS driver, mobile_number, email_id"
				),
				Orders.query()
					.avg('rating')
					.where('order_status', 'delivered')
					.where('driver_id', ref('user_login.user_id'))
					.groupBy('driver_id')
					.as('driver_rating'),
			])
			.eager('[tracker, orders]')
			.modifyEager('tracker', (builder) => {
				builder.select('driver_latitude', 'driver_longitude');
			})
			.modifyEager('orders', (builder) => {
				builder
					.select(
						builder
							.knex()
							.raw(
								'invoice_id, order_date, CONCAT(first_name, \'\', last_name) AS customer, CONCAT(address_1, \'\', address_2) AS address, mobile_number, subtotal, "order".latitude, "order".longitude, order_status, currency,  "businessBranch".latitude AS center_latitude, "businessBranch".longitude as center_longitude'
							)
					)
					.joinRelation('countries')
					.joinRelation('businessBranch')
					.where((builder) => {
						builder
							.where('order_status', 'ongoing')
							.orWhere('order_status', 'accepted')
							.orderBy('order_status', 'DESC');
					});
			})
			.where('business_id', req.user.business_id)
			.where('user_type', 'driver')
			.where('user_status', 'active')
			.runAfter((result, builder) => {
				return result;
			});
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	let resultData = {
		driverList,
	};

	// return response
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

const test_zubear = async (req, res) => {
	let insertUpdateDriver = [],
		message = 'test message send';
	let password = 1245;
	sendSms_twilio(
		'+919074995244',
		'Get login with your mobile number to . Your OTP is -' + password
	);
	/** Send response */
	return global.okResponse(
		res,
		{
			...insertUpdateDriver,
		},
		message
	);
};

module.exports = {
	fetchDriverList,
	addUpdateDriver,
	fetchSingleDriverDetails,
	activeDeactiveDriver,
	fetchAvailableVehicleForBusiness,
	assignVehicleToDriver,
	searchDriversForVehicles,
	changeVehicleDriver,
	fetchDataForAddEditDriver,
	fetchDriverFeedback,
	fetchIndividualDriverFeedback,
	driverTrackingList,
	test_zubear,
};
