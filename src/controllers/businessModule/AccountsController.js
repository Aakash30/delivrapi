'use strict';

const Accounts = require('../../models/Accounts');
const Users = require('../../models/Users');
const Fuel = require('../../models/FuelUpdate');
const Fine = require('../../models/PoliceFine');

const moment = require('moment');
/**
 * Fetch Accounts Details based on drivers
 */

const fetchAccountDetail = async (req, res) => {

    let accountDetails;
    let totalCount = 0;
    /** Get parameters for pagination purpose */
    let page = (req.query.page) ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);
    try {
        accountDetails = await Users.query().select("user_id", "first_name", "last_name", "business_id", "business_branch_id").mergeNaiveEager("[countries, accountsUser, fuel_accounts, police_fine]").modifyEager("accountsUser", builder => {
            builder.select(builder.knex().raw("SUM( CASE WHEN account_type='credit' THEN amount ELSE 0 END) AS credited, SUM( CASE WHEN account_type='debit' THEN amount ELSE 0 END) AS debited, SUM( CASE WHEN debit_type='fuel' THEN amount ELSE 0 END) AS fuel_amount,  SUM( CASE WHEN debit_type='fine' THEN amount ELSE 0 END) AS fine_amount,  SUM( CASE WHEN debit_type='salik' THEN amount ELSE 0 END) AS salik_amount ")).groupBy("driver_id");
        }).modifyEager("fuel_accounts", builder => {
            builder.select("id", "cost", "driver_id", "image").where("is_approved", false);
        }).modifyEager("police_fine", builder => {
            builder.select("id", "driver_id", "image").where("is_approved", false);
        }).modifyEager("countries", builder => {
            builder.select("currency");
        }).where("business_id", req.user.business_id).where("user_type", "driver").where((builder) => {
            if (req.query.keyword) {
                builder.where("first_name", "ilike", "%" + req.query.keyword + "%");
            }
        }).offset(offset).limit(limit);

        let getCount = await Users.query().count("user_id").where("business_id", req.user.business_id).where("user_type", "driver").where("business_id", req.user.business_id).where("user_type", "driver").where((builder) => {
            if (req.query.keyword) {
                builder.where("first_name", "ilike", "%" + req.query.keyword + "%");
            }
        }).first();
        totalCount = getCount.count;

    } catch (error) {
        throw global.badRequestError(error.message);
    }

    let resultData = {
        accountDetails,
        totalCount
    };

    return global.okResponse(res, {
        ...resultData
    }, "");
};

const updateAccountsForDriver = async (req, res) => {
    let data = req.body;

    if (!data.driver_id) {
        throw global.badRequestError("No User found");
    }

    if (!data.account_type) {
        throw global.badRequestError("Invalid Request");
    }

    if (!data.amount || data.amount == 0) {
        throw global.badRequestError("Please enter amount");
    }

    let updateAprrovedAmountEntries = {};
    if (data.debit_type == 'fuel' || data.debit_type == 'fine') {
        updateAprrovedAmountEntries.id = data.id;
        updateAprrovedAmountEntries.is_approved = true;
        updateAprrovedAmountEntries.cost = data.amount;
        delete data.id;
    }
    let updateAccount;
    data.business_id = req.user.business_id;
    try {
        updateAccount = await Accounts.query().upsertGraph(data).returning("*");
    } catch (error) {
        console.log(error);
        throw global.badRequestError(error.message);
    }

    if (!updateAccount) {
        throw global.badRequestError("Something went wrong");
    } else {
        if (Object.keys(updateAprrovedAmountEntries).length > 0) {
            let updateStatus, updateQuery;
            if (data.debit_type == 'fuel') {
                updateQuery = Fuel.query().upsertGraph(updateAprrovedAmountEntries, {
                    relate: true,
                    unrelate: false
                });
            } else {
                updateQuery = Fine.query().upsertGraph(updateAprrovedAmountEntries, {
                    relate: true,
                    unrelate: false
                });
            }
            try {
                updateStatus = await updateQuery;
            } catch (error) {
                console.log(error);
                throw global.badRequestError(error.message);
            }
        }
    }

    return global.okResponse(res, {
        ...updateAccount
    }, "");
};

const getAccountDetails = async (req, res) => {

    if (!req.params.id) {
        throw global.notFoundError("Not found.");
    }

    let driverId = new Buffer(req.params.id, "base64").toString("ascii");

    if (isNaN(parseInt(driverId))) {
        throw global.notFoundError("Not found.");
    }

    let accountData;
    let count = 0;

    /** Get parameters for pagination purpose */
    let page = (req.query.page) ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);
    try {
        accountData = await Users.query().select([(Accounts.query().select(Accounts.knex().raw("(SUM(CASE WHEN account_type='credit' THEN amount ELSE 0 END) - SUM(CASE WHEN  account_type='debit' THEN amount ELSE 0 END) ) AS balance ")).where("driver_id", driverId).groupBy("driver_id")).as('balance'), Users.knex().raw("CONCAT(first_name, ' ', last_name) as driver")]).mergeNaiveEager("[countries, accountsUser]").modifyEager("countries", builder => {
            builder.select("currency");
        }).modifyEager("accountsUser", builder => {
            builder.select("amount", "account_type", "debit_type", "created_at").where("business_id", req.user.business_id).where((builder) => {
                if (req.query.start_date && !req.query.end_date) {
                    builder.where("created_at", ">=", moment(req.query.start_date));
                } else
                if (req.query.end_date && !req.query.start_date) {
                    builder.where("created_at", "<=", moment(req.query.end_date));
                } else
                if (req.query.start_date && req.query.end_date) {
                    builder.whereBetween("created_at", [moment(req.query.start_date), moment(req.query.end_date)]);
                }
            }).orderBy("created_at", "DESC").offset(offset).limit(limit);
        }).where("user_id", driverId).first();

        let totalCount = await Accounts.query().count().where("business_id", req.user.business_id).where("driver_id", driverId).where((builder) => {
            if (req.query.start_date && !req.query.end_date) {
                builder.where("created_at", ">=", moment(req.query.start_date));
            } else
            if (req.query.end_date && !req.query.start_date) {
                builder.where("created_at", "<=", moment(req.query.end_date));
            } else
            if (req.query.start_date && req.query.end_date) {
                builder.whereBetween("created_at", [moment(req.query.start_date), moment(req.query.end_date)]);
            }
        }).first();
        count = totalCount.count;

    } catch (error) {
        console.log(error);
        throw global.badRequestError(error.message);
    }

    let resultData = {
        accountData,
        count
    };
    return global.okResponse(res, {
        ...resultData
    }, "");
};

module.exports = {
    fetchAccountDetail,
    updateAccountsForDriver,
    getAccountDetails
};