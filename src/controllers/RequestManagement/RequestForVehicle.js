'use strict';

const VehicleRequest = require('./../../models/VehicleRequest');
const Vehicle = require('./../../models/Vehicle');
const Business = require('./../../models/Business');
const User = require('./../../models/Users');

const notification = require('./../../notification');
const updateNotifications = require('./../NotificationController');
const moment = require('moment');

/**
 * For Business Users
 */

/**
 * fetches requested made by a particular business
 * @param {*} req
 * @param {*} res
 */
const fetchRequestedVehicleList = async (req, res) => {
	/** Set the Parameters for pagination */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	let vehicleRequestList = await VehicleRequest.query()
		.select(
			'requested_vehicle_count',
			'request_number',
			'requested_on',
			'status'
		)
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.query.keyword && req.query.keyword.trim() != '') {
				builder.where('request_number', 'ilike', '%' + req.query.keyword + '%');
			}
		})
		.offset(offset)
		.limit(limit)
		.orderBy('requested_on', 'DESC');

	let total = await VehicleRequest.query()
		.count('request_id')
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.query.keyword && req.query.keyword.trim() != '') {
				builder.where('request_number', 'ilike', '%' + req.query.keyword + '%');
			}
		})
		.first();

	if (req.query.readNotice && req.query.readNotice != '') {
		let id = new Buffer(req.query.readNotice, 'base64').toString('ascii');

		updateNotifications.updateNotificationReadStatus(id);
	}

	let responseData = {
		vehicleRequestList,
		total: total.count,
	};

	// return response
	return global.okResponse(
		res,
		{
			...responseData,
		},
		''
	);
};

/**
 * add request for new vehicle
 */

const addNewVeicleRequest = async (req, res) => {
	let data = req.body;
	let getNotificationMessage = {};

	if (!data.requested_vehicle_count || data.requested_vehicle_count == 0) {
		throw global.badRequestError('Not a valid number for the request.');
	}

	data.business_id = req.user.business_id;

	let insertNewRequest = await VehicleRequest.query()
		.insert(data)
		.returning('*');

	if (!insertNewRequest) {
		throw global.badRequestError('Something went wrong');
	}

	/** needs business name for notification */
	let getBusinessData = await Business.query()
		.select('business_name')
		.where('business_id', req.user.business_id)
		.first();

	/** dynamic notification message */
	getNotificationMessage = Object.assign(
		{},
		global.global.NOTIFICATION_PAYLOAD.NEW_VEHICLE_REQUEST
	);

	let updatedMessage = getNotificationMessage.body.replace(
		'[Business]',
		getBusinessData.business_name
	);
	let newMessage = updatedMessage.replace(
		'[number]',
		data.requested_vehicle_count
	);
	getNotificationMessage.body = newMessage;
	/** needs device token of admin to send notfication */
	let userToSend = await User.query()
		.select('user_id', 'device_token', 'token')
		.where('user_id', 1)
		.first();

	let dataToNotify = [
		{
			device_type: 'WEB',
			url: 'admin/request-for-new-vehicle',
			time: moment(),
			title: getNotificationMessage.title,
			body: getNotificationMessage.body,
			receiver_id: userToSend.user_id,
			notification_type: 'NEW_VEHICLE_REQUEST',
			unique_id: insertNewRequest.request_id,
			sender_id: req.user.user_id,
		},
	];

	let tokenArray = [];
	if (userToSend.device_token != '') {
		tokenArray = [userToSend.token];

		notification.sendNotificationToUser(
			userToSend.device_token,
			getNotificationMessage
		);
	}
	updateNotifications.updateNotification(dataToNotify, tokenArray);

	// return response
	return global.okResponse(
		res,
		{
			...getNotificationMessage,
		},
		'Your requested has been submited successfully.'
	);
};

/**
 * For Admin
 */

const fetchVehicleRequestsFromBusiness = async (req, res) => {
	/** Set the Parameters for pagination */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	console.log('-----------------------', req.query.requested_raised_on);
	let vehicleRequestList = await VehicleRequest.query()
		.select(
			'request_id',
			'requested_vehicle_count',
			'alloted_vehicle_count',
			'requested_on',
			'status',
			'request_number',
			'last_updated_on'
		)
		.eager('Business')
		.modifyEager('Business', (builder) => {
			builder.select('business_name');
		})
		.where((builder) => {
			if (req.query.keyword) {
				builder.where('request_number', 'ilike', '%' + req.query.keyword + '%');
			}
			if (req.query.requested_raised_on) {
				builder.whereRaw('DATE(requested_on) = ?', [
					moment(req.query.requested_raised_on).format('YYYY-MM-DD'),
				]);
			}

			if (req.query.status) {
				builder.where('status', req.query.status.toLowerCase());
			}
		})
		.orderBy('requested_on', 'DESC')
		.limit(limit)
		.offset(offset)
		.runAfter((result, builder) => {
			return result;
		});

	let total = await VehicleRequest.query()
		.count('request_id')
		.where((builder) => {
			if (req.query.keyword) {
				builder.where('request_number', 'ilike', '%' + req.query.keyword + '%');
			}
			if (req.query.requested_raised_on) {
				builder.whereRaw('DATE(requested_on) = ?', [
					moment(req.query.requested_raised_on).format('YYYY-MM-DD'),
				]);
			}
			if (req.query.status) {
				builder.where('status', req.query.status.toLowerCase());
			}
		})
		.first();

	if (req.query.readNotice && req.query.readNotice != '') {
		let id = new Buffer(req.query.readNotice, 'base64').toString('ascii');

		updateNotifications.updateNotificationReadStatus(id);
	}

	let totalVehicle = await Vehicle.query()
		.count('vehicle_id')
		.where((builder) => {
			return builder
				.where('vehicle.is_on_lease', 'false')
				.where('vehicle.is_available', 'true');
		})
		.first();
	let responseData = {
		vehicleRequestList,
		total: total.count,
		available_vehicle_count: totalVehicle.count,
	};

	// return response
	return global.okResponse(
		res,
		{
			...responseData,
		},
		''
	);
};

const performActionOnRequest = async (req, res) => {
	try {
		let data = req.body;
		console.log(data);

		if (!data.status) {
			throw global.badRequestError('Invalid request.');
		}

		if (!req.params.id) {
			throw global.badRequestError('Invalid request');
		}
		if (data.alloted_vehicle_count == undefined) {
			data.alloted_vehicle_count = 0;
		}
		if (data.status == 'finished') {
			let retreivedata = await VehicleRequest.query()
				.select('requested_vehicle_count')
				.where('request_id', req.params.id)
				.first();
			data.alloted_vehicle_count = retreivedata.requested_vehicle_count;
		}
		let actionUpdated = await VehicleRequest.query()
			.update({
				status: data.status,
				alloted_vehicle_count: parseInt(data.alloted_vehicle_count),
			})
			.where('request_id', req.params.id)
			.returning('*');

		if (!actionUpdated) {
			throw global.badRequestError('Something went wrong.');
		}

		let reciever = actionUpdated[0].business_id;
		let userData = await User.query()
			.select('user_id', 'device_token', 'token')
			.where('business_id', reciever)
			.where('user_type', 'vendor')
			.first()
			.runAfter((result, builder) => {
				return result;
			});

		/** dynamic notification message */
		let getNotificationMessage = Object.assign(
			{},
			global.NOTIFICATION_PAYLOAD.VEHICLE_REQUEST_RESPONSE
		);

		// actionUpdated.receiver_id, actionUpdated.receiver_id
		let message;

		if (data.status.toUpperCase() == 'ONGOING') {
			message = 'Request has been processed successfully.';
		} else if (data.status.toUpperCase() == 'REJECTED') {
			message = 'Request has been rejected successfully.';
			let updatedMessage = getNotificationMessage.body.replace(
				'[requested_on]',
				actionUpdated[0].requested_on
			);
			let newMessage = updatedMessage.replace('[request_status]', 'rejected');
			getNotificationMessage.body = newMessage;
		} else {
			message = 'Request has been completed successfully.';
			let updatedMessage = getNotificationMessage.body.replace(
				'[requested_on]',
				actionUpdated[0].requested_on
			);
			let newMessage = updatedMessage.replace('[request_status]', 'rejected');
			getNotificationMessage.body = newMessage;
		}

		console.log('user', userData.user_id);

		let dataToNotify = [
			{
				device_type: 'WEB',
				url: 'business/request-for-vehicle',
				time: moment(),
				title: getNotificationMessage.title,
				body: getNotificationMessage.body,
				receiver_id: userData.user_id,
				notification_type: 'VEHICLE_REQUEST_RESPONSE',
				unique_id: req.params.id,
				sender_id: req.user.user_id,
			},
		];
		let tokenArray = [];
		if (userData.device_token != '') {
			tokenArray.push(userData.token);
			notification.sendNotificationToUser(
				userData.device_token,
				getNotificationMessage
			);
		}

		updateNotifications.updateNotification(dataToNotify, tokenArray);

		// return response
		return global.okResponse(
			res,
			{
				...actionUpdated,
			},
			message
		);
	} catch (error) {
		console.log(error);
	}
};

module.exports = {
	fetchRequestedVehicleList,
	addNewVeicleRequest,
	fetchVehicleRequestsFromBusiness,
	performActionOnRequest,
};
