'use strict';

const RepairRequest = require('./../../models/RepairRequest');
const Service = require('./../../models/Services');
const vehicleBrand = require('./../../models/VehicleBrand');
const vehicleModel = require('./../../models/VehicleModel');

const Users = require('../../models/Users');

const moment = require("moment");
const Notifications = require('../../notification');

const updateNotificationRecord = require('../NotificationController');


const fetchRequestForRepair = async (req, res) => {

    /** Set the Parameters for pagination */
    let page = (req.query.page) ? req.query.page : 1;
    let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
    let offset = req.query.offset ? req.query.offset : limit * (page - 1);

    let requestForRepair;
    try {
        requestForRepair = await RepairRequest.query().select(RepairRequest.query().knex().raw("id, request_number, requested_on, status")).joinRelation("vehicleData").joinRelation("driver").eager("[driver, vehicleData]").modifyEager("driver", builder => {
            builder.select(builder.knex().raw("CONCAT(first_name, ' ', last_name) AS driver, business_name, user_login.mobile_number")).joinRelation("Business");
        }).modifyEager("vehicleData", builder => {

            builder.select("vehicle_number", "brand_name", "model_name", "engine_number").joinRelation("vehicle_brand").joinRelation("vehicle_model");
        }).where((builder) => {
            if (req.user.user_type == "vendor") {
                builder.where("driver.business_id", req.user.business_id);
            } else if (req.user.user_type == "sub_ordinate") {
                builder.where("driver.business_id", req.user.business_id).where("driver.business_branch_id", req.user.business_branch_id);
            }
        }).where((builder) => {
            if (req.query.keyword) {
                builder.where("request_number", 'ilike', "%" + req.query.keyword + "%");
            }

            if (req.query.status) {
                builder.where("status", req.query.status);
            }

            if (req.query.request_date) {

                builder.where("requested_on", moment(req.query.request_date));
            }

            if (req.query.brand) {
                builder.where("brand", req.query.brand);
            }

            if (req.query.model) {
                builder.where("model", req.query.model);
            }

        }).orderBy("requested_on", "DESC").offset(offset).limit(limit);
    } catch (error) {
        throw global.badRequestError(error.message);
    }

    let totalCount = await RepairRequest.query().count().joinRelation("vehicleData").joinRelation("driver").where((builder) => {
        if (req.query.keyword) {
            builder.where("request_number", 'ilike', "%" + req.query.keyword + "%");
        }

        if (req.query.status) {
            builder.where("status", req.query.status);
        }

        if (req.query.startDate && req.query.endDate) {

            builder.whereBetween("requested_on", [moment(req.query.startDate), moment(req.query.endDate)]);
        }
        if (req.query.brand) {
            builder.where("brand", req.query.brand);
        }

        if (req.query.model) {
            builder.where("model", req.query.model);
        }
    }).where((builder) => {
        if (req.user.user_type == "vendor") {
            builder.where("driver.business_id", req.user.business_id);
        } else if (req.user.user_type == "sub_ordinate") {
            builder.where("driver.business_id", req.user.business_id).where("driver.business_branch_id", req.user.business_branch_id);
        }
    }).first();

    let resultData = {
        requestForRepair,
        total: totalCount.count
    };

    if (Object.keys(req.query).length == 0) {
        let vehicleBrandList = await vehicleBrand.query().select("brand_id", "brand_name");
        let vehicleModelList = await vehicleModel.query().select("model_id", "model_name", "brand_id");
        resultData.vehicleBrandList = vehicleBrandList;
        resultData.vehicleModelList = vehicleModelList;
    }
    // return response
    return global.okResponse(res, {
        ...resultData,
    }, "");
};

const requestForRepairDetails = async (req, res) => {

    if (!req.params.id) {
        throw global.BadRequestError("Invalid Request");
    }

    let request_id = new Buffer(req.params.id, "base64").toString("ascii");

    let requestDetails;
    try {

        requestDetails = await RepairRequest.query().select(RepairRequest.query().knex().raw("request_number, requested_on,status, CASE WHEN status = 'completed' THEN last_updated_on ELSE NULL END AS resolved_at,  CASE WHEN status = 'rejected' THEN last_updated_on ELSE NULL END AS rejected_at")).joinRelation("driver").eager("[requestedService, driver, vehicleData]").modifyEager("vehicleData", builder => {

            builder.select("vehicle_number", "brand", "brand_name", "model_name").joinRelation("vehicle_brand").joinRelation("vehicle_model");
        }).modifyEager("requestedService", builder => {
            builder.select("service_id", "service_category_name").joinRelation("serviceCategory");
        }).modifyEager("driver", builder => {
            builder.select(builder.knex().raw("CONCAT(first_name, ' ', last_name) AS driver, mobile_number"));
        }).where("repair_request.id", request_id).where((builder) => {
            if (req.user.user_type == "vendor") {
                builder.where("driver.business_id", req.user.business_id);
            } else if (req.user.user_type == "sub_ordinate") {
                builder.where("driver.business_id", req.user.business_id).where("driver.business_branch_id", req.user.business_branch_id);
            }
        }).first();

        let serviceArray = [];
        requestDetails.requestedService.filter((element) => {
            serviceArray.push(element.service_id);
        });

        let serviceData = await Service.query().select("service_name", "type").eager("[servicePricing]").modifyEager("[servicePricing]", builder => {

            builder.select("engine_capacity", "price", "currency").joinRelation("countries").where("brand", (requestDetails.vehicleData.brand));

        }).whereIn("list_id", serviceArray);
        let newrequestdata = Object.assign(requestDetails.requestedService, serviceData);

        requestDetails.requestedService = (newrequestdata);
        // requestDetails = await RepairRequest.query().select(RepairRequest.query().knex().raw("request_number, requested_on,status, CASE WHEN status = 'completed' THEN last_updated_on ELSE NULL END AS resolved_at,  CASE WHEN status = 'rejected' THEN last_updated_on ELSE NULL END AS rejected_at")).eager("[requestedService, driver, vehicleData]").modifyEager("vehicleData", builder => {

        //     builder.select("vehicle_number", "brand", "brand_name", "model_name").joinRelation("vehicle_brand").joinRelation("vehicle_model");
        // }).modifyEager("requestedService", builder => {
        //     builder.select("service_name", "type", "service_category_name").joinRelation("service").joinRelation("serviceCategory").eager("service.[servicePricing]").modifyEager("service.[servicePricing]", builder => {
        //         // console.log(builder.select("*").where("brand", 'repair_request:vehicleData[0].brand').toString())
        //         // builder.select("RepairRequest:vehicleData.vehicle_number");
        //         //builder.select("engine_capacity", "price").whereRaw("brand = ?", (this.brand));
        //         // console.log(builder.select("engine_capacity", "price").joinRelation("brandList").joinRelation("modelList").where("brand", RepairRequest.knex().raw("vehicleData.brand")).toSql())
        //     });
        // }).modifyEager("driver", builder => {
        //     builder.select(builder.knex().raw("CONCAT(first_name, ' ', last_name) AS driver, mobile_number"));
        // }).where("repair_request.id", req.params.id).first();

    } catch (error) {
        console.log(error);
        throw global.badRequestError(error.message);
    }

    let resultData = {
        requestDetails
    };

    // return response
    return global.okResponse(res, {
        ...resultData,
    }, "");
};

const actionOnRepairRequest = async (req, res) => {

    let data = req.body;

    if (!data.status) {
        throw global.badRequestError("Invalid Action.");
    }

    if (!data.id) {
        throw global.badRequestError("Invalid Action.");
    }

    let statusMessage = "";
    if (data.status == "ongoing") {
        statusMessage = "in processing";
    } else {
        statusMessage = "completed";
    }

    let updateRequestAction = await RepairRequest.query().update({
        status: data.status,
        last_updated_on: moment()
    }).where("id", data.id).returning("*");

    if (!updateRequestAction) {
        throw global.badRequestError("Something went wrong.");
    }
    let getDriverData = await Users.query().select("device_token", "device_type", "user_id").where("user_id", updateRequestAction[0].driver_id).first();

    let getNotificationMessage = Object.assign({}, global.NOTIFICATION_PAYLOAD.REPAIR_REQUEST_RESPONSE);

    let updatedMessage = getNotificationMessage.body.replace("[REQUEST_ID]", updateRequestAction[0].request_number).replace("[status]", statusMessage);

    getNotificationMessage.body = updatedMessage;


    /**
     * prepare notification data to save in database
     */
    let nfData = [];
    nfData.push({
        device_type: 'Phone',
        url: 'REPAIR',
        receiver_id: getDriverData.user_id,
        time: moment(),
        title: getNotificationMessage.title,
        body: getNotificationMessage.body,
        notification_type: "REPAIR_REQUEST_RESPONSE",
        unique_id: updateRequestAction[0].id,
        sender_id: req.user.user_id
    });
    updateNotificationRecord.updateNotification(nfData, []);
    if (getDriverData.device_token != "") {
        if (getDriverData.device_type.toUpperCase() == "ANDROID") {
            Notifications.sendNotificationToAndroid(getDriverData.device_token, getNotificationMessage);
        } else {
            Notifications.sendNotificationToIOS(getNotificationMessage, getDriverData.device_token);
        }
    }

    /**for mobile ends */
    let getNotificationMessageForBusiness = Object.assign({}, global.NOTIFICATION_PAYLOAD.REPAIR_REQUEST_RESPONSE_BUSINESS);

    let getBusinessData = await Users.query().select("device_token", "device_type", "token", "user_id").where("business_id", updateRequestAction[0].business_id).where("user_type", "vendor").first();


    updatedMessage = getNotificationMessageForBusiness.body.replace("[REQUEST_ID]", updateRequestAction[0].request_number).replace("[status]", statusMessage);

    getNotificationMessageForBusiness.body = updatedMessage;
    nfData = [];
    let encodedId = new Buffer(data.id.toString()).toString("base64");
    nfData.push({
        device_type: 'WEB',
        url: 'business/request-for-repair/details/' + encodedId,
        receiver_id: getBusinessData.user_id,
        time: moment(),
        title: getNotificationMessageForBusiness.title,
        body: getNotificationMessageForBusiness.body,
        notification_type: "REPAIR_REQUEST_RESPONSE_BUSINESS",
        unique_id: updateRequestAction[0].id,
        sender_id: req.user.user_id
    });
    let token = [];
    if (getBusinessData.device_token != "") {
        Notifications.sendNotificationToUser(getBusinessData.device_token, getNotificationMessage);
        token.push(getBusinessData.token);
    }
    updateNotificationRecord.updateNotification(nfData, token);


    return global.okResponse(res, {
        ...updateRequestAction,
    }, "");
};
module.exports = {
    fetchRequestForRepair,
    requestForRepairDetails,
    actionOnRepairRequest
};