'use strict';

const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

AWS.config.accessKeyId = 'AKIA47GG5BI3FHZJS4HO';
AWS.config.secretAccessKey = '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';
AWS.config.region = 'ap-south-1';

// ========================== MULTER CONFIG ================================
// const Storage = multer.diskStorage({
//   destination: function(req, file, callback) {
//     callback(null, "./images");
//   },
//   filename: function(req, file, callback) {
//     callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
//   }
// });

// const upload = multer({
//   storage: Storage
// }).array("photo", 1);

const BASE_URL = 'https://s3.ap-south-1.amazonaws.com/delivreb/';

const upload = multer({
  storage: multerS3({
    s3: new AWS.S3(),
    bucket: 'delivreb',
    acl: 'public-read',
    metadata: function (req, file, cb) {
      cb(null, {
        fieldName: file.fieldname
      });
    },
    key: function (req, file, cb) {
      console.log(file);
      cb(null, Date.now().toString() + '_' + file.originalname)
    }
  })
});
// ========================== MULTER CONFIG ================================

const UploadImages = (req, res) => {
  let uploadMultiple = upload.array('files');
  uploadMultiple(req, res, async (err) => {
    if (err) {
      console.log(err);
      return res.status(400).send(JSON.stringify({
        message: "Files submission failed.",
        image: []
      }));
    }

    req.files = await req.files.map((file) => {
      return {
        url: file.location
      };
    });
    return res.status(201).send(JSON.stringify({
      message: "Files uploaded successfully.",
      image: req.files
    }));
  });
}

const UploadImage = (req, res) => {
  // var s3 = new AWS.S3();
  let uploadOne = upload.array('file', 1);
  console.log(uploadOne);
  uploadOne(req, res, async (err) => {
    if (err) {
      return res.status(400).send(JSON.stringify({
        message: "File submission failed.",
        image: []
      }));;
    }
    req.files = await req.files.map((file) => {
      return {
        url: file.location
      };
    });

    return res.status(201).send(JSON.stringify({
      message: "File uploaded successfully.",
      image: req.files[0]
    }));
  });
}

module.exports = {
  UploadImage,
  UploadImages,
  upload
};