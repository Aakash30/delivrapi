'use strict';

const Order = require('./../../models/Order');

const RejectedOrders = require('./../../models/RejectedOrders');

const Users = require('./../../models/Users');

const BusinessBranch = require('./../../models/BusinessBranchArea');

const sendNotification = require('../../notification').sendNotificationToUser;

const updateNotificationRecord = require('../NotificationController');

const moment = require('moment');

/**
 * @Function {fetchOrdersForDriver}
 * @description: fetches the assigned orders for the logged in driver user.
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */
const fetchOrdersForDriver = async (req, res) => {
	/** Set the Parameters for pagination */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	/** Execute the query to fetch order list. */

	let orderList = await Order.query()
		.select(
			'order.order_id',
			'order.address_1',
			'order.order_date',
			'order.invoice_id',
			'order_status'
		)
		.where('driver_id', req.user.user_id)
		.where((builder) => {
			if (!req.query.status) {
				builder.where('order_status', 'assigned');
			} else {
				if (req.query.status == 'accepted') {
					builder
						.where('order_status', req.query.status)
						.orWhere('order_status', 'ongoing');
				} else {
					builder.where('order_status', req.query.status);
				}
			}
		})
		.orderBy('order_id', 'desc')
		.offset(offset)
		.limit(limit);

	let acceptedOrder = await Order.query()
		.count('order_id')
		.where((builder) => {
			builder
				.where('order_status', 'accepted')
				.orWhere('order_status', 'ongoing');
		})
		.as('acceptedOrder')
		.where('driver_id', req.user.user_id)

		.first();
	let newOrder = await Order.query()
		.count('order_id')
		.as('newOrder')
		.where('order_status', 'assigned')
		.where('driver_id', req.user.user_id)
		.first();

	/**  get the total count of order */
	let totalOrder = await Order.query()
		.count('order_id')
		.where('driver_id', req.user.user_id)
		.where((builder) => {
			if (!req.query.status) {
				builder.where('order_status', 'assigned');
			} else {
				if (req.query.status == 'accepted') {
					builder
						.where('order_status', req.query.status)
						.orWhere('order_status', 'ongoing');
				} else {
					builder.where('order_status', req.query.status);
				}
			}
		})
		.first();

	/** Prepare the response data array */
	let returnData = {
		orders: orderList,
		count: totalOrder.count,
		page: page,
		acceptedOrder: acceptedOrder.count,
		newOrder: newOrder.count,
	};
	// return response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * @Function {viewOrderDetail}
 * @description: shows the details of order
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */

const viewOrderDetail = async (req, res) => {
	try {
		let orderDetails = await Order.query()
			.select(
				'order.invoice_id',
				'order_date',
				'first_name',
				'last_name',
				'mobile_number',
				'address_1',
				'address_2',
				'latitude',
				'longitude',
				'payment_method',
				'subtotal',
				'history_id',
				'currency',
				Order.knex().raw(
					"CASE WHEN history_id IS NOT NULL THEN 'rejected' ELSE order_status END AS order_status"
				)
			)
			.leftJoin('rejected_order_history', (builder) => {
				builder
					.on('rejected_order_history.order_id', '=', 'order.order_id')
					.on('rejected_order_history.driver_id', '=', req.user.user_id);
			})
			.joinRelation('countries')
			.eager('OrderItem')
			.modifyEager('OrderItem', (builder) => {
				builder.select('item_name', 'quantity', 'price');
			})
			.where('order.order_id', req.params.orderId)
			.first();

		if (req.query.readNotice != undefined && req.query.readNotice != '') {
			updateNotificationRecord.updateNotificationReadStatus(
				req.query.readNotice
			);
		}
		// return response
		return global.okResponse(
			res,
			{
				...orderDetails,
			},
			''
		);
	} catch (error) {
		throw global.badRequestError(error.message);
	}
};

/**
 * @Function {actionOnOrder}
 * @description: Perform action on order like accept the order or reject the order
 * @param {*} req stores the request parameters
 * @param {*} res stores the response parameters
 *
 */

const actionOnOrder = async (req, res) => {
	let data = req.body;

	if (!data.order_id) {
		throw global.badRequestError('Invalid Request');
	}

	if (!data.action) {
		throw global.badRequestError('Invalid Request');
	}

	let orderUpdate, message;

	let getNotificationMessage;
	if (data.action == 'rejected') {
		orderUpdate = await Order.query()
			.update({
				order_status: 'new',
				driver_id: null,
			})
			.where('order_id', data.order_id)
			.where('driver_id', req.user.user_id);

		await RejectedOrders.query().insert({
			order_id: data.order_id,
			invoice_id: data.invoice_id,
			driver_id: req.user.user_id,
			business_id: req.user.business_id,
		});
		message = 'Order has been rejected successfully.';

		getNotificationMessage = Object.assign(
			{},
			global.NOTIFICATION_PAYLOAD.ORDER_REJECTED
		);
		//sendNotification
	} else {
		if (data.action == 'ongoing') {
			let checkOngoingOrders = await Order.query()
				.select('order_id')
				.where('order_status', 'ongoing')
				.where('driver_id', req.user.user_id)
				.first();

			if (checkOngoingOrders) {
				throw global.badRequestError('You can start only one order at a time.');
			}
		}
		orderUpdate = await Order.query()
			.update({
				order_status: data.action,
			})
			.where('order_id', data.order_id)
			.where('driver_id', req.user.user_id);

		message = 'Order has been accepted successfully.';

		getNotificationMessage = Object.assign(
			{},
			global.NOTIFICATION_PAYLOAD.ORDER_ACCEPTED
		);
	}

	let dataForNotificationRecord = [];
	let tokenData = [];

	let updatedMessage = getNotificationMessage.body
		.replace('[order_id]', data.invoice_id)
		.replace('[driver_name]', req.user.first_name + ' ' + req.user.last_name);
	getNotificationMessage.body = updatedMessage;

	let businessUser = await Users.query()
		.select('user_id', 'token', 'device_token')
		.where('business_id', req.user.business_id)
		.where('user_type', 'vendor')
		.orderBy('user_id')
		.first();

	if (businessUser.device_token != '' && businessUser.device_token != null) {
		sendNotification(businessUser.device_token, getNotificationMessage);

		tokenData.push(businessUser.token);
	}
	/**
	 * prepare notification data to save in database
	 */
	let encodedId = new Buffer(data.order_id.toString()).toString('base64');
	dataForNotificationRecord.push({
		device_type: 'WEB',
		url: 'business/order-management/details/' + encodedId,
		time: moment(),
		title: getNotificationMessage.title,
		body: getNotificationMessage.body,
		receiver_id: businessUser.user_id,
		notification_type:
			data.action == 'rejected' ? 'ORDER_REJECTED' : 'ORDER_ACCEPTED',
		unique_id: data.order_id,
		sender_id: req.user.user_id,
	});

	let checkForBusinessBranch = await BusinessBranch.query()
		.select('branch_area_id')
		.eager('users')
		.modifyEager('users', (builder) => {
			builder
				.select('user_id', 'device_token', 'token')
				.where('user_type', 'sub_ordinate');
		})
		.where('branch_area_id', req.user.business_branch_id)
		.where('type', '!=', 'mainbranch')
		.first();

	if (checkForBusinessBranch != undefined) {
		if (checkForBusinessBranch.users[0] != null) {
			if (
				checkForBusinessBranch.users[0].device_token != '' &&
				checkForBusinessBranch.users[0].device_token != null
			) {
				sendNotification(
					checkForBusinessBranch.users[0].device_token,
					getNotificationMessage
				);
				tokenData.push(checkForBusinessBranch.users[0].token);
			}

			dataForNotificationRecord.push({
				device_type: 'WEB',
				url: 'business/order-management/details/' + encodedId,
				time: moment(),
				title: getNotificationMessage.title,
				body: getNotificationMessage.body,
				receiver_id: checkForBusinessBranch.users[0].user_id,
				notification_type:
					data.action == 'rejected' ? 'ORDER_REJECTED' : 'ORDER_ACCEPTED',
				unique_id: data.order_id,
				sender_id: req.user.user_id,
			});
		}
	}

	updateNotificationRecord.updateNotification(
		dataForNotificationRecord,
		tokenData
	);

	return global.okResponse(
		res,
		{
			...orderUpdate,
		},
		message
	);
};

/**
 * Used to verify user's order - the otp is sent to the customer while creating order
 * @param {*} req
 * @param {*} res
 */
const verifyUserOrder = async (req, res) => {
	let data = req.body;

	if (!data.order_id) {
		throw global.badRequestError('Invalid Request');
	}

	if (!data.otp) {
		throw global.badRequestError('Ask customer to enter OTP.');
	}

	let verify = await Order.query()
		.select('order_id')
		.where((builder) => {
			if (data.otp != '0000') {
				builder.where('OTP', data.otp);
			}
		})
		.where('order_id', data.order_id)
		.first();

	let message;
	if (!verify) {
		throw global.badRequestError('Invalid OTP.');
	} else {
		message = 'OTP verified.';

		await Order.query()
			.update({
				OTP: '',
			})
			.where('order_id', data.order_id);
	}

	return global.okResponse(res, {}, message);
};

/**
 * Order deliver - adds driver's rating signature and change status
 * @param {*} req
 * @param {*} res
 */
const orderDelivered = async (req, res) => {
	if (!req.file) {
		throw global.badRequestError("Ask for Customer's Signature");
	}

	let data = req.body;

	if (!data.order_id) {
		throw global.badRequestError('Invalid Request');
	}
	if (!data.rating) {
		throw global.badRequestError('Provide Rating');
	}
	let updateOrder = await Order.query()
		.update({
			rating: data.rating,
			customer_signature: req.file.location,
			order_status: 'delivered',
		})
		.where('order_id', data.order_id)
		.returning('*');

	if (!updateOrder) {
		throw global.badRequestError('Something went wrong.');
	}

	let getNotificationMessage = Object.assign(
		{},
		global.NOTIFICATION_PAYLOAD.ORDER_DELIVERED
	);

	let updatedMessage = getNotificationMessage.body
		.replace('[order_id]', updateOrder[0].invoice_id)
		.replace('[driver_name]', req.user.first_name + ' ' + req.user.last_name);
	getNotificationMessage.body = updatedMessage;

	let businessUser = await Users.query()
		.select('user_id', 'token', 'device_token')
		.where('business_id', req.user.business_id)
		.where('user_type', 'vendor')
		.orderBy('user_id')
		.first();

	let dataForNotificationRecord = [];
	let tokenData = [];

	if (businessUser.device_token != '' && businessUser.device_token != null) {
		sendNotification(businessUser.device_token, getNotificationMessage);
		tokenData.push(businessUser.token);
	}

	let encodedId = new Buffer(data.order_id.toString()).toString('base64');

	dataForNotificationRecord.push({
		device_type: 'WEB',
		url: 'business/order-management/details/' + encodedId,
		time: moment(),
		title: getNotificationMessage.title,
		body: getNotificationMessage.body,
		receiver_id: businessUser.user_id,
		notification_type: 'ORDER_DELIVERED',
		unique_id: data.order_id,
		sender_id: req.user.user_id,
	});

	let checkForBusinessBranch = await BusinessBranch.query()
		.select('branch_area_id')
		.eager('users')
		.modifyEager('users', (builder) => {
			builder
				.select('user_id', 'device_token', 'token')
				.where('user_type', 'sub_ordinate');
		})
		.where('branch_area_id', req.user.business_branch_id)
		.where('type', '!=', 'mainbranch')
		.first();

	if (checkForBusinessBranch != undefined) {
		if (checkForBusinessBranch.users[0] != null) {
			if (
				checkForBusinessBranch.users[0].device_token != '' &&
				checkForBusinessBranch.users[0].device_token != null
			) {
				sendNotification(
					checkForBusinessBranch.users[0].device_token,
					getNotificationMessage
				);
				tokenData.push(checkForBusinessBranch.users[0].token);
			}

			dataForNotificationRecord.push({
				device_type: 'WEB',
				url: 'business/order-management/details/' + encodedId,
				time: moment(),
				title: getNotificationMessage.title,
				body: getNotificationMessage.body,
				receiver_id: checkForBusinessBranch.users[0].user_id,
				notification_type: 'ORDER_DELIVERED',
				unique_id: data.order_id,
				sender_id: req.user.user_id,
			});
		}
	}

	updateNotificationRecord.updateNotification(
		dataForNotificationRecord,
		tokenData
	);

	return global.okResponse(res, {}, 'Thanks for the feedback.');
};

const orderHistory = async (req, res) => {
	try {
		let page = req.query.page ? req.query.page : 1;
		let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
		let offset = req.query.offset ? req.query.offset : limit * (page - 1);

		let history;
		if (req.query.status == 'delivered') {
			history = await Order.query()
				.select(
					'order_id',
					'invoice_id',
					'order_date',
					'address_1',
					'address_2'
				)
				.where((builder) => {
					builder.where('order_status', req.query.status);
				})
				.where('driver_id', req.user.user_id)
				.orderBy('order_id', 'desc')
				.offset(offset)
				.limit(limit);
			let totalHistory = await Order.query()
				.count('order_id')
				.where((builder) => {
					builder.where('order_status', req.query.status);
				})
				.where('driver_id', req.user.user_id)
				.first();

			return global.okResponse(
				res,
				{
					history,
					total: totalHistory.count,
				},
				'Order History Fetched Successfully'
			);
		} else if (req.query.status == 'rejected') {
			history = await RejectedOrders.query()
				.select(
					'rejected_order_history.order_id',
					'rejected_order_history.invoice_id',
					'order_date',
					'address_1',
					'address_2'
				)
				.joinRelation('rejected')
				.where('rejected_order_history.driver_id', req.user.user_id)
				.orderBy('order_id', 'desc')
				.offset(offset)
				.limit(limit);

			let totalHistory = await RejectedOrders.query()
				.count('rejected_order_history.order_id')
				.joinRelation('rejected')
				.where('rejected_order_history.driver_id', req.user.user_id)
				.first();
			// await RejectedOrders.query().select("").eager("rejected").modifyEager("rejected", builder => {
			//     return builder.select("order_id", "order_date", "order_status", "address_1", "address_2")
			// }).where('driver_id', req.user.user_id);

			return global.okResponse(
				res,
				{
					history,
					total: totalHistory.count,
				},
				'Order History Fetched Successfully'
			);
		} else throw global.badRequestError('Bad request.');
	} catch (err) {
		console.log(err.message);
		throw global.badRequestError('History could not be fetched.');
	}
};

module.exports = {
	fetchOrdersForDriver,
	viewOrderDetail,
	actionOnOrder,
	verifyUserOrder,
	orderDelivered,
	orderHistory,
};
