const SOSList = require('./../../models/SOSList');
const Users = require('../../models/Users');
const BusinessBranch = require('../../models/BusinessBranchArea');

const Email = require('./../../middlewares/email');

const request = require("request");
const CONFIG = require('../../config');



/**
 * Fetch SOS list for the driver
 * @param {*} req 
 * @param {*} res 
 */
const fetchSOSList = async (req, res) => {

    let fetchData = await SOSList.query().select("id", "contact_name", "contact_number", "is_default").where("driver_id", req.user.user_id);

    let resultData = {
        SOSList: fetchData
    };

    return okResponse(res, {
        ...resultData,
    }, "");
};

/**
 * add sos contact for the driver
 * @param {*} req 
 * @param {*} res 
 */
const addSOSContact = async (req, res) => {

    let data = req.body;

    if (!data.contact_name || data.contact_name.trim() == "") {
        throw badRequestError("Please enter contact name.");
    }

    if (!data.contact_number || data.contact_number.trim() == "") {
        throw badRequestError("Please enter contact number.");
    }

    data.driver_id = req.user.user_id;

    let insertSOSContact = await SOSList.query().insert(data);


    if (!insertSOSContact) {
        throw badRequestError("Something went wrong.");
    }

    return okResponse(res, {
        ...insertSOSContact,
    }, "Contact has been updated successfully.");

};

/**
 * set default contact for sos list
 * @param {*} req 
 * @param {*} res 
 */

const setDefaultContact = async (req, res) => {

    if (!req.params.id) {
        throw badRequestError("Invalid request.");
    }
    let defaultSet = await SOSList.query().update({
        "is_default": (SOSList.query().knex().raw("(CASE WHEN id = " + req.params.id + "  THEN true ELSE false END)"))
    }).where("driver_id", req.user.user_id).returning("*");

    let returnData = {
        sosList: defaultSet
    };

    return okResponse(res, {
        ...returnData,
    }, "");
};

/**
 * remove contact from sos list
 * @param {*} req 
 * @param {*} res 
 */

const removeContact = async (req, res) => {

    if (!req.params.id) {
        throw badRequestError("Invalid request.");
    }
    let deleteContact = await SOSList.query().delete().where("id", req.params.id);

    if (!deleteContact) {
        throw badRequestError("Something went wrong.");
    }

    return okResponse(res, {
        ...deleteContact,
    }, "Contact removed successfully.");
};

/**
 * Function to send SOS
 * @param {*} req 
 * @param {*} res 
 */
const sendSOSToBusiness = async (req, res) => {

    if (!req.query.latitude || !req.query.longitude) {
        throw badRequestError("location not found");
    }
    let fetchLocation = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + req.query.latitude + "," + req.query.longitude + "&key=" + CONFIG.mapid;
    console.log(fetchLocation)
    await request({
        url: fetchLocation,
        json: true
    }, async (error, {
        body
    }) => {
        if (error) {
            throw badRequestError("Unable to connect to location services!");
        } else if (body.results.length == 0) {
            throw badRequestError("Unable to find location.");
        } else {
            try {
                let location = body.results[0].formatted_address;

                let businessUser = await Users.query().select("user_id", "first_name", "email_id").where("business_id", req.user.business_id).where("user_type", "vendor").orderBy("user_id").first();

                Email.sendEmailT(businessUser.email_id, "SOS call", "", {
                    title: "SOS call!",
                    name: businessUser.first_name,
                    message1: "SOS call from your driver - " + req.user.first_name + " " + req.user.last_name,
                    message2: "Location - " + location,
                    buttonTitle: "",
                    buttonLink: "#"
                });


                let checkForBusinessBranch = await BusinessBranch.query().select("branch_area_id").eager("users").modifyEager("users", builder => {
                    builder.select("user_id", "first_name", "last_name", "email_id").where("user_type", "sub_ordinate");
                }).where("branch_area_id", req.user.business_branch_id).where("type", "!=", "mainbranch").first();

                if (checkForBusinessBranch != undefined) {
                    if (checkForBusinessBranch.users[0] != null) {
                        Email.sendEmailT(checkForBusinessBranch.users[0].email_id, "SOS call", "", {
                            title: "SOS call!",
                            name: businessUser.first_name,
                            message1: "SOS call from your driver - " + req.user.first_name + " " + req.user.last_name,
                            message2: "Location - " + location,
                            buttonTitle: "",
                            buttonLink: "#"
                        });
                    }
                }
                return global.okResponse(res, {}, "Message sent");
            } catch (error) {
                throw badRequestError(error.message);
            }


        }
    });



};

sendSOSMail = async (error, location) => {

    if (error) {

    } else {

    }
};

module.exports = {
    fetchSOSList,
    addSOSContact,
    removeContact,
    setDefaultContact,
    sendSOSToBusiness
};