'use strict';
const Users = require('./../../models/Users');
const Fine = require('./../../models/PoliceFine');
const FuelUpdate = require('./../../models/FuelUpdate');
const DriverLocation = require('./../../models/DriverLocation');
const Country = require('../../models/Countries');
const CONFIG = require('../../config');

const moment = require('moment');
const request = require('request');
const knex = require('knex');

//-----------------------------------------------Fines-----------------------------------------------//
//add new fine
const addFine = async (req, res) => {
    try {
        let files = [];
        req.files.forEach(file => {
            files.push({
                driver_id: req.user.user_id,
                fine_id: req.user.user_id + moment.now(),
                image: file.location
            });
        });
        let newFine = await Fine.query().insert(files).returning('*');
        if (!newFine) throw global.badRequestError("There was an error. Please try again later");
        return global.okResponse(res, newFine, "");
    } catch (error) {
        console.log(error);
        throw global.badRequestError("bad request");
    }
};

//fetch fines
const fetchFines = async (req, res) => {
    try {
        let page = (req.query.page) ? req.query.page : 1;
        let limit = req.query.limit ? req.query.limit : 10;
        let offset = req.query.offset ? req.query.offset : limit * (page - 1);

        let count = await Fine.query().countDistinct('fine_id').where(builder => {
            if (req.user.user_type == 'driver') return builder.where("driver_id", req.user.user_id);
            if (req.user.user_type == 'vendor') return builder.whereIn("driver_id", Users.query().select("user_id").where("business_id", req.user.user_id));

        }).first();

        let fine = await Fine.query().select('fine_id', 'created_at', knex.raw("ARRAY_AGG (image) as images"))
            .groupBy('fine_id', 'created_at')
            .where(builder => {
                if (req.user.user_type == 'driver') return builder.where("driver_id", req.user.user_id);
                if (req.user.user_type == 'vendor') return builder.whereIn("driver_id", Users.query().select("user_id").where("business_id", req.user.user_id));

            })
            .orderBy('created_at', 'desc')
            .offset(offset)
            .limit(limit);

        return global.okResponse(res, {
            count: count.count,
            fine: fine
        }, "");
    } catch (error) {
        console.log(error);
        throw global.badRequestError("bad request");
    }
};


//-----------------------------------------------Fuel Updates-----------------------------------------------//
//add new fuel update
const addFuelUpdate = async (req, res) => {
    try {
        if (!req.body.distance) throw global.badRequestError('distance is required');
        if (!req.body.cost) throw global.badRequestError('cost is required');

        let newFine = await FuelUpdate.query().insert({
            distance: req.body.distance,
            cost: req.body.cost,
            driver_id: req.user.user_id,
            image: req.file.location
        }).returning('*');
        if (!newFine) throw global.badRequestError("There was an error. Please try again later");

        return global.okResponse(res, newFine[0], "");
    } catch (error) {
        console.log(error);
        throw global.badRequestError("bad request");
    }
};

//fetch fuel updates
const fetchFuelUpdates = async (req, res) => {
    try {
        let page = (req.query.page) ? req.query.page : 1;
        let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
        let offset = req.query.offset ? req.query.offset : limit * (page - 1);

        let count = await FuelUpdate.query().count().where(builder => {
            if (req.user.user_type == 'driver') return builder.where("driver_id", req.user.user_id);
            if (req.user.user_type == 'vendor') return builder.whereIn("driver_id", Users.query().select("user_id").where("business_id", req.user.user_id));

        }).first();

        let fupdates = await FuelUpdate.query().select([Country.query().select("currency").where("country_id", req.user.country).as("currency"), "id", "driver_id", "distance", "cost", "image", "created_at"])
            .where(builder => {
                if (req.user.user_type == 'driver') return builder.where("driver_id", req.user.user_id);
                if (req.user.user_type == 'vendor') return builder.whereIn("driver_id", Users.query().select("user_id").where("business_id", req.user.user_id));

            }).orderBy("created_at", "desc").offset(offset).limit(limit);

        return global.okResponse(res, {
            count: count.count,
            fuel_updates: fupdates
        }, "");
    } catch (error) {
        console.log(error);
        throw global.badRequestError("bad request");
    }
};

//-----------------------------------------------Fuel Updates-----------------------------------------------//
//upload a photo
const addDriverPhoto = async (req, res) => {
    try {
        if (!req.body.lat) throw global.badRequestError('latitude is required');
        if (!req.body.lng) throw global.badRequestError('longitude is required');
        if (!req.file.location) throw global.badRequestError('image is required');

        let newDriverPhoto;

        let address = "";
        let dataResponse = await request({
            url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + req.body.lat + "," + req.body.lng + "&key=" + CONFIG.mapid,
            json: true
        }, async (error, response) => {

            if (error) {
                address = "Unknown location";
            } else {
                if (response.body.results) {
                    address = response.body.results[0].formatted_address;
                }

            }
            let data = {
                lat: req.body.lat,
                lng: req.body.lng,
                driver_id: req.user.user_id,
                image: req.file.location,
                address: address
            };
            newDriverPhoto = await DriverLocation.query().insert(data).returning('*');
            if (!newDriverPhoto) throw global.badRequestError("There was an error. Please try again later");

            // return newDriverPhoto[0];
        });

        return global.okResponse(res, newDriverPhoto, "Photo shared successfuly");

    } catch (error) {
        console.log(error);
        throw global.badRequestError("bad request");
    }
};

const updateDriverAddress = async (dataToInsert, res) => {

};

//fetch driver photos
const fetchDriverPhotos = async (req, res) => {
    try {
        let page = (req.query.page) ? req.query.page : 1;
        let limit = req.query.limit ? req.query.limit : 10;
        let offset = req.query.offset ? req.query.offset : limit * (page - 1);

        let count = await DriverLocation.query().count().where(builder => {
            if (req.user.user_type == 'driver') return builder.where("driver_id", req.user.user_id);
            if (req.user.user_type == 'vendor') return builder.whereIn("driver_id", Users.query().select("user_id").where("business_id", req.user.user_id));

        }).first();

        let dlocs = await DriverLocation.query().select("id", "driver_id", "lat", "lng", "image", "created_at")
            .where(builder => {
                if (req.user.user_type == 'driver') return builder.where("driver_id", req.user.user_id);
                if (req.user.user_type == 'vendor') return builder.whereIn("driver_id", Users.query().select("user_id").where("business_id", req.user.user_id));

            }).orderBy("created_at", "desc").offset(offset).limit(limit);

        return global.okResponse(res, {
            count: count.count,
            driver_location: dlocs
        }, "");
    } catch (error) {
        console.log(error);
        throw global.badRequestError("bad request");
    }
};

module.exports = {
    addFine,
    fetchFines,
    addFuelUpdate,
    fetchFuelUpdates,
    addDriverPhoto,
    fetchDriverPhotos
};