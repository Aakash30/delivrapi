const Users = require('./../../models/Users');

/**
 * fetches user driver profile for app
 * @param {*} req 
 * @param {*} res 
 */
const fetchMyDriverProfile = async (req, res) => {

    let driverProfile = await Users.query().select("first_name", "last_name", "profile_image", "email_id", "mobile_number", "license_document", "is_available").eager("vehicles").modifyEager("vehicles", builder => {
        builder.select("");
        builder.eager("vehicle").modifyEager("vehicle", builder => {
            builder.select("vehicle_number", "registration_card", "insurance", "brand_name", "model_name").joinRelation("vehicle_brand").joinRelation("vehicle_model").first();
        }).first();
    }).where("user_id", req.user.user_id).first();

    // return response
    return global.okResponse(res, {
        ...driverProfile,
    }, "");
};

/** Mark Availability for driver
 * If set false driver won't be available for delivry
 * This can be set by the driver through app
 */
const updateMyAvailability = async (req, res) => {
    let data = req.body;

    if (data.is_available == undefined) {
        throw global.badRequestError("Invalid Request");
    }

    let updateDriver = await Users.query().update({
        "is_available": data.is_available
    }).where("user_id", req.user.user_id);

    if (!updateDriver) {
        throw global.badRequestError("Something went wrong.");
    }
    // return response
    return global.okResponse(res, {
        ...updateDriver,
    }, "Status has been updated successfully.");

};

/** Mark Availability for driver
 * If set false driver won't be available for delivry
 * This can be set by the driver through app
 */
const updateMyProfile = async (req, res) => {
    let data = req.body;

    if (!data.first_name || data.first_name.trim() == "") {
        throw global.badRequestError("Please enter first name");
    }

    if (!data.last_name || data.last_name.trim() == "") {
        throw global.badRequestError("Please enter last name");
    }

    if (req.file) {
        data.profile_image = req.file.location;
    }
    //data.user_id = req.user.user_id;

    let updateDriver = await Users.query().update(data).where("user_id", req.user.user_id);

    if (!updateDriver) {
        throw global.badRequestError("Something went wrong.");
    }
    // return response
    return global.okResponse(res, {
        ...updateDriver,
    }, "Status has been updated successfully.");

};

const updatePassword = async (req, res) => {

    let data = req.body;

    if (!data.password || data.password.trim() == "") {
        throw global.badRequestError("Please enter new password.");
    }

    if (!data.current_password || data.current_password.trim() == "") {
        throw global.badRequestError("Please enter existing password.");
    }


    let user = await Users.query().where("user_id", req.user.user_id).first();


    if (!await user.comparePassword(data.current_password)) {
        err = true;
        throw global.badRequestError('Invalid current password');
    }

    delete data.current_password;

    let updateDriver = await Users.query().update(data).where("user_id", req.user.user_id);


    if (!updateDriver) {
        throw global.badRequestError("Something went wrong.")
    }
    // return response
    return global.okResponse(res, {
        ...updateDriver,
    }, "Password has been updated successfully.");

};
module.exports = {
    fetchMyDriverProfile,
    updateMyAvailability,
    updateMyProfile,
    updatePassword
};