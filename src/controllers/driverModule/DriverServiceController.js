'use strict';

const ServiceCategory = require('../../models/ServiceCategory');
const Services = require('../../models/Services');
const User = require('./../../models/Users');
const VehicleMapping = require('./../../models/VehicleMapping');
const RepairRequest = require('./../../models/RepairRequest');
const sendNotification = require('../../notification').sendNotificationToUser;

const updateNotificationRecord = require('../NotificationController');

const moment = require('moment');

const { transaction } = require('objection');

/**
 * Fetches data for service request page. Shows service category, vehicle and user data.
 * @param {*} req
 * @param {*} res
 */
const fetchDataForServiceRequest = async (req, res) => {
	let serviceCategory = await ServiceCategory.query()
		.select('service_category_id', 'service_category_name')
		.where('status', 'on');

	let vehicleData = await VehicleMapping.query()
		.select('vehicle.vehicle_id', 'vehicle_number', 'brand_name', 'model_name')
		.joinRelation('vehicle.[vehicle_brand]')
		.joinRelation('vehicle.[vehicle_model]')
		.where('driver_id', req.user.user_id)
		.where('business_id', req.user.business_id);

	let userData = await User.query()
		.select(
			User.knex().raw("CONCAT(first_name, ' ', last_name) As driver, email_id")
		)
		.where('user_id', req.user.user_id);
	let resultData = {
		serviceCategory,
		vehicleData,
		userData,
	};
	// return response
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

/**
 * Filters services based on service category
 * @param {*} req
 * @param {*} res
 */
const filterServicesBasedOnCategory = async (req, res) => {
	if (!req.params.id) {
		throw global.badRequestError('Invalid Request.');
	}

	let services;
	try {
		services = await Services.query()
			.select('list_id', 'service_name')
			.where('service_category', req.params.id);
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	let resultData = {
		services,
	};
	// return response
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

/**
 * Submits driver's request for repair
 * @param {*} req
 * @param {*} res
 */
const requestForRepair = async (req, res) => {
	let data = req.body;

	if (!data.vehicle_id) {
		throw global.badRequestError('Invalid Request');
	}

	if (!data.requestedService) {
		throw global.badRequestError('Please Post your request.');
	} else if (data.requestedService.length == 0) {
		throw global.badRequestError(
			'Atleast one service for repair should be mentioned'
		);
	}

	data.driver_id = req.user.user_id;
	data.business_id = req.user.business_id;
	let upsertData;
	try {
		upsertData = await transaction(RepairRequest.knex(), (trx) => {
			return RepairRequest.query(trx)
				.upsertGraph(data, {
					relate: true,
					unrelate: true,
				})
				.returning('*');
		});

		let getNotificationForAdmin = Object.assign(
			{},
			global.NOTIFICATION_PAYLOAD.NEW_REPAIR_REQUEST
		);
		let getNotificationMessage = Object.assign(
			{},
			global.NOTIFICATION_PAYLOAD.NEW_REPAIR_REQUEST_BUSINESS
		);

		let updatedMessage = getNotificationMessage.body
			.replace('[REQUEST_ID]', upsertData.request_number)
			.replace('[driver_name]', req.user.first_name + ' ' + req.user.last_name);
		getNotificationMessage.body = updatedMessage;
		getNotificationForAdmin.body = updatedMessage;

		let businessUser = await User.query()
			.select('user_id', 'token', 'device_token')
			.where('business_id', req.user.business_id)
			.where('user_type', 'vendor')
			.orderBy('user_id')
			.first();

		let getSuperAdmin = await User.query()
			.select('user_id', 'token', 'device_token')
			.where('user_id', 1)
			.first();

		let dataForNotificationRecord = [];
		let tokenData = [];
		/**
		 * prepare notification data to save in database
		 */
		let encodedId = new Buffer(upsertData.id.toString()).toString('base64');

		dataForNotificationRecord.push({
			device_type: 'WEB',
			url: 'business/request-for-repair/details/' + encodedId,
			time: moment(),
			title: getNotificationMessage.title,
			body: getNotificationMessage.body,
			receiver_id: businessUser.user_id,
			notification_type: 'NEW_REPAIR_REQUEST_BUSINESS',
			unique_id: upsertData.id,
			sender_id: req.user.user_id,
		});

		dataForNotificationRecord.push({
			device_type: 'WEB',
			url: 'admin/request-for-repair/details/' + encodedId,
			time: moment(),
			title: getNotificationForAdmin.title,
			body: getNotificationForAdmin.body,
			receiver_id: getSuperAdmin.user_id,
			notification_type: 'NEW_REPAIR_REQUEST',
			unique_id: upsertData.id,
			sender_id: req.user.user_id,
		});

		if (businessUser.device_token != '') {
			sendNotification(businessUser.device_token, getNotificationMessage);
			tokenData.push(businessUser.token);
		}

		if (getSuperAdmin.device_token != '') {
			sendNotification(getSuperAdmin.device_token, getNotificationForAdmin);
			tokenData.push(getSuperAdmin.token);
		}
		/**
		 * Below code commented is used if we need to notify branch of the corresponding driver
		 */
		// let checkForBusinessBranch = await BusinessBranch.query().select("branch_area_id").eager("users").modifyEager("users", builder => {
		//     builder.select("user_id", "device_token", "token").where("user_type", "sub_ordinate");
		// }).where("branch_area_id", req.user.business_branch_id).where("type", "!=", "mainbranch").first();

		// if (checkForBusinessBranch != undefined) {
		//     if (checkForBusinessBranch.users != null) {
		//         sendNotification(checkForBusinessBranch.users[0].device_token, getNotificationMessage);
		//         dataForNotificationRecord.push({
		//             receiver_id: checkForBusinessBranch.users[0].user_id
		//         });
		//         tokenData.push(checkForBusinessBranch.users[0].token);

		//     }
		// }

		updateNotificationRecord.updateNotification(
			dataForNotificationRecord,
			tokenData
		);
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	return global.okResponse(
		res,
		upsertData,
		'Your request has been submitted. We will get back to you soon.'
	);
};

/**
 * Fetches history for the requests made by driver
 * @param {*} req
 * @param {*} res
 */
const fetchHistoryRepairRequest = async (req, res) => {
	/** Set the Parameters for pagination */
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	let historyRequest = await RepairRequest.query()
		.select(
			RepairRequest.query()
				.knex()
				.raw(
					"id, request_number, requested_on,status, CASE WHEN status = 'completed' THEN last_updated_on ELSE NULL END AS resolved_at,  CASE WHEN status = 'rejected' THEN last_updated_on ELSE NULL END AS rejected_at"
				)
		)
		.where('driver_id', req.user.user_id)
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.query.fetch && req.query.fetch.trim() != '') {
				builder.where('status', 'completed').orWhere('status', 'rejected');
			} else {
				builder.where('status', 'new').orWhere('status', 'ongoing');
			}
		})
		.orderBy('id', 'desc')
		.offset(offset)
		.limit(limit);

	let totalCount = await RepairRequest.query()
		.count()
		.where('driver_id', req.user.user_id)
		.where('business_id', req.user.business_id)
		.where((builder) => {
			if (req.query.fetch && req.query.fetch.trim() != '') {
				builder.where('status', 'completed').orWhere('status', 'rejected');
			} else {
				builder.where('status', 'new').orWhere('status', 'ongoing');
			}
		})
		.first();

	let resultData = {
		historyRequest,
		count: totalCount.count,
	};

	// return response
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

/**
 * get the repair request details
 * @param {*} req
 * @param {*} res
 */
const repairRequestDetails = async (req, res) => {
	if (!req.params.id) {
		throw global.badRequestError('Invalid Request.');
	}

	let requestDetails;
	try {
		requestDetails = await RepairRequest.query()
			.select(
				RepairRequest.query()
					.knex()
					.raw(
						"request_number, requested_on,status, CASE WHEN status = 'completed' THEN last_updated_on ELSE NULL END AS resolved_at,  CASE WHEN status = 'rejected' THEN last_updated_on ELSE NULL END AS rejected_at"
					)
			)
			.eager('[requestedService, driver, vehicleData]')
			.modifyEager('requestedService', (builder) => {
				builder
					.select('service_name', 'service_category_name')
					.joinRelation('service')
					.joinRelation('serviceCategory');
			})
			.modifyEager('driver', (builder) => {
				builder.select(
					builder
						.knex()
						.raw("CONCAT(first_name, ' ', last_name) AS driver, email_id")
				);
			})
			.modifyEager('vehicleData', (builder) => {
				builder
					.select('vehicle_number', 'brand_name', 'model_name')
					.joinRelation('vehicle_brand')
					.joinRelation('vehicle_model');
			})
			.where('id', req.params.id)
			.first();

		if (
			typeof req.query.readNotice != undefined &&
			req.query.readNotice != ''
		) {
			updateNotificationRecord.updateNotificationReadStatus(
				req.query.readNotice
			);
		}
	} catch (error) {
		console.log(error.message);
		throw global.badRequestError(error.message);
	}

	let resultData = {
		requestDetails,
	};

	// return response
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

module.exports = {
	fetchDataForServiceRequest,
	filterServicesBasedOnCategory,
	requestForRepair,
	fetchHistoryRepairRequest,
	repairRequestDetails,
};
