const AWSController = require('./aws/aws');
const AppVersionController = require('./app-version/appversion');
const AuthController = require('./user/AuthController');
const BusinessController = require('./user/BusinessController');
const VehicleController = require('./VehicleController');
const CountryCitySettingController = require('./systemSetting/CountryCitySettingController');
const DriverController = require('./businessModule/DriverController');
const OrderController = require('./businessModule/OrderController');
const BusinessAreaUser = require('./businessModule/UserManagement');
const ServiceManagement = require('./systemSetting/ServiceManagementController');
const PaymentController = require('./PaymentController');
const RequestForRepair = require('./RequestManagement/RequestForRepair');
const RequestForVehicles = require('./RequestManagement/RequestForVehicle');
const CouponController = require('./businessModule/CouponController');
const BrandModelController = require('./systemSetting/BrandModelSettingController');
const Dashboard = require('./user/dashboard');
const Accounts = require('./businessModule/AccountsController');

/** For Driver App */
const DriverOrderController = require('./driverModule/DriverOrderController');
const SOSController = require('./driverModule/SOSController');
const DriverProfileController = require('./driverModule/DriverProfileController');
const DriverServiceController = require('./driverModule/DriverServiceController');

const NotificationController = require('./NotificationController');

module.exports = {
    AWSController,
    AppVersionController,
    AuthController,
    BusinessController,
    VehicleController,
    CountryCitySettingController,
    BrandModelController,
    DriverController,
    OrderController,
    DriverOrderController,
    BusinessAreaUser,
    SOSController,
    DriverProfileController,
    ServiceManagement,
    PaymentController,
    DriverServiceController,
    RequestForRepair,
    RequestForVehicles,
    CouponController,
    NotificationController,
    Dashboard,
    Accounts
};