const AppVersion = require('../../models/AppVersion');
const {
    transaction
} = require('objection');
const crypto = require('crypto');

const getAppVersion = async (req, res) => {
    let data = req.body;
    let versioncheck;
    if (!data.platform_type) {
        throw badRequestError('Android/IOS type is required');
    }
    if (!data.user_type) {
        throw badRequestError('User type is required');
    }
    if (!data.version) {
        throw badRequestError('version is required');
    }

    if (data.platform_type == "ios") {
        versioncheck = await AppVersion.query().select('id').where('app_type', data.user_type).where('ios_min_version', '<=', data.version).first();
    }
    else if (data.platform_type == "android") {
        versioncheck = await AppVersion.query().select('id').where('app_type', data.user_type).where('android_min_mersion', '<=', data.version).first();
    }
    // var tt = AppVersion.query().where('app_type', data.user_type).where('ios_min_version', '>=', data.version).first().toString();
    // console.log(tt);
    console.log('this is the version check', versioncheck);
    if (versioncheck) {
        // throw notFoundError('No user registered');
        throw badRequestError('');
    }
    return okResponse(res, versioncheck, "This version is became to obsolete. Please go to Play Store to download the latest version");
}

module.exports = {
    getAppVersion
}
