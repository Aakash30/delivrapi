'use strict';
const moment = require('moment');

/**
 * Only for Super Admin
 */

const Business = require('../../models/Business');
const ContactRequest = require('../../models/ContactRequest');

const City = require('../../models/City');
const Country = require('../../models/Countries');

const Payments = require('../../models/Payment');

/**
 * Only for business
 */

const Orders = require('../../models/Order');
const Users = require('../../models/Users');
const Branch = require('../../models/BusinessBranchArea');
const VehicleMapping = require('../../models/VehicleMapping');
/** */

const Vehicle = require('../../models/Vehicle');
const VehicleRequest = require('../../models/VehicleRequest');
const Coupons = require('../../models/Coupon');
const RepairRequest = require('../../models/RepairRequest');

const fetchAdminDashboard = async (req, res) => {
	let dashboardData = [];
	try {
		dashboardData = await Business.query()
			.select(
				Business.knex().raw(
					"'Total Business' AS card_title, count(business_id) AS total, 'total-business.svg' AS icon, 'admin/business-list' AS url"
				)
			)
			.where('status', 'active')
			.unionAll(
				[
					Vehicle.query()
						.select(
							Vehicle.knex().raw(
								"'Total Bikes' AS card_title, count(vehicle_id) AS total, 'total-bikes.svg' AS icon, 'admin/vehicles-list' AS url"
							)
						)
						.where('is_available', 'true'),
					Vehicle.query()
						.select(
							Vehicle.knex().raw(
								"'Total Available Bikes' AS card_title, count(vehicle_id) AS total, 'total-bikes-available.svg' AS icon, 'admin/vehicles-list' AS url"
							)
						)
						.where('is_available', 'true')
						.where('is_on_lease', 'false'),
					Vehicle.query()
						.select(
							Vehicle.knex().raw(
								"'Total Bikes On Lease' AS card_title, count(vehicle_id) AS total, 'total-bikes-lease.svg' AS icon, 'admin/vehicles-list' AS url"
							)
						)
						.where('is_available', 'true')
						.where('is_on_lease', 'true'),
					VehicleRequest.query().select(
						VehicleRequest.knex().raw(
							"'Request From Business' AS card_title, count(request_id) AS total, 'total-requests.svg' AS icon, 'admin/request-for-new-vehicle' AS url"
						)
					),
					Coupons.query().select(
						Coupons.knex().raw(
							"'Total Coupons' AS card_title, count(coupon_id) AS total, 'total-coupons.svg' AS icon, 'admin/coupon' AS url"
						)
					),
					Payments.query()
						.select(
							Payments.knex().raw(
								"'Payments' AS card_title, sum(amount_paid) AS total, 'total-payments.svg' AS icon, 'admin/payment' AS url"
							)
						)
						.where('is_paid', true),
					RepairRequest.query().select(
						RepairRequest.knex().raw(
							"'Total Request For Repair' AS card_title, count(id) AS total, 'total-request-repair.svg' AS icon, 'admin/request-for-repair' AS url"
						)
					),
					ContactRequest.query().select(
						ContactRequest.knex().raw(
							"'Total Request For Module' AS card_title, count(id) AS total, 'total-requests-module.svg' AS icon, 'admin/request-for-module' AS url"
						)
					),
					City.query()
						.select(
							City.knex().raw(
								"'Total Cities' AS card_title, count(city_id) AS total, 'total-cities.svg' AS icon, 'admin/country' AS url"
							)
						)
						.where('status', 'active'),
					Country.query()
						.select(
							Country.knex().raw(
								"'Total Countries' AS card_title, count(country_id) AS total, 'total-countries.svg' AS icon, 'admin/country' AS url"
							)
						)
						.where('status', 'active'),
				],
				true
			);
	} catch (error) {
		console.log(error);
	}
	let resultData = {
		dashboardData,
	};
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

const fetchBusinessDashboard = async (req, res) => {
	let dashboardData = [];
	try {
		let initialQuery;
		let today = moment('000000', 'HH:mm:ss');
		let queryArray = [];
		if (req.user.user_role != '') {
			let roles = JSON.parse(req.user.user_role);
			roles.forEach((role) => {
				switch (role) {
					case 'order-management':
						queryArray.push(
							Orders.query()
								.select(
									Orders.knex().raw(
										"'Total Orders' AS card_title, count(order_id) AS total, 'total-orders.svg' AS icon, 'business/order-management' AS url"
									)
								)
								.where('business_id', req.user.business_id)
								.where((builder) => {
									if (req.user.user_type == 'sub_ordinate')
										builder.where(
											'business_branch_id',
											req.user.business_branch_id
										);
								})
						);
						queryArray.push(
							Orders.query()
								.select(
									Orders.knex().raw(
										"'Total New Orders' AS card_title, count(order_id) AS total, 'total-orders.svg' AS icon, 'business/order-management' AS url"
									)
								)
								.where('business_id', req.user.business_id)
								.where('order_status', 'new')
								.where((builder) => {
									if (req.user.user_type == 'sub_ordinate')
										builder.where(
											'business_branch_id',
											req.user.business_branch_id
										);
								})
						);
						queryArray.push(
							Orders.query()
								.select(
									Orders.knex().raw(
										"'Total Delivered Orders' AS card_title, count(order_id) AS total, 'total-orders.svg' AS icon, 'business/order-management' AS url"
									)
								)
								.where('business_id', req.user.business_id)
								.where('order_status', 'delivered')
								.where((builder) => {
									if (req.user.user_type == 'sub_ordinate')
										builder.where(
											'business_branch_id',
											req.user.business_branch_id
										);
								})
						);
						queryArray.push(
							Orders.query()
								.select(
									Orders.knex().raw(
										"'Total Order Revenue' AS card_title, sum(subtotal) AS total, 'total-payments.svg' AS icon, 'business/order-management' AS url"
									)
								)
								.where('business_id', req.user.business_id)
								.where('order_status', 'delivered')
								.where((builder) => {
									if (req.user.user_type == 'sub_ordinate')
										builder.where(
											'business_branch_id',
											req.user.business_branch_id
										);
								})
						);
						break;
					case 'driver-management':
						queryArray.push(
							Users.query()
								.select(
									Users.knex().raw(
										"'Total Drivers' AS card_title, count(user_id) AS total, 'total-drivers.svg' AS icon, 'business/driver-list' AS url"
									)
								)
								.where('business_id', req.user.business_id)
								.where('user_type', 'driver')
								.where((builder) => {
									if (req.user.user_type == 'sub_ordinate')
										builder.where(
											'business_branch_id',
											req.user.business_branch_id
										);
								})
						);
						break;
					case 'vehicle-details':
						queryArray.push(
							VehicleMapping.query()
								.select(
									VehicleMapping.knex().raw(
										"'Total Vehicles' AS card_title, count(map_id) AS total, 'total-bikes.svg' AS icon, 'business/vehicle-list' AS url"
									)
								)
								.where('business_id', req.user.business_id)
								//.where('assigned_to_business_to', today)
								.where((builder) => {
									if (req.user.user_type == 'sub_ordinate')
										builder.where(
											'business_area_id',
											req.user.business_branch_id
										);
								})
						);
						break;
					case 'branch-management':
						queryArray.push(
							Branch.query()
								.select(
									Branch.knex().raw(
										"'Total Branches' AS card_title, count(branch_area_id) AS total, 'total-requests-module.svg' AS icon, 'business/branch' AS url"
									)
								)
								.where('business_id', req.user.business_id)
								.where('status', 'active')
						);
						break;
					case 'raise-vehicle-request':
						queryArray.push(
							VehicleRequest.query()
								.select(
									VehicleRequest.knex().raw(
										"'Total Vehicles Requests' AS card_title, count(request_id) AS total, 'total-bikes.svg' AS icon, 'business/request-for-vehicle' AS url"
									)
								)
								.where('business_id', req.user.business_id)
						);
						break;
					case 'coupon-management':
						queryArray.push(
							Coupons.query()
								.select(
									Coupons.knex().raw(
										"'Total Coupons' AS card_title, count(coupon_id) AS total, 'total-coupons.svg' AS icon, 'business/coupon' AS url"
									)
								)
								.where('business_id', req.user.business_id)
								.where((builder) => {
									if (req.user.user_type == 'sub_ordinate')
										builder.where(
											'business_branch_id',
											req.user.business_branch_id
										);
								})
						);
						break;
					case 'request-for-repair':
						queryArray.push(
							RepairRequest.query()
								.select(
									RepairRequest.knex().raw(
										"'Total Repair Requests' AS card_title, count(id) AS total, 'total-request-repair.svg' AS icon, 'business/request-for-repair' AS url"
									)
								)
								.where('business_id', req.user.business_id)
						);
						break;
					case 'user-management':
						queryArray.push(
							Users.query()
								.select(
									RepairRequest.knex().raw(
										"'Total Admins' AS card_title, count(user_id) AS total, 'total-active-users.svg' AS icon, 'business/admin-list' AS url"
									)
								)
								.where('business_id', req.user.business_id)
								.where('user_type', 'sub_ordinate')
								.where((builder) => {
									if (req.user.user_type == 'sub_ordinate')
										builder.where(
											'business_branch_id',
											req.user.business_branch_id
										);
								})
						);
						break;
				}
			});

			if (queryArray[0]) {
				initialQuery = queryArray[0];
				queryArray.shift();
				if (queryArray.length == 0) {
					dashboardData = await initialQuery;
				} else {
					dashboardData = await initialQuery.unionAll(queryArray);
				}
			}
		}
	} catch (error) {
		console.log(error);
	}
	let resultData = {
		dashboardData,
	};
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

module.exports = {
	fetchAdminDashboard,
	fetchBusinessDashboard,
};
