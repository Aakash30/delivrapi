'use strict';
const Moment = require('moment');
const MomentRange = require('moment-range'); //range plugin for better difference and manipulation
const moment = MomentRange.extendMoment(Moment);
const Email = require('./../../middlewares/email');
const knex = require('knex');

const jwt = require('jsonwebtoken');
require('./../../global_functions');

const { transaction } = require('objection');

/** Load Models */
const Business = require('./../../models/Business');
const BusinessTypeList = require('./../../models/BusinessTypeList');
const BusinessBranchArea = require('./../../models/BusinessBranchArea');
const Users = require('./../../models/Users');
const Country = require('./../../models/Countries');
const City = require('./../../models/City');
const UserTypeRoles = require('./../../models/UserTypeRoles');
const VehicleMapping = require('./../../models/VehicleMapping');
const Vehicle = require('./../../models/Vehicle');
const VehicleMappingHistory = require('./../../models/VehicleMappingHistory');
const Batch = require('./../../models/Batch');

/** fetching business type list API
 * Description: Used to fetch business type list
 * Return: Response in JSON Format
 **/
const fetchBusinessType = async (req, res) => {
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	// execute query to fetch data
	let businessTypeListData = await BusinessTypeList.query()
		.select('id', 'business_type', 'status')
		.where((builder) => {
			// check if keyword is passed and apply condition accordingly
			if (req.query.keyword) {
				builder.where('business_type', 'ilike', '%' + req.query.keyword + '%');
			}
		})
		.offset(offset)
		.limit(limit);

	let totalBusinessType = await BusinessTypeList.query()
		.count('id')
		.where((builder) => {
			// check if keyword is passed and apply condition accordingly
			if (req.query.keyword) {
				builder.where('business_type', 'ilike', '%' + req.query.keyword + '%');
			}
		})
		.first();

	if (!businessTypeListData) {
		throw global.notFoundError('No record found.');
	}

	let returnData = {
		count: totalBusinessType.count,
		businessTypeListData: businessTypeListData,
	};
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/** add/update business type list API
 * Description: Used to add / update business type list
 * Params: data.businessType - stores the business type data
 * Params: req.params.businessTypeId - is the unique id; is passed when API is called for updation
 * Return: Response in JSON Format along with the unique token in header
 **/
const addUpdateBusinessType = async (req, res) => {
	let data = req.body;

	// chek for the missing data
	if (!data.businessType) {
		throw global.badRequestError('Please enter Business Types.');
	}

	let insertBusiness, message;

	// if no id is provided then insetion will take place
	if (!req.params.businessTypeId) {
		// execute insert query
		insertBusiness = await BusinessTypeList.query()
			.insert({
				business_type: data.businessType,
				created_by: req.user.user_id,
			})
			.returning('*');

		message = 'Business type has been added successfully.';
	} else {
		// execute update query
		insertBusiness = await BusinessTypeList.query()
			.context({
				business_type_id: req.params.businessTypeId,
			})
			.where('id', req.params.businessTypeId)
			.update({
				business_type: data.businessType,
				updated_by: req.user.user_id,
			})
			.returning('*');

		message = 'Business type has been updated successfully.';
	}

	// if not executed throw error
	if (!insertBusiness) {
		throw global.badRequestError('Something went wrong.');
	}
	// return response
	return global.okResponse(
		res,
		{
			...insertBusiness,
		},
		message
	);
};

/** fetch single business type API
 * Description: Used to add / update business type list
 * Params: data.businessType - stores the business type data
 * Params: req.params.businessTypeId - is the unique id; is passed when API is called for updation
 * Return: Response in JSON Format along with the unique token in header
 **/
const fetchSingleBusinessType = async (req, res) => {
	if (!req.params.businessTypeId) {
		throw global.badRequestError('Invalid request.');
	}

	const businessTypeData = await BusinessTypeList.query()
		.select('id', 'business_type', 'status')
		.where('id', req.params.businessTypeId)
		.first();

	if (!businessTypeData) {
		throw global.badRequestError('Invalid request.');
	}

	const businessData = await Business.query()
		.select('business_id', 'business_name', 'email_id', 'mobile_number')
		.eager('vehicleMapping')
		.modifyEager('vehicleMapping', (builder) => {
			builder.count('vehicle_id').groupBy('business_id').first();
		})
		.where('business_type', req.params.businessTypeId);

	let resultData = {
		businessTypeData,
		businessData,
	};
	return global.okResponse(
		res,
		{
			...resultData,
		},
		''
	);
};

/** update business type status API
 * Description: Used to change business type status, if it is set to block then all it's related user will be
 * inactive and bikes assigned to it will be unassigned. This action can not be undone.
 * Params: data.businessType - stores the business type data
 * Params: req.params.businessTypeId - is the unique id; is passed when API is called for updation
 * Return: Response in JSON Format along with the unique token in header
 **/

const updateBusinessTypeStatus = async (req, res) => {
	if (!req.params.businessTypeId) {
		throw global.badRequestError('Invalid request.');
	}

	let data = req.body;

	if (!data.status) {
		throw global.badRequestError('Invalid request.');
	}

	let message;

	let updateBusinessType = await BusinessTypeList.query()
		.where('id', req.params.businessTypeId)
		.update({
			status: data.status,
			updated_by: req.user.user_id,
		})
		.returning('id');

	// if not executed throw error
	if (!updateBusinessType) {
		throw global.badRequestError('Something went wrong.');
	}
	let businessStatus = data.status == 'block' ? 'deactive' : 'active';
	let updateBusiness = await Business.query()
		.where('business_type', req.params.businessTypeId)
		.update({
			status: businessStatus,
			updated_by: req.user.user_id,
		})
		.returning('business_id');

	if (updateBusiness) {
		let updateUser;

		updateBusiness.forEach(async (businessIds) => {
			if (businessStatus == 'active') {
				updateUser = await Users.query()
					.where('business_id', businessIds.business_id)
					.update({
						user_status: Users.query()
							.knex()
							.raw(
								"(CASE WHEN is_user_verified  THEN 'active' ELSE 'pending' END)"
							),
						updated_by: req.user.user_id,
					})
					.returning('business_id');
			} else {
				updateUser = await Users.query()
					.where('business_id', businessIds.business_id)
					.update({
						user_status: businessStatus,
						updated_by: req.user.user_id,
					})
					.returning('user_id');
			}
		});
	}

	if (data.status == 'active') {
		message = 'Business type has been activated successfully.';
	} else {
		message = 'Business type has been blocked successfully.';
	}
	// return response
	return global.okResponse(
		res,
		{
			...updateBusinessType,
		},
		message
	);
};

/** fetching business - user list API
 * Description: Used to fetch business user list data. It also sends country, city, businessType data which will be used for filters
 * Return: Response in JSON Format
 **/
const fetchBusinessUsers = async (req, res) => {
	let page = req.query.page ? req.query.page : 1;
	let limit = req.query.limit ? req.query.limit : global.PER_PAGE;
	let offset = req.query.offset ? req.query.offset : limit * (page - 1);

	// execute query to fetch data join with user_login to show the data

	let business = await Business.query()
		.select(
			'business.business_id',
			'business_name',
			'business.email_id',
			'business.mobile_number',
			'business.status'
		)
		.eager('[businessType, businessUsers, businessBranch]')
		.modifyEager('businessType', (builder) => {
			builder.select('business_type');
		})
		.modifyEager('businessUsers', (builder) => {
			builder
				.select('first_name', 'last_name', 'is_user_verified', 'user_id')
				.where('user_type', 'vendor');
		})
		.modifyEager('businessBranch', (builder) => {
			builder.select('').where('type', 'mainbranch');
			builder
				.eager('[countries, cities]')
				.modifyEager('countries', (builder) => {
					builder.select('country');
				})
				.modifyEager('cities', (builder) => {
					builder.select('city');
				});
		})
		.joinRelation('businessType')
		.joinRelation('businessUsers')
		.joinRelation('businessBranch')
		.where((builder) => {
			// check if keyword is passed and apply condition accordingly
			if (req.query.keyword) {
				builder.where('business_name', 'ilike', '%' + req.query.keyword + '%');
				builder.orWhere('first_name', 'ilike', '%' + req.query.keyword + '%');
				builder.orWhere('last_name', 'ilike', '%' + req.query.keyword + '%');
			}

			if (req.query.businessType) {
				builder.where('business.business_type', '=', req.query.businessType);
			}

			if (req.query.city) {
				builder.where('businessBranch.city', '=', req.query.city);
			}

			if (req.query.country) {
				builder.where('businessBranch.country', '=', req.query.country);
			}

			if (req.query.status) {
				builder.where('business.status', '=', req.query.status);
			}
		})
		.where('type', 'mainbranch')
		.groupBy('business.business_id')
		.orderBy('business.created_at', 'DESC')
		.offset(offset)
		.limit(limit);

	let totalBusiness = await Business.query()
		.select(Business.knex().raw('count(distinct business.business_id)'))
		.joinRelation('businessType')
		.joinRelation('businessUsers')
		.joinRelation('businessBranch')
		.where((builder) => {
			// check if keyword is passed and apply condition accordingly
			if (req.query.keyword) {
				builder.where('business_name', 'ilike', '%' + req.query.keyword + '%');

				builder.orWhere('first_name', 'ilike', '%' + req.query.keyword + '%');
				builder.orWhere('last_name', 'ilike', '%' + req.query.keyword + '%');
			}

			if (req.query.businessType) {
				builder.where('business.business_type', '=', req.query.businessType);
			}

			if (req.query.city) {
				builder.where('businessBranch.city', '=', req.query.city);
			}

			if (req.query.country) {
				builder.where('businessBranch.country', '=', req.query.country);
			}

			if (req.query.status) {
				builder.where('business.status', '=', req.query.status);
			}
		})
		.where('type', 'mainbranch')
		.first();

	let country, city, businessType, userPermissionArray;
	if (Object.keys(req.query).length == 0) {
		// fetch data for country
		country = await Country.query().select(
			'country_id',
			'country',
			'country_code'
		);

		// fetch data for city
		city = await City.query().select('city_id', 'city', 'country_id');

		// fetch data for business type list
		businessType = await BusinessTypeList.query();

		// userPermissionArray = await UserTypeRoles.query().where("role_type", "custom");
	}

	// prepare response array
	let returnData = {
		count: totalBusiness.count,
		business,
		country,
		city,
		businessType,
		page,
	};

	// send response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/**
 * Fetches data while adding / editing data
 * @param {*} req
 * @param {*} res
 */

const fetchDataForAddEditBusiness = async (req, res) => {
	let cityData = await City.query().select(
		'city_id',
		'city',
		'country_id',
		'status'
	);

	let countryData = await Country.query().select(
		'country_id',
		'country',
		'status',
		'country_code',
		'alpha_code'
	);

	// fetch data for business type list
	let businessType = await BusinessTypeList.query().select(
		'status',
		'id',
		'business_type'
	);

	let returnData = {
		cityList: cityData,
		countryList: countryData,
		businessType: businessType,
	};

	if (req.params.businessId) {
		let business = await Business.query()
			.select(
				'business.business_id',
				'business_name',
				'business.email_id',
				'business.mobile_number',
				'business.status',
				'alternate_number',
				'license',
				'agreement',
				'business_type',
				'business_logo',
				'bank_statement',
				'credit_application_form',
				'other_document'
			)
			.eager('[businessUsers, businessBranch, businessType]')
			.modifyEager('businessType', (builder) => {
				builder.select('id', 'business_type', 'status');
			})
			.modifyEager('businessUsers', (builder) => {
				builder
					.select(
						'first_name',
						'last_name',
						'user_id',
						'designation',
						'emirates_id',
						'passport_document'
					)
					.where('user_type', 'vendor');
			})
			.modifyEager('businessBranch', (builder) => {
				builder
					.select(
						'branch_area_id',
						'address_line_1',
						'address_line_2',
						'postal_Code',
						'latitude',
						'longitude',
						'address_branch_name',
						'country',
						'city'
					)
					.where('type', 'mainbranch');
			})
			.where('business_id', req.params.businessId)
			.first();
		returnData.business = business;
	}
	// send response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

/** add update business - user API
 * Description: Used to add / update business details
 * Return: Response in JSON Format
 **/
const addUpdateBusiness = async (req, res) => {
	try {
		let data = req.body;
		console.log(data);

		let businessInsertUpdate, userInsertUpdate, message;
		if (data.agreement != undefined && data.agreement.length == 0)
			data.agreement = null;

		if (data.license != undefined && data.license.length == 0)
			data.license = null;
		if (data.bank_statement != undefined && data.bank_statement.length == 0)
			data.bank_statement = null;

		if (
			data.credit_application_form != undefined &&
			data.credit_application_form.length == 0
		)
			data.credit_application_form = null;
		if (data.other_document != undefined && data.other_document.length == 0)
			data.other_document = null;
		if (
			data.businessUsers.emirates_id != undefined &&
			data.businessUsers.emirates_id.length == 0
		)
			data.businessUsers.emirates_id = null;
		if (
			data.businessUsers.passport_document != undefined &&
			data.businessUsers.passport_document.length == 0
		)
			data.businessUsers.passport_document = null;

		if (!data.business_name || data.business_name.trim() == '') {
			throw global.badRequestError('Please enter business name.');
		}

		if (!data.business_type) {
			throw global.badRequestError('Please select business type.');
		}

		if (!data.email_id || data.email_id.trim() == '') {
			throw global.badRequestError('Please enter email id.');
		}

		if (!data.mobile_number || data.mobile_number.trim() == '') {
			throw global.badRequestError('Please enter mobile number.');
		}

		if (req.file) {
			data.business_logo = req.file.location;
		}
		if (data.businessBranch) {
			if (!data.businessBranch.country) {
				throw global.badRequestError('Please select country.');
			}

			if (!data.businessBranch.city) {
				throw global.badRequestError('Please select city.');
			}

			if (
				!data.businessBranch.address_line_1 ||
				data.businessBranch.address_line_1.trim() == ''
			) {
				throw global.badRequestError('Please enter street address.');
			}

			if (
				!data.businessBranch.address_line_2 ||
				data.businessBranch.address_line_2.trim() == ''
			) {
				throw global.badRequestError('Please enter address.');
			}

			if (
				!data.businessBranch.latitude ||
				data.businessBranch.latitude.toString().trim() == ''
			) {
				throw global.badRequestError('Please provide location.');
			}

			if (
				!data.businessBranch.longitude ||
				data.businessBranch.longitude.toString().trim() == ''
			) {
				throw global.badRequestError('Please provide location.');
			}

			if (data.businessBranch.branch_area_id == null) {
				delete data.businessBranch.branch_area_id;
			}
		} else {
			throw global.badRequestError('Please choose address.');
		}

		if (data.businessUsers) {
			if (
				!data.businessUsers.first_name ||
				data.businessUsers.first_name.trim() == ''
			) {
				throw global.badRequestError('Please enter first name.');
			}

			if (
				!data.businessUsers.last_name ||
				data.businessUsers.last_name.trim() == ''
			) {
				throw global.badRequestError('Please enter last name.');
			}

			data.businessUsers.email_id = data.email_id;
			data.businessUsers.mobile_number = data.mobile_number;

			if (data.businessUsers.user_id == null) {
				delete data.businessUsers.user_id;
			}
		} else {
			throw global.badRequestError('Please add user details.');
		}
		let verification;

		if (!data.business_id) {
			// if (!data.agreement) {
			//     throw global.badRequestError("Please provide agreement copy.");
			// }

			// if (!data.license) {
			//     throw global.badRequestError("Please provide license copy.");
			// }

			// if (!data.businessUsers.emirates_id) {
			//     throw global.badRequestError("Please provide emirates-Id copy.");
			// }

			data.businessUsers.user_type = 'vendor';
			verification = await jwt.sign(
				{
					email: data.businessUsers.email_id,
				},
				global.CONFIG.jwt_encryption
			);

			data.businessUsers.verification = verification;

			let userRolesResult = await UserTypeRoles.query()
				.select('roles')
				.where('user_type', 'vendor');

			let userRoles = [];

			userRolesResult.forEach((element) => {
				userRoles.push(element.roles);
			});
			data.businessUsers.user_role = JSON.stringify(userRoles);
		}

		try {
			businessInsertUpdate = await transaction(Business.knex(), (trx) => {
				return (
					/** execute query */
					Business.query(trx)
						.upsertGraph(data, {
							relate: true,
							unrelate: false,
						})
						.returning('*')
				);
			});
		} catch (error) {
			console.log(error);
			throw global.badRequestError(error.message);
		}

		if (!businessInsertUpdate) {
			throw global.badRequestError('Something went wrong.');
		}

		if (!data.business_id) {
			let link = global.GLOBAL_CLIENT_URL + 'activate/' + verification;
			message =
				'Business has been added successfully. An Email has been sent to the user to activate account.';

			Email.sendEmailT(
				data.businessUsers.email_id,
				'Welcome to Delivr! Confirm Your Email',
				'',
				{
					title: 'Welcome to Delivr!',
					name: data.businessUsers.first_name,
					message1: "You're on your way! Let's confirm your email address.",
					message2:
						'By clicking on the following link, you are confirming your email address.',
					buttonTitle: 'Confirm Email Address',
					buttonLink: link,
				}
			);
		} else {
			message = 'Business has been updated successfully.';
		}

		return global.okResponse(
			res,
			{
				...businessInsertUpdate,
			},
			message
		);
	} catch (error) {
		console.log(error);
	}
};

/** blockOrActivate - user API
 * Description: Used to block or activate business. When any business is blocked all users related to him are blocked
 * Return: Response in JSON Format
 **/
const blockOrActivatebusiness = async (req, res) => {
	if (!req.params.businessId) {
		throw global.badRequestError('Invalid request.');
	}

	let data = req.body;

	if (!data.status) {
		throw global.badRequestError('Invalid request.');
	}

	let message;

	let updateBusiness = await Business.query()
		.where('business_id', req.params.businessId)
		.update({
			status: data.status,
			updated_by: req.user.user_id,
		})
		.returning('business_id');

	let updateUser;
	if (data.status == 'active') {
		updateUser = await Users.query()
			.where('business_id', req.params.businessId)
			.update({
				user_status: Users.query()
					.knex()
					.raw(
						"(CASE WHEN user_login.is_user_verified  THEN 'active' ELSE 'pending' END)"
					),
				updated_by: req.user.user_id,
			})
			.returning('business_id');
	} else {
		updateUser = await Users.query()
			.where('business_id', req.params.businessId)
			.update({
				user_status: data.status,
				updated_by: req.user.user_id,
			})
			.returning('user_id');
	}

	// if not executed throw error
	if (!updateBusiness && !updateUser) {
		throw global.badRequestError('Something went wrong.');
	}

	if (data.status == 'active') {
		message = 'Business has been activated successfully.';
	} else {
		message = 'Business has been deactivated successfully.';
	}

	let returnData = {
		business: updateBusiness,
		user: updateUser,
	};
	// return response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		message
	);
};

const fetchSingleBusinessUser = async (req, res) => {
	try {
		if (!req.params.businessId) {
			throw global.badRequestError('Invalid request.');
		}
		const dateObject = new Date();
		const month =
			parseInt(dateObject.getMonth()) + 1 >= 10
				? parseInt(dateObject.getMonth()) + 1
				: '0' + (parseInt(dateObject.getMonth()) + 1);
		let today =
			dateObject.getFullYear() + '-' + month + '-' + dateObject.getDate();

		let business = await Business.query()
			.select(
				'business.business_id',
				'business_name',
				'business.email_id',
				'business.mobile_number',
				'business.status',
				'alternate_number',
				'agreement',
				'license',
				'bank_statement',
				'credit_application_form',
				'other_document',
				'business_logo'
			)
			.eager('[businessType, businessUsers, businessBranch]')
			.modifyEager('businessType', (builder) => {
				builder.select('business_type', 'id');
			})
			.modifyEager('businessUsers', (builder) => {
				builder
					.select(
						'first_name',
						'last_name',
						'user_id',
						'designation',
						'emirates_id',
						'passport_document'
					)
					.where('user_type', 'vendor');
			})
			.modifyEager('businessBranch', (builder) => {
				builder
					.select(
						'address_branch_name as name',
						'postal_Code as pincode',
						'branch_area_id',
						'address_line_1',
						'address_line_2'
					)
					.where('type', 'mainbranch');
				builder
					.eager('[countries, cities]')
					.modifyEager('countries', (builder) => {
						builder.select('country_id', 'country');
					})
					.modifyEager('cities', (builder) => {
						builder.select('city_id', 'city');
					});
			})
			.where('business_id', req.params.businessId)
			.first();

		let vehicleList = await VehicleMapping.query().where((builder) => {
			builder
				.where('business_id', req.params.businessId)
				.whereRaw('assigned_to_business_to >= ?', [today]);
		});

		let batch = await Batch.query()
			.where({
				business_id: req.params.businessId,
			})
			.eager('vehicleMapping.vehicle.[vehicle_brand, vehicle_model]')
			.modifyEager('vehicleMapping', (builder) => {
				return builder.select('map_id', 'vehicle_id');
				// .count('batch_id', {as: 'batches'}).count('vehicle_id', {as: 'vehicles'})
			})
			.modifyEager('[vehicleMapping.vehicle]', (builder) => {
				return builder.select('color', 'engine_number', 'vehicle_number');
			});
		console.log(business);
		//console.log(JSON.parse(business.bank_statement));
		let returnData = {
			business: business,
			batch: batch,
			vehicleList: vehicleList,
		};
		return global.okResponse(
			res,
			{
				...returnData,
			},
			''
		);
	} catch (error) {
		console.log(error);
	}
};

const assignVehicleToVendor = async (req, res) => {
	if (!req.params.businessId) {
		throw global.badRequestError('Invalid request.');
	}
	let data = req.body;

	if (!data.vehicleList) {
		throw global.badRequestError('Select atleast one vehicle.');
	}

	if (!data.startDate) {
		throw global.badRequestError('Select the lease time duration.');
	}

	if (!data.endDate) {
		throw global.badRequestError('Select the lease time duration.');
	}

	if (data.vehicleList.length == 0) {
		throw global.badRequestError('Select atleast one vehicle.');
	}

	var vehicleInput = [],
		payments = [];
	let vehicleTrackInput = [];

	let defaultAddress = await BusinessBranchArea.query()
		.select('branch_area_id')
		.where('business_id', req.params.businessId)
		.where('type', 'mainbranch')
		.first();

	//create empty rows in payments table according to payment count
	for (let i = 0; i < data.installment_count; i++) {
		payments.push({
			amount_paid: 0,
			created_by: req.user.user_id,
			payment_id: 'IN' + Date.now().toString() + i, //Date.now() alone creates the same payment id, so 'i' was appended.
			payment_offset:
				i == data.installment_count - 1 ? data.payment_offset : '',
		});
	}
	data.vehicleList.forEach((element) => {
		vehicleInput.push({
			vehicle_id: element,
			business_id: req.params.businessId,
			assigned_to_business_from: data.startDate,
			assigned_to_business_to: data.endDate,
			created_by: req.user.user_id,
			business_area_id: defaultAddress.branch_area_id,
			vehicle: {
				is_on_lease: true,
				updated_by: req.user.user_id,
				vehicle_id: element,
			},
		});
		vehicleTrackInput.push({
			vehicle_id: element,
			business_id: req.params.businessId,
			assinged_by: req.user.user_id,
			assigned_on: new Date().toISOString(),
		});
	});

	// moment("2020-09-30", "YYYY-MM-DD").diff(moment("2019-09-01", "YYYY-MM-DD"), 'months', true)

	// let paymentsCount = 0, extraDays = 0, extraMonths = 0;

	// let range = moment.range(data.startDate, data.endDate);
	// extraDays = dateDiff(data.startDate, data.endDate).days;

	// if(data.installment_type == 1) {
	//     paymentsCount = range.diff('months', true);
	// }
	// if(data.installment_type == 2) {
	//     paymentsCount = Math.ceil((range.diff('months', true))/3);
	//     extraMonths = Math.floor(dateDiff(data.startDate, data.endDate).months%3);
	//     console.log(extraMonths)
	// }
	// if(data.installment_type == 3) {
	//     paymentsCount = Math.ceil((range.diff('months', true))/6);
	//     extraMonths = Math.floor(dateDiff(data.startDate, data.endDate).months%6);
	// }

	// console.log(paymentsCount+' : '+extraMonths+' : '+extraDays);

	let batchInsert = {
		business_id: req.params.businessId,
		slug: parseInt(Date.now()),
		paid_price: 0,
		installment_type: req.body.installment_type, //1=monthly, 2=quarterly, 3=half-yearly
		installment_counts: req.body.installment_count,
		assigned_to_business_from: req.body.startDate,
		assigned_to_business_to: req.body.endDate,
		total_price: req.body.totalPrice,
		vehicleMapping: vehicleInput,
		payments: payments,
	};

	// res.json(batchInsert); return;
	let vehicleAssign;
	try {
		vehicleAssign = await transaction(Batch.knex(), (trx) => {
			return Batch.query(trx)
				.upsertGraph(batchInsert, {
					relate: true,
					unrelate: true,
				})
				.eager('[vehicleMapping]')
				.modifyEager('vehicleMapping', (builder) => {
					builder.eager('vehicle');
				});
		});
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	// try {
	//     console.log(vehicleInput);
	//     vehicleAssign = await transaction(VehicleMapping.knex(),trx => {
	//         return (VehicleMapping.query(trx).upsertGraph(vehicleInput, {relate: true,unrelate: false}))
	//     });
	// } catch (error) {

	//     throw global.badRequestError(error.message);
	// }

	if (!vehicleAssign) {
		throw global.badRequestError('Invalid request.');
	}

	await VehicleMappingHistory.query().insert(vehicleTrackInput);

	return global.okResponse(
		res,
		{
			...vehicleAssign,
		},
		'Vehicles has been assigned successfully.'
	);
};

const dateDiff = (end, start) => {
	var a = moment(start, 'YYYY-MM-DD');
	var b = moment(end, 'YYYY-MM-DD');

	var years = a.diff(b, 'year');
	b.add(years, 'years');

	var months = a.diff(b, 'months');
	b.add(months, 'months');

	var days = a.diff(b, 'days');

	return {
		years: years,
		months: months,
		days: days,
	};
};

module.exports = {
	fetchBusinessType,
	addUpdateBusinessType,
	fetchSingleBusinessType,
	updateBusinessTypeStatus,
	fetchBusinessUsers,
	addUpdateBusiness,
	blockOrActivatebusiness,
	fetchSingleBusinessUser,
	assignVehicleToVendor,
	fetchDataForAddEditBusiness,
};
