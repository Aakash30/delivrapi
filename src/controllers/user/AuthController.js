'use strict';

const validator = require('validator');
const User = require('./../../models/Users');
const Country = require('./../../models/Countries');
const jwt = require('jsonwebtoken');
const Email = require('./../../middlewares/email');
const sendSMS = require('../../middlewares/sms').sendSms;
const sendSms_twilio = require('../../middlewares/sms').sendSms_twilio;

//const Token = require('./../../models/Token');

/** login API
 * Description: Used to validate user Login Details and generates unique token for the user to work across
 * Params: data.email - stores the unique email id which was used while logging in (other then driver)
 * Params: data.mobile_number - stores the unique mobile number which was used while logging in (for driver only)
 * Params: data.password - stores the password used for the user to validate the user
 * Params: data.userType - stores the type of user; possible values super_admin, vendor, driver
 * Return: Response in JSON Format along with the unique token in header
 **/
const Login = async (req, res) => {
	let data = req.body;
	console.log(data);
	/** Validate for the empty fields */

	if (!data.userType) {
		throw global.badRequestError('Invalid Request.');
	}

	if (data.userType == 'driver') {
		if (!data.mobile_number) {
			throw global.badRequestError(
				'Please enter your registered contact number.'
			);
		}

		let mobile = data.mobile_number.split('-');
		if (!validator.isMobilePhone(mobile[1])) {
			throw global.badRequestError('Please enter a valid mobile number.');
		}
	} else {
		if (!data.email) {
			throw global.badRequestError('Please enter your registered email id.');
		}

		if (!validator.isEmail(data.email)) {
			throw global.badRequestError('Please enter a valid email id.');
		}
	}

	if (!data.password) {
		throw global.badRequestError('Please enter your password.');
	}

	let err;

	/** Execute Query to fetch the user */
	let user;
	try {
		user = await User.query()
			.select(
				'user_id',
				'first_name',
				'last_name',
				'email_id',
				'mobile_number',
				'alternate_number',
				'profile_image',
				'user_type',
				'user_role',
				'user_status',
				'password',
				'is_user_verified'
			)
			.where((builder) => {
				if (data.userType == 'driver') {
					builder.where('mobile_number', data.mobile_number);
				} else {
					builder.where('email_id', data.email);
				}
			})
			.where((builder) => {
				if (data.userType == 'vendor') {
					builder
						.where('user_type', 'vendor')
						.orWhere('user_type', 'sub_ordinate');
				} else {
					builder.where('user_type', data.userType);
				}
			})
			.first()
			.runAfter((result, builder) => {
				return result;
			});
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	// if user not found
	if (!user) {
		err = true;
		if (data.userType == 'driver') {
			throw global.badRequestError('Contact Number is not registered.');
		} else {
			throw global.badRequestError('Email id is not registered.');
		}
	}

	// // password not matched
	if (!(await user.comparePassword(data.password))) {
		err = true;
		throw global.badRequestError('Please enter valid credentials.');
	}

	// not an active user

	if (user.user_status == 'pending') {
		if (data.userType != 'driver') {
			err = true;
			throw global.unverifiedError('Your account is not verified yet.');
		} else {
			let token = await User.query().where('user_id', user.user_id).update({
				user_status: 'active',
			});
		}
	}

	if (user.user_status == 'deactive') {
		err = true;
		throw global.forbiddenError(
			'Your account has been deactivated, please contact admin for the same.'
		);
	}

	if (!user.is_user_verified) {
		if (data.userType != 'driver') {
			err = true;
			throw global.unverifiedError('Please verify your email to continue.');
		} else {
			let token = await User.query().where('user_id', user.user_id).update({
				is_user_verified: true,
			});

			user.is_first_login = true;
		}
		err = true;
	} else {
		user.is_first_login = false;
	}

	// generate authenticated data
	let auth_token = await user.getJWT();
	let updateUser = {
		token: auth_token,
		user_status: 'active',
		last_login_at: new Date().toISOString(),
	};

	if (data.device_id) {
		updateUser.device_id = data.device_id;
	}

	if (data.device_type) {
		updateUser.device_type = data.device_type;
	}

	if (data.device_token) {
		updateUser.device_token = data.device_token;
		console.log('token', data.device_token);
	}
	let token = await User.query()
		.where('user_id', user.user_id)
		.update(updateUser)
		.returning('*');

	res.setHeader('Content-Type', 'application/json');
	res.setHeader('Authorization', 'Bearer ' + auth_token);
	res.setHeader('Access-Control-Expose-Headers', 'Authorization');

	delete user.password;

	return global.okResponse(
		res,
		{
			...user.toJSON(),
		},
		'Login Successful'
	);
};

const Logout = async (req, res) => {
	if (req.headers.authorization) {
		//  console.log("req", req.headers.authorization)
		let token = req.headers.authorization.replace('Bearer ');
		await User.query().where('user_id', req.user.user_id).update({
			token: '',
			device_token: '',
		});
	}

	return global.okResponse(res, {}, 'Logout successfull!!');
};

/** forgotpassword API
 * Description: Used to fetch user for forgot password. This will generate a unique code (number for driver encrypted string for others) and will send it to the user on mobile / email respoectively
 * Params: data.email - stores the unique email id which was used while logging in
 * Params: data.userType - stores the type of user; possible values super_admin, vendor, driver
 * Return: Response in JSON Format along with the unique token in header
 **/
const RequestForForgotPassword = async (req, res) => {
	//console.log(req.headers.host)
	let data = req.body;

	/** Validate for the empty fields */

	if (!data.userType) {
		throw global.badRequestError('Invalid Request.');
	}

	if (data.userType == 'driver') {
		if (!data.mobile_number) {
			throw global.badRequestError(
				'Please enter your registered contact number.'
			);
		}

		let mobile = data.mobile_number.split('-');
		if (!validator.isMobilePhone(mobile[1])) {
			throw global.badRequestError('Please enter a valid mobile number.');
		}
	} else {
		if (!data.email) {
			throw global.badRequestError('Please enter your registered email id.');
		}

		if (!validator.isEmail(data.email)) {
			throw global.badRequestError('Please enter a valid email id');
		}
	}

	// execute query
	try {
		let user = await User.query()
			.where((builder) => {
				if (data.userType == 'driver') {
					builder.where('mobile_number', data.mobile_number);
				} else {
					builder.where('email_id', data.email);
				}
			})
			.where((builder) => {
				if (data.userType == 'vendor') {
					builder
						.where('user_type', 'vendor')
						.orWhere('user_type', 'sub_ordinate');
				} else {
					builder.where('user_type', data.userType);
				}
			})
			.first();

		let err;

		// if user not found show message based on user type
		if (!user) {
			err = true;
			if (data.userType == 'driver') {
				throw global.badRequestError('Contact Number is not registered.');
			} else {
				throw global.badRequestError('Email id is not registered.');
			}
		}

		// check user's status before performing further actions
		if (user.status == 'pending') {
			err = true;
			throw global.unverifiedError('Your account is not verified yet.');
		}

		if (user.status == 'deactive') {
			err = true;
			throw global.unverifiedError(
				'Your account has been deactivated, please contact admin for the same.'
			);
		}

		if (!user.is_user_verified) {
			err = true;
			throw global.unverifiedError('Please verify your account to continue.');
		}

		let auth_token;
		let link;

		// auth link will be generated for vendor/sub-ordinates & super admin
		// generate authenticated data
		if (data.userType == 'driver') {
			auth_token = Math.floor(1000 + Math.random() * 9000);

			let message =
				'Your verification code is ' +
				auth_token +
				(data.device_type == 'IOS' ? '.' : '.'); //cnd7k5RHPNG

			link = message;

			// sendSMS(data.mobile_number.replace("+", "").replace("-", ""), message);
			sendSms_twilio(
				data.mobile_number.replace('+', '').replace('-', ''),
				message
			);
		} else {
			auth_token = await user.getJWT();

			link =
				(data.userType == 'super_admin'
					? global.GLOBAL_ADMIN_URL
					: global.GLOBAL_CLIENT_URL) +
				'reset-password/' +
				auth_token;

			Email.sendEmailT(data.email, 'Reset Your Password', '', {
				title: 'Reset Password',
				name: user.first_name,
				message1: 'You have requested to reset your password',
				message2: 'Please click on the link below to process further.',
				buttonTitle: 'Reset Password',
				buttonLink: link,
			});
		}

		let token = await User.query()
			.where('user_id', user.user_id)
			.update({
				verification: auth_token,
			})
			.returning('*');
	} catch (error) {
		console.log(error);
		throw global.badRequestError(error.message);
	}

	/**here instead of sending it in response we will send it in sms or mail respectively.
	 * For Driver code is sent to him for others email varification link.
	 */
	return global.okResponse(res, {}, 'Check');
};

/** verifyUserLink API
 * Description: Used to verify user based on a unique code (either by link or by code).
 * Params: data.email - stores the unique email id which was used while logging in
 * Return: Response in JSON Format along with the unique token in header
 **/

const verifyUserLink = async (req, res) => {
	let data = req.body;

	let verification_code = data.verify;

	if (data.userType == 'driver' && (!data.verify || !data.mobile_number)) {
		throw global.badRequestError('Invalid Link.');
	}
	// if no code is passed
	if (verification_code == '') {
		throw global.badRequestError('Invalid link.');
	}
	// execute query
	let user = await User.query()
		.where('verification', verification_code)
		.where((builder) => {
			if (data.userType && data.userType == 'driver') {
				builder.where('mobile_number', data.mobile_number);
			}
		})
		.first();

	let err;

	// if no result found show the appropriate message
	if (!user) {
		err = true;
		if (data.userType == 'driver') {
			throw global.badRequestError('Your Otp is incorrect');
		}

		throw global.badRequestError(
			'Either invalid link or link is expired or already used'
		);
	}

	// check the user status to prevent him from updating any data
	if (user.user_status == 'pending') {
		err = true;
		throw global.unverifiedError('Your account is not verified yet.');
	}

	if (user.user_status == 'deactive') {
		err = true;
		throw global.unverifiedError(
			'Your account has been deactivated, please contact admin for the same.'
		);
	}

	if (!user.is_user_verified) {
		err = true;
		throw global.unverifiedError('Please verify your account to continue.');
	}

	// send the response
	return global.okResponse(res, {}, 'Valid Link');
};

/** VerifyAndUpdatePassword API
 * Description: Used to verify user based on a unique code (either by link or by code).
 * Params: data.email - stores the unique email id which was used while logging in
 * Params: data.password - stores the password used for the user to validate the user
 * Params: data.userType - stores the type of user; possible values super_admin, vendor, driver
 * Return: Response in JSON Format along with the unique token in header
 **/

const VerifyAndUpdatePassword = async (req, res) => {
	let data = req.body;

	// if no code is passed

	// if no password is added
	if (!data.password) {
		throw global.badRequestError('Enter a password');
	}

	if (data.userType == 'driver') {
		if (!data.mobile_number) {
			throw global.badRequestError('Invalid Link.');
		}
	} else {
	}

	if (!data.verify) {
		throw global.badRequestError('Invalid link.');
	}

	// execute query
	let user = {};
	if (data.is_verified_firebase && data.userType == 'driver') {
		user = await User.query()
			.where((builder) => {
				if (data.userType == 'driver') {
					builder.where('mobile_number', data.mobile_number);
				}
			})
			.first();
	} else {
		user = await User.query()
			.where('verification', data.verify)
			.where((builder) => {
				if (data.userType == 'driver') {
					builder.where('mobile_number', data.mobile_number);
				}
			})
			.where((builder) => {
				if (data.userType == 'vendor') {
					builder
						.where('user_type', 'vendor')
						.orWhere('user_type', 'sub_ordinate');
				} else {
					builder.where('user_type', data.userType);
				}
			})
			.first();
	}

	let err;

	// if no result found show the appropriate message
	if (!user) {
		err = true;
		if (data.userType == 'driver') {
			throw global.badRequestError('Code does not match');
		} else {
			throw global.badRequestError(
				'Either invalid link or link is expired or already used'
			);
		}
	}

	// check the user status to prevent him from updating any data
	if (user.user_status == 'pending') {
		err = true;
		throw global.unverifiedError('Your account is not verified yet.');
	}

	if (user.user_status == 'deactive') {
		err = true;
		throw global.unverifiedError(
			'Your account has been deactivated, please contact admin for the same.'
		);
	}

	if (!user.is_user_verified) {
		err = true;
		throw global.unverifiedError('Please verify your email to continue.');
	}

	// execute query to update password and reset the token
	await User.query().where('user_id', user.user_id).update({
		password: data.password,
		verification: '',
	});

	delete user.password;

	// send the response
	return global.okResponse(res, {}, 'Password updated successfully.');
};

/** This is called when business activates via email */
const activateBusinessUsers = async (req, res) => {
	let verification_code = req.params.code;
	let data = req.body;

	// if no code is passed
	if (verification_code == '') {
		throw global.badRequestError('Invalid link.');
	}

	// if no password is added
	if (!data.password && data.checkOnly == undefined) {
		throw global.badRequestError('Enter a password');
	}
	// execute query
	let user = await User.query()
		.where('verification', verification_code)
		.where((builder) => {
			builder.where('user_type', 'vendor').orWhere('user_type', 'sub_ordinate');
		})
		.where('user_status', 'pending')
		.where('is_user_verified', false)
		.first();

	if (!user) {
		throw global.badRequestError(
			'Either link has been expired or your email is already verified. Please check with your business.'
		);
	}
	console.log('userid', user.user_id);
	let message = '';
	if (data.checkOnly == undefined) {
		await User.query()
			.update({
				password: data.password,
				verification: '',
				is_user_verified: true,
				user_status: 'active',
			})
			.where('user_id', user.user_id);
		message = 'Password has been updated. Please login with your credentials.';
	} else {
		message = 'Activate your account by providing password below.';
	}

	// send the response
	return global.okResponse(res, {}, message);
};

/**
 * Updates device token for web user
 * @param {*} req
 * @param {*} res
 */
const updateDeviceToken = async (req, res) => {
	let data = req.body;

	if (!data.device_token) {
		throw global.badRequestError('Invalid Request');
	}
	let user = await User.query()
		.update({
			device_token: data.device_token,
			device_type: 'WEB',
		})
		.where('user_id', req.user.user_id);
	if (!user) {
		throw global.badRequestError('Something went wrong.');
	}
	return global.okResponse(res, {}, '');
};

const fetchCountryCode = async (req, res) => {
	let countryCode = await Country.query()
		.select('country_code')
		.where('status', 'active');

	let returnData = {
		countryCode,
	};
	// send the response
	return global.okResponse(
		res,
		{
			...returnData,
		},
		''
	);
};

const resendOTPOrLink = async (req, res) => {
	let data = req.body;

	let verificationMessage;
	let message;
	let sender;
	let name = '';
	if (data.user_type == 'driver') {
		name = data.name;
		let password = Math.floor(1000 + Math.random() * 9000);
		data.password = password.toString();

		verificationMessage =
			'Get login with your mobile number to . Your OTP is -' + password;
		sender = data.mobile_number;
		delete data.mobile_number;
		message = 'OTP sent successfully';
		delete data.name;
	} else {
		delete data.user_type;
		name = data.name;
		let verification = await jwt.sign(
			{
				email: data.email_id,
			},
			global.CONFIG.jwt_encryption
		);

		data.verification = verification;
		sender = data.email_id;
		delete data.email_id;
		delete data.name;

		verificationMessage = global.GLOBAL_CLIENT_URL + 'activate/' + verification;
		message = 'Link sent successfully';
		console.log(data.verification);
	}
	let userUpdate = await User.query().upsertGraph(data).returning('*');
	console.log(name);
	if (data.user_type) {
		// sendSMS(sender.replace('-', '').replace("+", ""), verificationMessage);
		sendSms_twilio(
			sender.replace('-', '').replace('+', ''),
			verificationMessage
		);
	} else {
		Email.sendEmailT(sender, 'Welcome to Delivr! Confirm Your Email', '', {
			title: 'Welcome to Delivr!',
			name: name,
			message1: "You're on your way! Let's confirm your email address.",
			message2:
				'By clicking on the following link, you are confirming your email address.',
			buttonTitle: 'Confirm Email Address',
			buttonLink: verificationMessage,
		});
	}

	return global.okResponse(
		res,
		{
			...userUpdate,
		},
		message
	);
};

//////////////////////////////////////////////////////////////////////
const is_driver_verified = async (req, res) => {
	let data = req.body;
	/** Validate for the empty fields */

	if (!data.mobile_number) {
		throw global.badRequestError(
			'Please enter your registered contact number.'
		);
	}

	let mobile = data.mobile_number.split('-');
	if (!validator.isMobilePhone(mobile[1])) {
		throw global.badRequestError('Please enter a valid mobile number.');
	}

	let err;

	/** Execute Query to fetch the user */
	let user;
	try {
		user = await User.query()
			.select('user_id', 'mobile_number', 'is_user_verified')
			.where((builder) => {
				builder.where('mobile_number', data.mobile_number);
			})
			.first();
	} catch (error) {
		throw global.badRequestError(error.message);
	}

	// if user not found
	if (!user) {
		err = true;
		throw global.badRequestError('Driver is not registered.');
	}

	// not an active user

	// if (user.user_status == 'deactive') {
	//     err = true;
	//     throw global.forbiddenError('Your account has been deactivated, please contact admin for the same.');
	// }

	// if (!user.is_user_verified) {

	//     if (data.userType != 'driver') {
	//         err = true;
	//         throw global.unverifiedError('Please verify your email to continue.');
	//     } else {

	//         let token = await User.query().where('user_id', user.user_id).update({
	//             is_user_verified: true
	//         });

	//         user.is_first_login = true;
	//     }
	//     err = true;

	// }
	// generate authenticated data

	return global.okResponse(
		res,
		{
			...user.toJSON(),
		},
		''
	);
};

module.exports = {
	Login,
	Logout,
	RequestForForgotPassword,
	verifyUserLink,
	VerifyAndUpdatePassword,
	activateBusinessUsers,
	updateDeviceToken,
	fetchCountryCode,
	resendOTPOrLink,
	is_driver_verified,
};
