'use strict';

const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const Users = require('../models/Users');

module.exports = function (passport) {

  var opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();

  opts.secretOrKey = CONFIG.jwt_encryption;
  opts.passReqToCallback = true;

  passport.use(new JwtStrategy(opts, async function (req, jwt_payload, done) {
    let token = req.headers.authorization.split(' ')[1];
    let auth_token;

    auth_token = await Users.query().where('token', token).first();
    if (auth_token) {
      req.token = token;

      return done(null, auth_token);
    } else {
      return done(unauthorizedError("You have been logged in with other device."), auth_token);
    }

  }));
};