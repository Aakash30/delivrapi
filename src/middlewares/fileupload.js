const multer = require('multer');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const folderModule = req.params.module;

        cb(null, 'public/uploads/' + folderModule + '/temp');
    },
    filename: (req, file, cb) => {

        var filetype = '';
        if (file.mimetype === 'image/gif') {
            filetype = 'gif';
        }
        if (file.mimetype === 'image/png') {
            filetype = 'png';
        }
        if (file.mimetype === 'image/jpeg') {
            filetype = 'jpg';
        }
        cb(null, Date.now().toString() + '_' + file.originalname);
    }
});
var upload = multer({
    storage: storage
});
module.exports = {
    upload
};