module.exports = {
    adminAccess: (req, res, next) => {
        if (!req.user) {
            throw unauthorizedError("You have been logged in with other device.");
        }
        if (req.user.user_type === 'super_admin') {
            return next();
        } else {
            throw unauthorizedError("You are not authorized to perform this action.");
        }
    },

    vendorAccess: (req, res, next) => {
        if (!req.user) {
            throw unauthorizedError("You have been logged in with other device.");
        }
        if (req.user.user_type === 'vendor' || req.user.user_type === 'sub_ordinate') {
            return next();
        } else {
            throw unauthorizedError("You are not authorized to perform this action32s.");
        }
    },

    driverAccess: (req, res, next) => {
        if (!req.user) {
            throw unauthorizedError("You have been logged in with other device.");
        }
        if (req.user.user_type === 'driver' && req.user.user_status == 'active') {
            return next();
        } else {
            throw unauthorizedError("You are not authorized to perform this action.");
        }
    },
    checkRole: (roleIds) => {
        return function (req, res, next) {
            if (!req.user) {
                throw unauthorizedError("You are not authorized to perform this action.");
            }
            if (JSON.parse(req.user.user_role).indexOf(roleIds) !== -1) {
                return next();
            } else {
                throw unauthorizedError("You are not authorized to perform this action.");
            }
        };
    }
};