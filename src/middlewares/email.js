const sgMail = require("@sendgrid/mail");
const sendgrid = require("../config/index").sendgrid;
const pug = require("pug");
sgMail.setApiKey(sendgrid);

module.exports.sendEmailT = async (to, subject, template, globals) => {
  let templatePath = "";

  switch (template) {
    case "sos":
      templatePath = "public/templates/sos.pug";
      break;
    default:
      templatePath = "public/templates/email.pug";
      break;
  }
  /**  */
  const msg = {
    to: to,
    from: "Delivr <support@delivr.biz>",
    subject: subject,
    html: pug.renderFile(templatePath, globals)
  };

  console.log(new Date().toLocaleTimeString());
  sgMail
    .send(msg)
    .then(() => {
      //Celebrate
      console.log("sent");
    })
    .catch(error => {
      //Log friendly error
      console.error(error.toString());

      //Extract error msg
      const {
        message,
        code,
        response
      } = error;

      //Extract response msg
      const {
        headers,
        body
      } = response;
    });
};