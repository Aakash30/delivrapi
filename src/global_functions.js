const ValidationError = require('objection').ValidationError;

const fs = require('fs');
const path = require('path');

/** Move files from one place to other 
 * 
 * @params {stores old path of the file} oldpath
 * @params {stores new path or where file has to be moved} newpath
 */
moveFile = function (oldpath, newpath, callback) {
  let err;

  if (!fs.existsSync(newpath)) {

    fs.mkdir(newpath, {}, (err) => {

      replaceFileLocation(oldpath, newpath, callback);
    });
  } else {

    replaceFileLocation(oldpath, newpath, callback);
  }

};

replaceFileLocation = function (oldpath, newpath, callback) {

  var f = path.basename(oldpath);
  var destination = path.resolve(newpath, f);
  fs.rename(oldpath, destination, (err) => {
    if (err) callback(err);
    else {
      callback(undefined, "success");
    }
  });
};

// The error returned by this function is handled in the error handler middleware in app.js.
createStatusCodeError = function (statusCode, message) {
  return Object.assign(new Error(), {
    statusCode,
    message,
  });
};

createStatusCodeError = function (statusCode, message, body) {
  return Object.assign(new Error(), {
    statusCode,
    message,
    body: body
  });
};

badRequestError = function (msg) {
  return createStatusCodeError(422, msg);
};

unverifiedError = function (message) {
  return createStatusCodeError(412, message);
};


unverifiedError = function (message, body) {
  return createStatusCodeError(412, message, body);
};

forbiddenError = function (msg) {
  return createStatusCodeError(403, msg);
};

unauthorizedError = function (msg) {
  return createStatusCodeError(401, msg);
};

notFoundError = function (msg) {
  return createStatusCodeError(404, msg);
};


errorResponse = function (res, data, message, code) {
  res.statusCode = code;
  return res.json({
    success: false,
    code,
    data,
    message
  });
};

// Response handlers
successResponse = function (res, code, data, message) {
  return res.status(code || 200).json({
    success: true,
    statusCode: code,
    data,
    message
  });
};

okResponse = function (res, data, message) {
  res.statusCode = 200;
  if (!message) {
    message = "";
  }
  return successResponse(res, 200, data, message);
};

createdResponse = function (res, data, message) {
  return successResponse(res, 201, data, message);
};

noContentResponse = function (res, message) {
  return successResponse(res, 204, {}, message);
};

// Utility functions
slugify = function (Text) {
  Text = Text || '';
  return Text
    .toLowerCase()
    .replace(/[^\w ]+/g, '')
    .replace(/ +/g, '-');
}


basePath = function () {
  return "http://localhost:3000/";
}

randomStrings = function (lengthOfString) {

  // String of all alphanumeric character 
  const strResult = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  let result = "";
  for (var i = lengthOfString; i > 0; --i) {
    let rnum = Math.floor(Math.random() * strResult.length);
    result += strResult.substring(rnum, rnum + 1);
  }
  return result;
}