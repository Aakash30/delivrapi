CONFIG = {};
//
CONFIG.jwt_encryption = 'xIMEE1vdvlvTjac1tGyiJHZusIFBtl';
//GLOBAL_HOST = 'http://localhost:3000/';

//GLOBAL_CLIENT_URL = 'http://localhost:4200/business/'; // will add client side url
//GLOBAL_ADMIN_URL = 'http://localhost:4200/admin/';

GLOBAL_HOST = 'http://13.234.120.28/'; //Zubear
GLOBAL_CLIENT_URL = 'http://13.234.120.28/business/'; //Zubear
GLOBAL_ADMIN_URL = 'http://13.234.120.28/admin/'; //Zubear

//GLOBAL_CLIENT_URL = 'http://35.154.205.28/business';
// GLOBAL_ADMIN_URL = 'http://35.154.205.28/admin/';

// GLOBAL_HOST = 'https://delivr.biz/';//Zubear

// GLOBAL_CLIENT_URL = 'https://delivr.biz/business/'; //Zubear
// GLOBAL_ADMIN_URL = 'https://delivr.biz/admin/'; //Zubear

GLOBAL_URL = GLOBAL_HOST + 'api/v1';
PER_PAGE = 10;

STARTING_MECHANISM = ['Self Start', 'Kick Start', 'Self-start and Kick both'];
TYRE_TYPE = ['Tyre-1', 'Tyre-2', 'Tyre-3'];
WHEEL_TYPE = ['Rim Wheels', 'Tyre-2', 'Tyre-3'];
STROKE = ['Two Stroke', 'Four Stroke', 'Five Stroke'];
BREAK = ['Front Break', 'Rear Break'];
CHANNEL = ['Manual'];
//Payment method
PAYMENTMEHTODS = ['Cash', 'Online'];
USER_TITLE = [
	{
		key: 'manager',
		label: 'Manager',
	},
	{
		key: 'operational_manager',
		label: 'Operational Manager',
	},
	{
		key: 'account_manager',
		label: 'Account Manager',
	},
];

/**
 
NEW_REPAIR_REQUEST
ASSIGN_BIKE
REQUEST_FOR_MODULE
REPAIR_RESPONSE
ORDER_ASSIGNED
ORDER_ACEPTED
ORDER_REJECTED
ORDER_DELIVERED
SOS
PAYMENT_DUE_REMINDER

 */
NOTIFICATION_PAYLOAD = {
	NEW_VEHICLE_REQUEST: {
		title: 'Assign Vehicle',
		body: '[Business] has requested for [number] vehicle(s).',
		icon: GLOBAL_HOST + 'images/icon-bell-1.svg',
		urlToRedirect: GLOBAL_ADMIN_URL + 'notification',
	},
	VEHICLE_REQUEST_RESPONSE: {
		title: 'Vehicles Assigned',
		body: 'Your request raised on [requested_on] has been [request_status].',
		icon: GLOBAL_HOST + 'images/icon-bell-1.svg',
		urlToRedirect: GLOBAL_CLIENT_URL + 'notification',
	},
	ORDER_ASSIGNED: {
		title: 'New Order',
		type: 'ORDER',
		body: 'A new order [order_id] has been assigned to you.',
	},
	ORDER_REJECTED: {
		title: 'Order Rejected',
		type: 'ORDER',
		body: 'Order [order_id] has been rejected by [driver_name].',
		icon: GLOBAL_HOST + 'images/icon-bell-2.svg',
		urlToRedirect: GLOBAL_CLIENT_URL + 'notification',
	},
	ORDER_ACCEPTED: {
		title: 'Order Accepted',
		type: 'ORDER',
		body: 'Order [order_id] has been accepted by [driver_name].',
		icon: GLOBAL_HOST + 'images/icon-bell-2.svg',
		urlToRedirect: GLOBAL_CLIENT_URL + 'notification',
	},
	ORDER_DELIVERED: {
		title: 'Order Delivered',
		type: 'ORDER',
		body: 'Order [order_id] has been delivered by [driver_name].',
		icon: GLOBAL_HOST + 'images/icon-bell-2.svg',
		urlToRedirect: GLOBAL_CLIENT_URL + 'notification',
	},
	REQUEST_FOR_MODULE: {
		title: 'New Inquiry',
		body: 'You have received a new request for module.',
		icon: GLOBAL_HOST + 'images/icon-bell-1.svg',
		urlToRedirect: GLOBAL_ADMIN_URL + 'notification',
	},
	NEW_REPAIR_REQUEST: {
		title: 'New Repair Request',
		body: '[driver_name] has raised a repairing request - [REQUEST_ID].',
		icon: GLOBAL_HOST + 'images/icon-bell-1.svg',
		urlToRedirect: GLOBAL_ADMIN_URL + 'notification',
	},
	NEW_REPAIR_REQUEST_BUSINESS: {
		title: 'New Repair Request',
		body: '[driver_name] has raised a repairing request - [REQUEST_ID].',
		icon: GLOBAL_HOST + 'images/icon-bell-1.svg',
		urlToRedirect: GLOBAL_CLIENT_URL + 'notification',
	},
	REPAIR_REQUEST_RESPONSE_BUSINESS: {
		title: 'Driver Repair Request Response',
		body: 'Repairing request - [REQUEST_ID] is [status].',
		icon: GLOBAL_HOST + 'images/icon-bell-1.svg',
		urlToRedirect: GLOBAL_CLIENT_URL + 'notification',
	},
	REPAIR_REQUEST_RESPONSE: {
		title: 'Repair Request Status',
		type: 'REPAIR',
		body: 'Your Repairing request - [REQUEST_ID] is [status].',
	},
	BATCH_EXPIRED: {
		title: 'Batch Expiration',
		type: 'Batch',
		body: 'Your Vehicle Assignment - [batchId] [DATETIME].',
		icon: GLOBAL_HOST + 'images/icon-bell-3.svg',
		urlToRedirect: GLOBAL_CLIENT_URL + 'notification',
	},
	BATCH_EXPIRED_RESPONSE: {
		title: 'Batch Expiration',
		type: 'Batch',
		body: 'Vehicle batch - [batchId] for [business] [DATETIME].',
		icon: GLOBAL_HOST + 'images/icon-bell-3.svg',
		urlToRedirect: GLOBAL_CLIENT_URL + 'notification',
	},
};
