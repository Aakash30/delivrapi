const fs = require("fs");
const path = require("path");
var admin = require("firebase-admin");
const CONFIG = require('./config');

const apn = require("apn");

var serviceAccount = require("./delivr-d6cdc-firebase-adminsdk-edrv9-4438ffeff1.json");
var options = {
    token: {
        key: fs.readFileSync(
            path.resolve("./services/AuthKey_T5Y89399M5.p8")
        ),
        keyId: "T5Y89399M5",
        teamId: "H6P5YXD7HN"
    },
    production: false
};
var apnProvider = new apn.Provider(options);

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://delivr-d6cdc.firebaseio.com"
});

const sendNotificationToAndroid = (registrationToken, dataToSend) => {
    // registrationToken = "ejG8a6LZ_NL_Tc2jzlIFk5:APA91bGTxEoT97P84LcLOuQsnCUKhPN1uL08y-uV4IL1GFC4Ta-2wAaIWrYd4Vc7lLMA__4wkYysHmK_PSIhXtGssaOPSBjbschtmLs5ZA_FLFHwN9TgHMshUauIAMmOW7wsSU5ZhAvG";
    var payload = {
        data: dataToSend
    };

    var options = {
        priority: "high",
        timeToLive: 60 * 60 * 24
    };
    admin.messaging().sendToDevice(registrationToken, payload, options).then(function (response) {
            //  console.log(response.results[0])
        })
        .catch(function (error) {
            console.log(error)
            console.log("Error sending message:", error.message);
        });
};
//let registrationToken = "eKVWNNfrgx0:APA91bGajxet5xfOL6CSvucYpyUAMKAZMuuuWeWvoJC3CQshGXqK5OunEl92hpQKIwn-bOteKksIGKQtkjB0-45xc2sdP3pdD3my7eXjlkoCF0VEEStNgDX4MPtR-KkW0Ws6a41I_i7n";

const sendNotificationToUser = (registrationToken, dataToSend) => {

    // console.log(dataToSend)
    // console.log(registrationToken)
    var payload = {
        data: dataToSend
    };
    // let registrationToken = "ejG8a6LZ_NL_Tc2jzlIFk5:APA91bGTxEoT97P84LcLOuQsnCUKhPN1uL08y-uV4IL1GFC4Ta-2wAaIWrYd4Vc7lLMA__4wkYysHmK_PSIhXtGssaOPSBjbschtmLs5ZA_FLFHwN9TgHMshUauIAMmOW7wsSU5ZhAvG";
    var options = {
        priority: "high",
        timeToLive: 60 * 60 * 24
    };
    admin.messaging().sendToDevice(registrationToken, payload, options).then(function (response) {
            //console.log(response)
        })
        .catch(function (error) {
            // console.log("Error sending message:", error);
        });
};

const sendNotificationToIOS = (payload, registrationTokens) => {
    var note = new apn.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = 1;
    note.sound = "ping.aiff";
    note.alert = payload.title;
    note.payload = payload;
    note.topic = "com.horsepowerllc.app";

    apnProvider
        .send(note, registrationTokens)
        .then(result => {
            console.log(JSON.stringify(result));
            console.log("notification sent to ios");
        })
        .catch(err => {
            console.log("error sending notification");
            console.log(err);
        });
};

module.exports = {
    sendNotificationToAndroid,
    sendNotificationToUser,
    sendNotificationToIOS
};