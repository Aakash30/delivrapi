exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("vehicle_images", table => {
            table.increments("image_entry_id").primary();
            table.integer("vehicle_id");
            table.text("vehicle_image").collate('utf8_general_ci');
        });
};

exports.down = function (knex, Promise) {

};