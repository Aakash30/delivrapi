exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("coupons", table => {
            table.increments("coupon_id").primary();
            table.string("coupon_code").collate("utf8_general_ci");
            table.text("description").collate("utf8_general_ci");
            table.decimal("discount_in_percent");
            table.timestamp("expired_on", false);
            table.integer("business_id");
            table.integer("business_branch_id");
            table.integer("created_by");
            table.timestamp("created_at", false);
        })
};

exports.down = function (knex, Promise) {

};