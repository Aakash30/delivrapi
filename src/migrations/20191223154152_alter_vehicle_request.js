exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("vehicle_request_record", builder => {
            builder.string("request_number").collate("utf8_general_ci").unique();
        });
};

exports.down = function (knex, Promise) {

};