exports.up = function (knex, Promise) {
    return knex.schema
        .createTable('country_list', table => { // table: country_list
            table.increments('country_id').primary();
            table.string('country', 150).collate('utf8_general_ci').unique();
            table.string('currency', 20).collate('utf8_general_ci');
            table.enum('status', ['active', 'inActive']).defaultTo('active').collate('utf8_general_ci');
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, true);
        })

        .createTable('city_list', table => { // table: city_list
            table.increments('city_id').primary();
            table.string('city', 150).collate('utf8_general_ci');
            table.integer('country_id')
                .unsigned()
                .references('country_id')
                .inTable('country_list')
                .onDelete('CASCADE');
            table.enum('status', ['active', 'inActive']).defaultTo('active').collate('utf8_general_ci');
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, true);
            table.unique(['city', 'country_id']);
        })

        .createTable('business_type_list', table => { // table: vendor_type_list
            table.increments('id').primary();
            table.string('business_type', 150).collate('utf8_general_ci');
            table.enum('status', ['active', 'block']).defaultTo('active').collate('utf8_general_ci');
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, false);
        })
        .createTable('business', table => { // table: business
            table.increments('business_id').primary();
            table.string('business_name', 150).collate('utf8_general_ci');
            table.integer('business_type')
                .unsigned()
                .references('id')
                .inTable('business_type_list')
                .onDelete('CASCADE')
                .comment('This defines users type');
            table.string('email_id', 60).unique().collate('utf8_general_ci');
            table.string('mobile_number').collate('utf8_general_ci');
            table.string('country').collate('utf8_general_ci');
            table.string('city').collate('utf8_general_ci');
            table.text('address_line_1').collate('utf8_general_ci');
            table.text('address_line_2').collate('utf8_general_ci');
            table.string('latitude').collate('utf8_general_ci');
            table.string('longitude').collate('utf8_general_ci');
            table.enum('status', ['active', 'deactive']).collate('utf8_general_ci');
            table.string('postal_Code').collate('utf8_general_ci');
            table.text('agreement').collate('utf8_general_ci');
            table.text('license').collate('utf8_general_ci');
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, false);
        })
        .createTable('user_login', table => { // table: user_login
            table.increments('user_id').primary();
            table.string('first_name', 60).collate('utf8_general_ci');
            table.string('last_name', 60).collate('utf8_general_ci');
            table.string('email_id', 60).unique().collate('utf8_general_ci');
            table.string('mobile_number').collate('utf8_general_ci');
            table.string('password').collate('utf8_general_ci');
            table.string('alternate_number').collate('utf8_general_ci');
            table.integer('country');
            table.integer('city');
            table.text('address_line_1').collate('utf8_general_ci');
            table.text('address_line_2').collate('utf8_general_ci');
            table.string('postal_Code').collate('utf8_general_ci');
            table.text('profile_image').collate('utf8_general_ci');
            table.text('token').collate('utf8_general_ci');
            table.text('verification').collate('utf8_general_ci').comment('it will store verification otp for drivers and link for others.');
            table.integer('business_id')
                .unsigned()
                .references('business_id')
                .inTable('business')
                .onDelete('CASCADE')
                .comment('This defines user business');
            table.enum('user_type', ['super_admin', 'vendor', 'sub_ordinate', 'driver']).collate('utf8_general_ci').comment('This defines users type');;
            table.text('user_role').collate('utf8_general_ci');
            table.enum('user_status', ['active', 'pending', 'deactive']).defaultTo('pending');
            table.text('emirates_id').collate('utf8_general_ci');
            table.string('ip');
            table.boolean('is_email_verified').defaultTo(false);
            table.boolean('is_mobile_verified').defaultTo(false);
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, false);
            table.timestamp('last_login_at', true);

        })
        .createTable('user_type_roles', table => { // table: user_type_roles
            table.increments('id').primary();
            table.string('user_type', 30).collate('utf8_general_ci');
            table.string('module', 50).collate('utf8_general_ci');
            table.text('roles').collate('utf8_general_ci');
            table.enum('role_type', ['default', 'custom']).comment('It will decide if the role is assigned by default or not');
        })
        .createTable('user_roles', table => { // table: user_roles
            table.increments('id').primary();
            table.string('roles').collate('utf8_general_ci');
            table.integer('parent_id');
        })

        /*.createTable('action_logs', table => { // table: action_logs
            table.increments('id', 11).primary();
            table.text('action_performed_path').collate('utf_general_ci').comment('Defines the module path or api call which was performed');
            table.text('affected_table_column').collate('utf_general_ci').comment('Defines the affected table and column names, all in case of all column updates');
            table.text('unique_key_value_set').collate('utf_general_ci').comment('Defines the value and column which was used to perform the action like table id to perform delete action');
            table.enum('action_type', ['Create', 'Update', 'Delete']).comment('Defines the action which was performed like create, delete or update');
            table.enum('action_status', ['Success', 'Error']).comment('Defines the response of action whether it was success or error');
            table.string('response_message').comment('shows the response message');
            table.integer('created_by').comment('id of the user who had performed the action');
            table.timestamps(false, true);
        })*/

        .createTable('vehicle_brand', table => { // table: vehicle brand
            table.increments('brand_id', 11).primary();
            table.string('brand_name', 200).unique();
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, false);
        })

        .createTable('vehicle_model', table => { // table: vehicle model
            table.increments('model_id', 11).primary();
            table.string('model_name', 200).unique();
            table.integer('brand_id')
                .unsigned()
                .references('brand_id')
                .inTable('vehicle_brand')
                .onDelete('CASCADE');
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, false);
        })

        .createTable('color_table', table => { // table: color table
            table.increments('color_id', 11).primary();
            table.string('color', 200).unique();
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, true);
        })
        .createTable('vehicle', table => {
            table.increments('vehicle_id').primary();
            table.integer('country').collate('utf8_general_ci');
            table.integer('city').collate('utf8_general_ci');
            table.string('vehicle_number').unique().collate('utf8_general_ci');
            table.integer('brand')
                .unsigned()
                .references('brand_id')
                .inTable('vehicle_brand')
                .onDelete('CASCADE');
            table.integer('model')
                .unsigned()
                .references('model_id')
                .inTable('vehicle_model')
                .onDelete('CASCADE');
            table.string('color', 30).collate('utf8_general_ci');
            table.string('engine_display').collate('utf8_general_ci');
            table.string('engine_number').collate('utf8_general_ci');
            table.string('starting_mechanism', 50).collate('utf8_general_ci');
            table.string('tyre_type', 50).collate('utf8_general_ci');
            table.string('wheel_type', 50).collate('utf8_general_ci');
            table.string('stroke', 50).collate('utf8_general_ci');
            table.string('breakfront', 50).collate('utf8_general_ci');
            table.decimal('price');
            table.string('currency', 20).collate('utf8_general_ci');
            table.decimal('mileage');
            table.text('registration_card').collate('utf8_general_ci');
            table.text('insurance').collate('utf8_general_ci');
            table.text('vehicle_images').collate('utf8_general_ci');
            table.enum('is_available', [true, false]).defaultTo(true);
            table.enum('is_on_lease', [true, false]).defaultTo(false);
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, false);
        });
};

exports.down = function (knex, Promise) {

};