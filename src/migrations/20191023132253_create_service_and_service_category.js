exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("service_category", table => {
            table.increments("service_category_id").primary();
            table.string("service_category_name").collate("utf8_general_ci");
            table.integer("created_by");
            table.timestamp("created_at", false);
            table.integer("updated_by");
            table.timestamp("updated_at", false);
        })
        .createTable("service_list", table => {
            table.increments("list_id").primary();
            table.string("service_name").collate("utf8_general_ci");
            table.integer("created_by");
            table.timestamp("created_at", false);
            table.integer("updated_by");
            table.timestamp("updated_at", false);
        })
        .createTable("service_pricing", table => {
            table.increments("id").primary();
            table.integer("country");
            table.integer("brand");
            table.integer("model");
            table.decimal("price");
            table.string("engine_capacity");
        })
};

exports.down = function (knex, Promise) {

};