exports.up = function (knex, Promise) {
      return knex.schema
            .createTable('order', table => { // table: customer order
                  table.increments('order_id').primary();
                  table.string('invoice_id').collate('utf8_general_ci');
                  table.timestamp('order_date', true);
                  table.enum('order_status', ['new', 'delivered', 'accepted', 'rejected', 'assigned']).defaultTo('new').collate('utf8_general_ci');
                  table.string('first_name').collate('utf8_general_ci');
                  table.string('last_name').collate('utf8_general_ci');
                  table.string('email', 60).collate('utf8_general_ci');
                  table.string('mobile_number').collate('utf8_general_ci');
                  table.string('channel').collate('utf8_general_ci');
                  table.integer('country');
                  table.integer('city');
                  table.text('address_1').collate('utf8_general_ci');
                  table.text('address_2').collate('utf8_general_ci');
                  table.text('latitude').collate('utf8_general_ci');
                  table.text('longitude').collate('utf8_general_ci');
                  table.string('postal_code').collate('utf8_general_ci');
                  table.string('payment_method').collate('utf8_general_ci');
                  table.double('subtotal');
                  table.integer('driver_id');
                  table.integer('business_id');
                  table.integer('created_by');
                  table.integer('update_by');
                  table.timestamps(false, false);

            })

            .createTable('item_list', table => { // table: itme list
                  table.increments('item_id').primary();
                  table.integer('order_id')
                        .unsigned()
                        .references('order_id')
                        .inTable('order')
                        .onDelete('CASCADE')
                        .comment('This defines user business');
                  table.string('item_name').collate('utf8_general_ci');
                  table.string('quantity').collate('utf8_general_ci');
                  table.integer('business_id');
                  table.double('price');
                  table.timestamps(false, false);

            })
            .createTable('order_feedback', table => {
                  table.increments('feedback_id').primary();
                  table.integer('order_id');
                  table.integer('ratings');
                  table.text('customer_review').collate('utf8_general_ci');
                  table.text('customer_signature').collate('utf8_general_ci');
                  table.timestamps(false, false);
            });

};

exports
      .down = function (knex, Promise) {

      };