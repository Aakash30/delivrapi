exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("service_list", table => {
            table.enum("type", ['paid', 'free']).defaultTo('paid');
        })
        .alterTable("service_pricing", table => {
            table.integer("service_id");
            table.integer("created_by");
            table.timestamp("created_at", false);
            table.integer("updated_by");
            table.timestamp("updated_at", false);
        })
};

exports.down = function (knex, Promise) {

};