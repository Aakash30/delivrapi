
exports.up = function(knex, Promise) {
    return knex.schema
    .createTable('notifications', table => {
        table.increments('notification_id').primary();
        table.string('notification_type').collate('utf8_general_ci');
        table.integer("unique_id").unsigned();
        table.string("device_type");
        table.integer('sender_id').unsigned().references('user_id').inTable('user_login').onDelete('CASCADE');
        table.integer('receiver_id').unsigned().references('user_id').inTable('user_login').onDelete('CASCADE');
        table.specificType('receivers_opt', 'INT[]');
        table.string('title');
        table.string('body');
        table.boolean('isRead').defaultTo(false);
        table.boolean('isRelevant').defaultTo(true);
        table.timestamp("time")
        table.timestamps(false, true);
    })
};

exports.down = function(knex, Promise) {
  
};
