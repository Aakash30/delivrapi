exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("business_accounts", table => {
            table.increments("account_id").primary();
            table.integer("driver_id");
            table.integer("business_id");
            table.integer("business_branch");
            table.double("amount");
            table.enum("account_type", ['credit', 'debit']);
            table.enum("debit_type", ['', 'fuel', 'fine', 'salik']).defaultTo('');
            table.timestamp("created_at", true);
        });
};

exports.down = function (knex, Promise) {

};