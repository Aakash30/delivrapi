exports.up = function(knex, Promise) {
    return knex.schema.alterTable("payments", t => {
        t.integer('driver_id')
        .unsigned()
        .references('user_id')
        .inTable('user_login')
        .onDelete('CASCADE');
    });
  };
  
  exports.down = function(knex, Promise) {};
  