exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("user_type_roles", table => {
            table.string("role_label").collate('utf8_general_ci');
        });
};

exports.down = function (knex, Promise) {
    return knex.schema
        .alterTable("user_type_roles", table => {
            table.dropColumn("role_label");
        });
};