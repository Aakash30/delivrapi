exports.up = function (knex, Promise) {
	return knex.schema.alterTable('business', (table) => {
		table
			.specificType('bank_statement', 'text ARRAY')
			.collate('utf8_general_ci');
	});
};

exports.down = function (knex, Promise) {};
