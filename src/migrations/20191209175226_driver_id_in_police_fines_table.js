exports.up = function(knex, Promise) {
    return knex.schema.alterTable("police_fines", t => {
        t.integer('driver_id')
        .unsigned()
        .references('user_id')
        .inTable('user_login')
        .onDelete('CASCADE');
    }).alterTable("payments", t=>{
        t.dropColumn('driver_id')
    });
  };
  
  exports.down = function(knex, Promise) {};
  