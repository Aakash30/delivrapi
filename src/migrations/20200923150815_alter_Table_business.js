exports.up = function (knex, Promise) {
	return knex.schema.alterTable('business', (table) => {
		table.dropColumn('credit_application_form');
		table.dropColumn('license');
		table.dropColumn('agreement');
		table.dropColumn('other_document');
	});
};

exports.down = function (knex, Promise) {};
