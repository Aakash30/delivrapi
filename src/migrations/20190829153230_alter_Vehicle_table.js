exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("vehicle", table => {
            table.renameColumn("engine_display", "engine_capacity")
        });
};

exports.down = function (knex, Promise) {
    return knex.schema
        .alterTable("vehicle", table => {
            table.renameColumn("engine_capacity", "engine_display")
        });
};