
exports.up = function(knex, Promise) {
    knex.schema.alterTable('payments', function(t) {
        t.decimal('amount_paid').defaultTo(0).alter();
      });
};

exports.down = function(knex, Promise) {
  
};
