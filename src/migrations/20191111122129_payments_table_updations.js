exports.up = function(knex, Promise) {
  knex.schema
    .alterTable("batch", function(t) {
      t.integer("installment_count").defaultTo(0);
      t.integer("installment_type").defaultTo(1);
      t.integer("most_recent_installment").defaultTo(0);
    })
    .alterTable("payments", function(t) {
      t.string("payment_id", 120).unique();
      t.boolean("is_paid").defaultTo(false);
      t.datetime("payment_date");
      t.string("remarks");
    });
};

exports.down = function(knex, Promise) {};
