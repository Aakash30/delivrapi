exports.up = function (knex, Promise) {
    return knex.schema.raw(`ALTER TABLE "order" DROP CONSTRAINT "order_order_status_check", ADD CONSTRAINT "order_order_status_check" CHECK(order_status IN ('new', 'delivered', 'accepted', 'rejected', 'assigned', 'ongoing'))`)
};

exports.down = function (knex, Promise) {

};