exports.up = function (knex, Promise) {
    return knex.schema
        .createTable('batch', table => { // manage batches for assigning drivers and payments
            table.increments('id').primary();
            table.string('slug', 120).unique();
            table.integer('business_id').unsigned().references('business_id').inTable('business').onDelete('CASCADE');
            table.timestamp("assigned_to_business_from", false);
            table.timestamp("assigned_to_business_to", false);
            table.timestamps(false, true);
        })
};

exports.down = function (knex, Promise) {

};