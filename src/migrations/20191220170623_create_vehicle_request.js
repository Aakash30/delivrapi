exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("vehicle_request_record", builder => {
            builder.increments("request_id").primary();
            builder.integer("business_id");
            builder.integer("requested_vehicle_count");
            builder.timestamp("requested_on", false);
            builder.enum("status", ['pending', 'ongoing', 'finished', 'rejected']).default('pending');
            builder.timestamp("last_updated_on", false);
        })
};

exports.down = function (knex, Promise) {

};