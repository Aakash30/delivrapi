exports.up = function (knex, Promise) {
	return knex.schema.alterTable('vehicle_request_record', (table) => {
		table.integer('alloted_vehicle_count').default(0);
		table
			.enum('status', ['pending', 'ongoing', 'finished', 'rejected', 'custom'])
			.default('pending');
	});
};

exports.down = function (knex, Promise) {};
