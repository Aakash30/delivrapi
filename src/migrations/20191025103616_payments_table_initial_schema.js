exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("payments", table => {
            table.increments("id").primary();
            table.integer('batch_id').unsigned().references('id').inTable('batch').onDelete('CASCADE');
            table.decimal('amount_paid');
            table.integer("created_by").comment('Whether this payment was initiated by the admin or business');
            table.timestamps(false, true);
        })
};

exports.down = function (knex, Promise) {

};