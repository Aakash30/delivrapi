exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("repair_request", table => {
            table.increments("id").primary();
            table.string("request_number").collate("utf8_general_ci");
            table.integer("driver_id");
            table.integer("business_id");
            table.integer("vehicle_id");
            table.timestamp("requested_on", false);
            table.enum("status", ['new', 'ongoing', 'completed', 'rejected']).defaultTo(
                'new'
            ).collate("utf8_general_ci");
            table.timestamp("last_updated_on", false);
        })
        .createTable("repair_request_service", table => {
            table.increments("id").primary();
            table.integer("request_id");
            table.integer("service_category_id");
            table.integer("service_id");
        })
};

exports.down = function (knex, Promise) {

};