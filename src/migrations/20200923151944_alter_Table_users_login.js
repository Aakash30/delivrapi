exports.up = function (knex, Promise) {
	return knex.schema.alterTable('user_login', (table) => {
		table.dropColumn('emirates_id');
		table.dropColumn('passport_document');
	});
};

exports.down = function (knex, Promise) {};
