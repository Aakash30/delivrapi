exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("business_partner", table => {
            table.increments("partner_id").primary();
            table.string("partner").collate("utf8_general_ci");
            table.integer("business_id")
                .unsigned()
                .references('business_id')
                .inTable('business')
                .onDelete('CASCADE');
            table.timestamp("created_at", false);
        })
        .alterTable("business_address_area", table => {
            table.integer("partner_id");
        })
};

exports.down = function (knex, Promise) {

};