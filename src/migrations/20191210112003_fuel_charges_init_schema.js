
exports.up = function(knex, Promise) {
    return knex.schema
    .createTable('fuel_updates', table => { 
        table.increments('id').primary();
        table.integer('driver_id').unsigned().references('user_id').inTable('user_login').onDelete('CASCADE');
        table.decimal('distance');
        table.decimal('cost');
        table.text('image');
        
        table.timestamps(false, true);
    })
};

exports.down = function(knex, Promise) {
  
};
