exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("fuel_updates", table => {
            table.boolean("is_approved").defaultTo(false);
        }).alterTable("police_fines", table => {
            table.boolean("is_approved").defaultTo(false);
            table.decimal("cost");
        });
};

exports.down = function (knex, Promise) {

};