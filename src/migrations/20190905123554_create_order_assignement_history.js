exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("rejected_order_history", table => {
            table.increments("history_id").primary();
            table.integer("order_id");
            table.string('invoice_id').collate('utf8_general_ci');
            table.integer("driver_id");
            table.integer("business_id");
            table.timestamp("rejected_on", true);
        })
};

exports.down = function (knex, Promise) {

};