exports.up = function(knex, Promise) {
  return knex.schema
    .alterTable("batch", t => {
        t.integer("most_recent_installment").defaultTo(0);
    })
    .alterTable("payments", t => {
      t.string("payment_id").unique();
      t.boolean("is_paid").defaultTo(false);
      t.datetime("payment_date");
    });
};

exports.down = function(knex, Promise) {};
