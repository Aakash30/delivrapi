exports.up = function (knex, Promise) {
	return knex.schema.alterTable('user_login', (table) => {
		table.specificType('emirates_id', 'text ARRAY').collate('utf8_general_ci');
		table
			.specificType('passport_document', 'text ARRAY')
			.collate('utf8_general_ci');
	});
};

exports.down = function (knex, Promise) {};
