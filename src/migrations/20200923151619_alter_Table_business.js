exports.up = function (knex, Promise) {
	return knex.schema.alterTable('business', (table) => {
		table
			.specificType('credit_application_form', 'text ARRAY')
			.collate('utf8_general_ci');
		table.specificType('license', 'text ARRAY').collate('utf8_general_ci');
		table.specificType('agreement', 'text ARRAY').collate('utf8_general_ci');
		table
			.specificType('other_document', 'text ARRAY')
			.collate('utf8_general_ci');
	});
};

exports.down = function (knex, Promise) {};
