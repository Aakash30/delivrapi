exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable('business_address_area', table => {
            table.enum('type', ['mainbranch', 'subbranch']).defaultTo('mainbranch').collate('utf8_general_ci');
        })
        .alterTable('vehicle', table => {
            table.integer('business_area_id').defaultTo(0);
        })
};

exports.down = function (knex, Promise) {

};