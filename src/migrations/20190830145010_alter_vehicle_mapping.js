exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("vehicle_mapping", table => {
            table.integer("business_area_id");
        })
        .alterTable("vehicle", table => {
            table.dropColumn("business_area_id")
        })
};

exports.down = function (knex, Promise) {

};