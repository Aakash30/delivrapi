exports.up = function (knex, Promise) {
	return knex.schema.alterTable('vehicle_request_record', (builder) => {
		builder.dropColumn('status');
		builder.dropColumn('alloted_vehicle_count');
	});
};

exports.down = function (knex, Promise) {};
