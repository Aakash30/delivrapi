exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("sos_list", table => {
            table.increments("id").primary();
            table.string("contact_name").collate("utf8_general_ci");
            table.string("contact_number").collate("utf8_general_ci");
            table.boolean("is_default").defaultTo(false);
            table.integer("driver_id");
            table.timestamp("created_at", false);
        })
};

exports.down = function (knex, Promise) {

};