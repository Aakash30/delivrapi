exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("user_login", table => {
            table.dropColumn("is_mobile_verified");
            table.renameColumn("is_email_verified", "is_user_verified");
        })
};

exports.down = function (knex, Promise) {

};