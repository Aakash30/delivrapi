
exports.up = function(knex, Promise) {
    return knex.schema
    .createTable('contact_requests', table => { 
        table.increments('id').primary();
        table.string('name', 150);
        table.string('city', 20);
        table.string('phone_number');
        table.string('form_type').comment('stores whether the contact request was from the business or partner page')
        table.timestamps(false, true);
    })
};

exports.down = function(knex, Promise) {
  
};
