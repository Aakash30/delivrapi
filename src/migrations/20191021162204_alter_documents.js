exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("business", table => {
            table.string("bank_statement").collate("utf8_general_ci").comment("bank statement");
            table.string("credit_application_form").collate("utf8_general_ci")
            table.string("other_document").collate("utf8_general_ci")
        })
};

exports.down = function (knex, Promise) {

};