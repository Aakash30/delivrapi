exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("business_address_area", table => {
            table.increments('branch_area_id').primary();
            table.string('address_branch_name').collate('utf8_general_ci');
            table.integer('business_id')
                .unsigned()
                .references('business_id')
                .inTable('business')
                .onDelete('CASCADE')
                .comment('This defines user business');
            table.string('country').collate('utf8_general_ci');
            table.string('city').collate('utf8_general_ci');
            table.text('address_line_1').collate('utf8_general_ci');
            table.text('address_line_2').collate('utf8_general_ci');
            table.string('latitude').collate('utf8_general_ci');
            table.string('longitude').collate('utf8_general_ci');
            table.string('postal_Code').collate('utf8_general_ci');
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, false);
        })
        .alterTable("user_login", table => {
            table.integer('business_branch_id');
        })
        .alterTable("business", table => {
            table.dropColumn('country');
            table.dropColumn('city');
            table.dropColumn('address_line_1');
            table.dropColumn('address_line_2');
            table.dropColumn('latitude');
            table.dropColumn('longitude');
            table.dropColumn('postal_Code');
        })
};

exports.down = function (knex, Promise) {

};