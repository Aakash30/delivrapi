exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("user_login", table => {
            table.boolean("has_new_alerts").defaultTo(false);
        });
};

exports.down = function (knex, Promise) {

};