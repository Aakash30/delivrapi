exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("business_address_area", table => {
            table.integer("country").alter();
            table.integer("city").alter();
        });
};

exports.down = function (knex, Promise) {

};