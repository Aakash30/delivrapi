exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("batch", table => {
            table.decimal("total_price").comment("This price is based on the duration of the batch");
            table.decimal("paid_price").comment("Amount Paid by the business");
        })
};

exports.down = function (knex, Promise) {

};