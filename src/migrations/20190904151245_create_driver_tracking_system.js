exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("track_driver", table => {
            table.increments("track_id").primary();
            table.integer("driver_id");
            table.integer("business_id");
            table.integer("business_branc_id");
            table.string("driver_latitude").collate("utf8_general_ci");
            table.string("driver_longitude").collate("utf8_general_ci");
            table.timestamp("last_updated_at", true);
        })
};

exports.down = function (knex, Promise) {

};