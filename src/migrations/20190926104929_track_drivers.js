exports.up = function (knex, Promise) {
    return knex.schema
        .createTable("track_drivers", table => {
            table.increments("track_id").primary();
            table.integer("driver_id");
            table.string("latitude").collate("utf8_general_ci");
            table.string("longitude").collate("utf8_general_ci");
            table.timestamp("last_update");
        })
};

exports.down = function (knex, Promise) {

};