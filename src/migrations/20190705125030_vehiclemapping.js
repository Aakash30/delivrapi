exports.up = function (knex, Promise) {
    return knex.schema
        .createTable('vehicle_mapping', table => { // table: country_list
            table.increments('map_id').primary();
            table.integer('vehicle_id')
                .unsigned()
                .references('vehicle_id')
                .inTable('vehicle')
                .onDelete('CASCADE');
            table.integer('business_id')
                .unsigned()
                .references('business_id')
                .inTable('business')
                .onDelete('CASCADE');
            table.integer('driver_id')
                .unsigned()
                .references('user_id')
                .inTable('user_login')
                .onDelete('CASCADE');
            table.timestamp("assigned_to_business_from", false);
            table.timestamp("assigned_to_business_to", false);
            table.integer('created_by');
            table.integer('updated_by');
            table.timestamps(false, false);
        })
        .createTable('vehicle_mapping_history', table => { // table: city_list
            table.increments('history_id').primary();
            table.integer('vehicle_id')
                .unsigned()
                .references('vehicle_id')
                .inTable('vehicle')
                .onDelete('CASCADE');
            table.integer('business_id')
                .unsigned()
                .references('business_id')
                .inTable('business')
                .onDelete('CASCADE');
            table.integer('driver_id')
                .unsigned()
                .references('user_id')
                .inTable('user_login')
                .onDelete('CASCADE');
            table.timestamp("assigned_on", false);
            table.timestamp("unassigned_on", false);
            table.integer('assinged_by');
            table.integer('unassigned_by');
        });
};

exports.down = function (knex, Promise) {

};