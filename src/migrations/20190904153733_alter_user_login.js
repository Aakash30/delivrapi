exports.up = function (knex, Promise) {
    return knex.schema
        .alterTable("user_login", table => {
            table.string("device_id").collate("utf8_general_ci");
            table.string("device_token").collate("utf8_general_ci");
            table.string("device_type").collate("utf8_general_ci");
        })
};

exports.down = function (knex, Promise) {

};