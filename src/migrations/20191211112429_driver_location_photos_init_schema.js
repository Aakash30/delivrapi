
exports.up = function(knex, Promise) {
    return knex.schema
    .createTable('driver_location', table => { 
        table.increments('id').primary();
        table.integer('driver_id').unsigned().references('user_id').inTable('user_login').onDelete('CASCADE');
        table.decimal('lat', null);
        table.decimal('lng', null);
        table.text('image');
        table.timestamps(false, true);
    })
};

exports.down = function(knex, Promise) {
  
};
