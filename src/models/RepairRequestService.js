'use strict';

const Model = require('objection').Model;

class RepairRequestService extends Model {

    // Table name is the only required property.
    static get tableName() {
        return 'repair_request_service';
    }

    static get idColumn() {
        return 'id';
    }

    //This object defines the relations to other models.
    static get relationMappings() {
        return {

            service: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Services',
                join: {
                    from: 'repair_request_service.service_id',
                    to: 'service_list.list_id'
                }
            },
            serviceCategory: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/ServiceCategory',
                join: {
                    from: 'repair_request_service.service_category_id',
                    to: 'service_category.service_category_id'
                }
            }
        }
    }
}
module.exports = RepairRequestService;