'use strict';

const Model = require('objection').Model;

class TrackDriver extends Model {
    // Table name is the only required property.
    static get tableName() {
        return 'track_driver';
    }

    static get idColumn() {
        return 'track_id';
    }

    static get relationMappings() {
        return {
            driver: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/Users",
                join: {
                    from: 'track_driver.driver_id',
                    to: 'user_login.user_id'
                }
            }
        }
    }
    async $beforeInsert() {
        await super.$beforeInsert();

    }
}
module.exports = TrackDriver;