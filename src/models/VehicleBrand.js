'use strict';

const Model = require('objection').Model;
const fn = require('objection').fn;
const validator = require('validator');
const ValidationError = require('objection').ValidationError;
const bcrypt = require('bcrypt');
//const Business = require('../models/Business');

class VehicleBrand extends Model {
    // Table name is the only required property.
    static get tableName() {
        return 'vehicle_brand';
    }

    static get idColumn() {
        return 'brand_id';
    }

    static get relationMappings() {
        return {
            VehicleModel: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/VehicleModel',

                join: {
                    from: 'vehicle_brand.brand_id',
                    to: 'vehicle_model.brand_id'
                }
            },
            Vehicle: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + "/Vehicle",
                join: {
                    from: 'vehicle_brand.brand_id',
                    to: 'vehicle.brand'
                }
            },
        };
    }

    async $beforeInsert() {
        await super.$beforeInsert();
        this.created_at = new Date().toISOString();
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);
        this.updated_at = new Date().toISOString();
    }
    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);
        return json;
    }
}

module.exports = VehicleBrand;