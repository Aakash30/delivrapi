'use strict';

const Model = require('objection').Model;
const Vehicle = require('./Vehicle');

class VehicleMappingHistory extends Model {
    // Table name is the only required property.
    static get tableName() {
        return 'vehicle_mapping_history';
    }

    static get idColumn() {
        return 'history_id';
    }

    static get relationMappings() {
        return {
            Vehicle: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Vehicle',

                join: {
                    from: 'vehicle_mapping_history.vehicle_id',
                    to: 'vehicle.vehicle_id'
                }
            },
            Users: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Users',

                join: {
                    from: 'vehicle_mapping_history.driver_id',
                    to: 'user_login.user_id'
                }
            },
            Business: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Business',

                join: {
                    from: 'vehicle_mapping_history.business_id',
                    to: 'business.business_id'
                }
            }
        }
    }

    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);
        return json
    }
}

module.exports = VehicleMappingHistory;