'use strict';

const Model = require('objection').Model;
const jwt = require('jsonwebtoken');
const validator = require('validator');
const ValidationError = require('objection').ValidationError;
const bcrypt = require('bcrypt');
const Business = require('../models/Business');
const VehicleMapping = require('../models/VehicleMapping');

class User extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'user_login';
  }

  static get idColumn() {
    return 'user_id';
  }

  async comparePassword(password) {
    if (!password) {
      return false;
    }

    let pass = await bcrypt.compare(password, this.password);
    return pass;
  }

  // This object defines the relations to other models.
  static get relationMappings() {
    return {

      Business: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Business",
        join: {
          from: 'user_login.business_id',
          to: 'business.business_id'
        }
      },
      vehicles: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/VehicleMapping",

        join: {
          from: 'user_login.user_id',
          to: 'vehicle_mapping.driver_id'
        }
      },
      businessArea: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/BusinessBranchArea",
        join: {
          from: "user_login.business_branch_id",
          to: "business_address_area.branch_area_id"
        }
      },
      countries: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Countries",
        join: {
          from: "user_login.country",
          to: "country_list.country_id"
        }
      },
      cities: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/City",
        join: {
          from: "user_login.city",
          to: "city_list.city_id"
        }
      },
      orders: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/Order",
        join: {
          from: "user_login.user_id",
          to: "order.driver_id"
        }
      },
      tracker: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/TrackDriver",
        join: {
          from: "user_login.user_id",
          to: "track_driver.driver_id"
        }
      },
      driverLocation: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/DriverLocation",
        join: {
          from: "user_login.user_id",
          to: "driver_location.driver_id"
        }
      },
      accountsUser: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/Accounts",
        join: {
          from: "user_login.user_id",
          to: "business_accounts.driver_id"
        }
      },
      fuel_accounts: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/FuelUpdate",
        join: {
          from: "user_login.user_id",
          to: "fuel_updates.driver_id"
        }
      },
      police_fine: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + "/PoliceFine",
        join: {
          from: "user_login.user_id",
          to: "police_fines.driver_id"
        }
      }
    };
  }

  $formatJson(json, opt) {
    json = super.$formatJson(json, opt);
    delete json.password;
    return json
  }

  async getJWT() {
    return await jwt.sign({
      userId: this.user_id,
      roles: this.user_role,
      type: this.user_type
    }, CONFIG.jwt_encryption);
  }

  async $beforeInsert() {
    await super.$beforeInsert();

    /* console.log(this.userTypeId);
     if ((this.userTypeId !== 3 && this.userTypeId !== 4 && this.userTypeId !== 5 && this.userTypeId !== 6) && !this.email) {
       if (!validator.isEmail(this.email || '')) {
         console.log('in before insert');
         throw badRequestError('Please enter email address!');
       }
     }
     if ((this.userTypeId !== 3 && this.userTypeId !== 4 && this.userTypeId !== 5 && this.userTypeId !== 6) && !this.password) {
       throw badRequestError('Please enter password!');
     }*/

    // let checkSpecialCharacters = this.name.replace(" ", "");
    // if (!validator.isAlphanumeric(checkSpecialCharacters)) {
    //   throw badRequestError("Please enter valid name!");
    // }

    if (this.mobile_number) {
      let mobile = this.mobile_number.split('-');

      if (!validator.isNumeric(mobile[1])) {
        throw badRequestError("Please enter valid mobile number!");
      }

      // if (!validator.isLength(mobile[1], {
      //     min: 10,
      //     max: 10
      //   })) {
      //   console.log('in before insert');
      //   throw badRequestError('Please enter valid mobile number!');

      // }
    }

    // if (validator.isNumeric(this.name)) {
    //   throw badRequestError("Please enter valid name!");
    // }
    if (this.email_id) {
      if (!validator.isEmail(this.email_id || '')) {

        throw badRequestError("Not a valid email address!");
      }

      let result = await this.constructor.query().skipUndefined().select('user_id').where('email_id', this.email_id).first();
      if (result) {
        throw badRequestError("Account with this email already exists!");
      }

    }
    if (this.mobile_number) {
      let result = await this.constructor.query().skipUndefined().select('user_id').where('mobile_number', this.mobile_number).first();
      if (result) {
        throw badRequestError("Account with this mobile number already exists!");
      }
    }
    if (this.password) {
      this.password = await bcrypt.hash(this.password, 10);
    }
    //latitude and longitude validation only for customer
    // if (this.userTypeId == 4 && (!this.latitude || !this.longitude)) {
    //   throw badRequestError("Please enter Location!!!");
    // }
    this.created_at = new Date().toISOString();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeUpdate(opt, queryContext);
    console.log(opt);
    /*if (!opt.patch) {
      if (!this.email) {
        throw badRequestError("Please enter email");
      }
      if (!this.mobileNumber) {
        throw badRequestError("Please enter mobile number");
      }
      if (!this.name) {
        throw badRequestError("Please enter name");
      }
      let checkSpecialCharacters = this.name.replace(" ", "");
      if (!validator.isAlphanumeric(checkSpecialCharacters)) {
        throw badRequestError("Please enter valid name");
      }
      if (!validator.isNumeric(this.mobileNumber)) {
        throw badRequestError("Please enter valid mobile number");
      }
      if (validator.isNumeric(this.name)) {
        throw badRequestError("Please enter valid name");
      }
    } else {
      if (this.isActive === null) {
        if (!this.name) {
          throw badRequestError("Please enter name");
        }

        let checkSpecialCharacters = this.name.replace(" ", "");
        if (!validator.isAlphanumeric(checkSpecialCharacters)) {
          throw badRequestError("Please enter valid name");
        }
        if (validator.isNumeric(this.name)) {
          throw badRequestError("Please enter valid name");
        }
      }
    }*/

    this.updated_at = new Date().toISOString();

    if (this.password) {
      this.password = await bcrypt.hash(this.password, 10);
    }
  }
}

module.exports = User;