'use strict';

const Model = require('objection').Model;
const Vehicle = require('../models/Vehicle');

class VehicleMapping extends Model {
    // Table name is the only required property.
    static get tableName() {
        return 'vehicle_mapping';
    }

    static get idColumn() {
        return 'map_id';
    }

    static get relationMappings() {
        return {
            vehicle: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Vehicle',

                join: {
                    from: 'vehicle_mapping.vehicle_id',
                    to: 'vehicle.vehicle_id'
                }
            },
            batch: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Batch',

                join: {
                    from: 'vehicle_mapping.batch_id',
                    to: 'batch.id'
                }
            },
            Users: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Users',

                join: {
                    from: 'vehicle_mapping.driver_id',
                    to: 'user_login.user_id'
                }
            },
            Business: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Business',

                join: {
                    from: 'vehicle_mapping.business_id',
                    to: 'business.business_id'
                }
            },
            businessBranch: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/BusinessBranchArea',
                join: {
                    from: 'vehicle_mapping.business_area_id',
                    to: 'business_address_area.branch_area_id'
                }
            }
        }
    }

    async $beforeInsert() {
        await super.$beforeInsert();
        this.created_at = new Date().toISOString();
    }
    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);
        return json
    }
}

module.exports = VehicleMapping;