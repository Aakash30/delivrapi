'use strict';

const Model = require('objection').Model;
const moment = require('moment');

class VehicleRequest extends Model {

    // Table name is the only required property.
    static get tableName() {
        return 'vehicle_request_record';
    }

    static get idColumn() {
        return 'request_id';
    }

    static get relationMappings() {
        return {
            Business: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Business',

                join: {
                    from: 'vehicle_request_record.business_id',
                    to: 'business.business_id'
                }
            }
        }
    }

    async $beforeInsert() {
        await super.$beforeInsert();
        this.requested_on = new Date(new Date().toUTCString());
        this.request_number = "NVR-" + Date.now();
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);
        this.last_updated_on = new Date().toISOString();
    }
    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);
        return json
    }
}

module.exports = VehicleRequest;