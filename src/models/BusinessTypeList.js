'use strict';

const validator = require('validator');
const ValidationError = require('objection').ValidationError;

const Model = require('objection').Model;

class BusinessTypeList extends Model {
    static get tableName() {
        return 'business_type_list';
    }

    // static get jsonSchema() {
    //     return {
    //         type: 'object',
    //         required: ['business_type'],

    //         properties: {
    //             business_type: {
    //                 type: 'string'
    //             }
    //         }
    //     }
    // }

    // action need to perform before insert
    async $beforeInsert() {
        await super.$beforeInsert();

        if (validator.isEmpty(this.business_type.trim())) {
            throw global.badRequestError('Please enter Business Type.');
        }

        if (!(/^[a-zA-Z0-9 .-]*$/).test(this.business_type)) {
            throw global.badRequestError('Only alphabet, numbers, ., - and space are allowd.');
        }
        this.business_type = this.business_type.toLowerCase();

        // check for duplicate

        let result = await this.constructor.query().skipUndefined().select('id').where('business_type', this.business_type).first();
        if (result) {
            throw global.badRequestError("This Business Type already exists.");
        }

        this.created_at = new Date().toISOString();
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);

        if (opt.patch) {

        }

        if (this.business_type != undefined) {
            if (validator.isEmpty(this.business_type.trim())) {
                throw global.badRequestError('Please enter Business Type.');
            }

            if (!(/^[a-zA-Z0-9 .-]*$/).test(this.business_type)) {
                throw global.badRequestError('Only alphabet, numbers, ., - and space are allowd.');
            }
            this.business_type = this.business_type.toLowerCase();

            // check for duplicate
            let result = await this.constructor.query().skipUndefined().select('id').where('business_type', this.business_type).whereNot('id', queryContext.business_type_id).first();
            if (result) {
                throw global.badRequestError("This Business Type already exists.");
            }
        }

        this.updated_at = new Date().toISOString();

    }

    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);
        return json
    }
}

module.exports = BusinessTypeList;