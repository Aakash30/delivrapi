'use strict';

const Model = require('objection').Model;
const validator = require('validator');

const OrderItems = require('../models/OrderItem');

const RejectedOrders = require('../models/RejectedOrders');
class Order extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'order';
  }

  static get idColumn() {
    return 'order_id';
  }
  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  // static get jsonSchema() {
  //   return {
  //     type: 'object',
  //     required: ['name', 'mobileNumber'],

  //     properties: {
  //       id: {
  //         type: 'integer'
  //       },
  //       name: {
  //         type: 'string',
  //         minLength: 1,
  //         maxLength: 255
  //       },
  //       email: {
  //         type: 'string'
  //       },
  //       mobileNumber: {
  //         type: 'string',
  //         minLength: 1,
  //         maxLength: 255
  //       }
  //     }
  //   };
  // }

  // async comparePassword(password) {
  //   if (!password) {
  //     return false;
  //   }

  //   let pass = await bcrypt.compare(password, this.password);
  //   return pass;
  // }

  //This object defines the relations to other models.
  static get relationMappings() {
    return {

      OrderItem: {
        relation: Model.HasManyRelation,
        modelClass: __dirname + '/OrderItem',
        join: {
          from: 'order.order_id',
          to: 'item_list.order_id'
        }
      },
      countries: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/Countries',
        join: {
          from: 'order.country',
          to: 'country_list.country_id'
        }
      },
      cities: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/City',
        join: {
          from: 'order.city',
          to: 'city_list.city_id'
        }
      },
      driver: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/Users',
        join: {
          from: 'order.driver_id',
          to: 'user_login.user_id'
        }
      },
      businessBranch: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/BusinessBranchArea',
        join: {
          from: 'order.business_branch_id',
          to: 'business_address_area.branch_area_id'
        }
      }
    };
  }

  $formatJson(json, opt) {
    json = super.$formatJson(json, opt);
    return json
  }


  async $beforeInsert(opt, queryContext) {
    await super.$beforeInsert(opt, queryContext);

    if (this.email) {
      if (!validator.isEmail(this.email || '')) {

        throw badRequestError("Not a valid email address!");
      }

    }

    if (this.mobile_number) {
      let mobile = this.mobile_number.split('-');

      if (!validator.isNumeric(mobile[1])) {
        throw badRequestError("Please enter valid mobile number!");
      }

    }

    if (!validator.isAlpha(this.first_name.trim())) {
      throw badRequestError('Please enter valid name!');
    }

    this.first_name = this.first_name.trim();
    if (!validator.isAlpha(this.last_name.trim())) {
      throw badRequestError('Please enter valid name!');
    }

    this.last_name = this.last_name.trim();

    this.invoice_id = "OD_" + Date.now();
    this.created_at = new Date().toISOString();

  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeUpdate(opt, queryContext);

    this.update_by = queryContext.created_by;
    this.updated_at = new Date().toISOString();

  }

}

module.exports = Order;