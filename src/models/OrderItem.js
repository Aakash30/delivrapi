'use strict';

const Model = require('objection').Model;
const jwt = require('jsonwebtoken');
const validator = require('validator');
const ValidationError = require('objection').ValidationError;
const bcrypt = require('bcrypt');
const Order = require('./Order');
class OrderItem extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'item_list';
  }

  static get idColumn() {
    return 'item_id';
  }

  // static get relationMappings() {
  //   return {

  //     Order: {
  //       relation: Model.BelongsToOneRelation,
  //       modelClass: Order,
  //       join: {
  //         from: 'item_list.order_id',
  //         to: 'order.order_id'
  //       }
  //     }
  //   };
  // }
  async $beforeInsert() {
    await super.$beforeInsert();

    if (this.price) {
      if (!validator.isNumeric(this.price)) {
        throw badRequestError("Please enter number only!");
      }

    }

  }

}

module.exports = OrderItem;