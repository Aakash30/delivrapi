'use strict';

const Model = require('objection').Model;

class VehicleModel extends Model {
    // Table name is the only required property.
    static get tableName() {
        return 'vehicle_model';
    }

    static get idColumn() {
        return 'model_id';
    }

    static get relationMappings() {
        return {
            VehicleBrand: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/VehicleBrand',

                join: {
                    from: 'vehicle_model.brand_id',
                    to: 'vehicle_brand.brand_id'
                }
            },
            Vehicle: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + "/Vehicle",
                join: {
                    from: 'vehicle_model.model_id',
                    to: 'vehicle.model'
                }
            },
        };
    }

    async $beforeInsert() {
        await super.$beforeInsert();
        this.created_at = new Date().toISOString();
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);
        this.updated_at = new Date().toISOString();
    }
    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);
        return json;
    }
}

module.exports = VehicleModel;