'use strict';
const Model = require('objection').Model;

class DriverLocation extends Model {
    static get tableName() {
        return 'driver_location';
    }

    static get relationMappings() {
        return {
            driver: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/Users",
                join: {
                    from: 'driver_location.driver_id',
                    to: 'user_login.user_id'
                }
            }
        };
    }
}
module.exports = DriverLocation;