'use strict';

const Model = require('objection').Model;

class Contact extends Model {

    // return table Name
    static get tableName() {
        return 'contact_requests';
    }


    // static get relationMappings() {
    //     return {
    //         business: {
    //             relation: Model.HasManyRelation,
    //             modelClass: __dirname + '/City',

    //             join: {
    //                 from: 'coupons.business_id',
    //                 to: 'business.business_id'
    //             }
    //         },
    //     }
    // }


}

module.exports = Contact;