"use strict";

const Model = require("objection").Model;
const Country = require("../models/Countries");
const users = require("../models/Users");

const validator = require("validator");

class Payment extends Model {
  // return table Name
  static get tableName() {
    return "payments";
  }

  static get idColumn() {
    //payment_id is not the primary key of this table
    //it just happens to be unique
    return "id";
  }

  static get relationMappings() {
    return {
      batch: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + "/Batch",

        join: {
          from: "payments.batch_id",
          to: "batch.id"
        }
      },
    };
  }
}

module.exports = Payment;