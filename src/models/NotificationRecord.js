"use strict";
const Model = require("objection").Model;

class NotificationRecord extends Model {

    // return table Name
    static get tableName() {
        return "notifications";
    }

    static get idColumn() {
        return 'notification_id';
    }
}

module.exports = NotificationRecord;