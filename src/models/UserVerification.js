'use strict';

const Model = require('objection').Model;
class UserVerification extends Model {
  static get tableName() {
    return 'user_verification';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      // required: ['token, userId, codeType'],

      properties: {
        token: {
          type: 'string'
        }
      }
    }
  }

  static get relationMappings() {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: __dirname + '/Users',

        join: {
          from: 'user_verification.userId',
          to: 'users.id'
        }
      }
    }
  }

  $formatJson(json, opt) {
    json = super.$formatJson(json, opt);
    delete json.userId;
    delete json.id;
    return json
  }
}

module.exports = UserVerification;
