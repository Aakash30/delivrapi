'use strict';

const Model = require('objection').Model;

class Coupon extends Model {

    // return table Name
    static get tableName() {
        return 'coupons';
    }

    static get idColumn() {
        return 'coupon_id';
    }

    static get relationMappings() {
        return {
            business: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/Business',

                join: {
                    from: 'coupons.business_id',
                    to: 'business.business_id'
                }
            },
            businessBranch: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + "/BusinessBranchArea",
                join: {
                    from: 'coupons.business_branch_id',
                    to: 'business_address_area.branch_area_id'
                }
            },
        }
    }

    async $beforeInsert() {

    }
}

module.exports = Coupon;