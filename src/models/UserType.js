'use strict';

const Model = require('objection').Model;
class UserType extends Model {
  static get tableName() {
    return 'user_type';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['userType'],

      properties: {
        token: {
          type: 'string'
        }
      }
    }
  }

  static get relationMappings() {
    return {
      
    }
  }

  $formatJson(json, opt) {
    json = super.$formatJson(json, opt);
    delete json.userId;
    delete json.id;
    return json
  }
}

module.exports = UserType;
