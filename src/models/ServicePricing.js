'use strict';

const Model = require('objection').Model;

class ServicePricing extends Model {

    // Table name is the only required property.
    static get tableName() {
        return 'service_pricing';
    }

    static get idColumn() {
        return 'id';
    }

    //This object defines the relations to other models.
    static get relationMappings() {
        return {

            countries: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Countries',
                join: {
                    from: 'service_pricing.country',
                    to: 'country_list.country_id'
                }
            },
            brandList: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/VehicleBrand',
                join: {
                    from: 'service_pricing.brand',
                    to: 'vehicle_brand.brand_id'
                }
            },
            modelList: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/VehicleModel',
                join: {
                    from: 'service_pricing.model',
                    to: 'vehicle_model.model_id'
                }
            }
        }
    }
    async $beforeInsert() {
        await super.$beforeInsert();

        // let result = await this.constructor.query().skipUndefined().select('id').where('service_category_name', this.service_category_name).first();
        // if (result) {
        //     throw badRequestError("This service category already exists.");
        // }
        this.created_at = new Date().toISOString();
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);

        // let result = await this.constructor.query().skipUndefined().select('id').where('service_category_name', this.service_category_name).where("id", "!=", this.id).first();
        // if (result) {
        //     throw badRequestError("This service category already exists.");
        // }
        this.updated_at = new Date().toISOString();
    }
}

module.exports = ServicePricing;