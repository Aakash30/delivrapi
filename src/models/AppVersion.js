const Model = require('objection').Model;


class AppVersion extends Model {

    static get tableName() {
        return 'app_version';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['ios_min_version', 'ios_max_version', 'android_min_mersion', 'android_max_version', 'user_type'],

            properties: {
                user_type: {
                    type: 'string',
                    minLength: 1,
                    maxLength: 255
                },
                ios_min_version: {
                    type: 'float'
                },
                android_min_mersion: {
                    type: 'float'
                },
            }
        };
    }

    static get relationMappings() {
        return {
            // customization_categories: {
            //     relation: Model.HasManyRelation,
            //     modelClass: __dirname + '/Customization_Category',
            //     join: {
            //         to: 'customization_category.dishId',
            //         from: 'dish.id'
            //     }
            // }
        };
    }
}

module.exports = AppVersion;