'use strict';

const Model = require('objection').Model;
const validator = require('validator');
const ValidationError = require('objection').ValidationError;
const bcrypt = require('bcrypt');
const VehicleBrand = require('../models/VehicleBrand');
const VehicleModel = require('../models/VehicleModel');

class Vehicle extends Model {
    // Table name is the only required property.
    static get tableName() {
        return 'vehicle';
    }

    static get idColumn() {
        return 'vehicle_id';
    }

    static get relationMappings() {
        return {
            vehicle_images: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + "/VehicleImages",
                join: {
                    from: 'vehicle.vehicle_id',
                    to: 'vehicle_images.vehicle_id'
                }
            },
            vehicle_brand: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/VehicleBrand",
                join: {
                    from: 'vehicle.brand',
                    to: 'vehicle_brand.brand_id'
                }
            },
            vehicle_model: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/VehicleModel",
                join: {
                    from: 'vehicle.model',
                    to: 'vehicle_model.model_id'
                }
            },
            vehicle_mapping: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + "/VehicleMapping",
                join: {
                    from: 'vehicle.vehicle_id',
                    to: 'vehicle_mapping.vehicle_id'
                }
            },
            cities: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/City",
                join: {
                    from: 'vehicle.city',
                    to: 'city_list.city_id'
                }
            },
            countries: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/Countries",
                join: {
                    from: 'vehicle.country',
                    to: 'country_list.country_id'
                }
            }
        };
    }

    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);
        return json;
    }

    async $beforeInsert() {
        await super.$beforeInsert();

        if (this.color) {
            this.color = this.color.toUpperCase();
        }


        if (!validator.isNumeric(this.engine_capacity)) {
            throw global.badRequestError("Only numbers are allowed.");
        }

        if (!validator.isAlphanumeric(this.engine_number)) {
            throw global.badRequestError("Only alphabets and numbers are allowed..s.");
        }

        if (!validator.isNumeric(this.price)) {
            throw global.badRequestError("Only numbers are allowed.");
        }

        if (!validator.isNumeric(this.mileage)) {
            throw global.badRequestError("Only numbers are allowed.");
        }

        let checkEngineNumber = await this.constructor.query().skipUndefined().select('vehicle_id').where('engine_number', this.engine_number).first();
        if (checkEngineNumber) {
            throw global.badRequestError("Engine number already exists!");
        }

        let checkVehicleNumber = await this.constructor.query().skipUndefined().select('vehicle_id').where('vehicle_number', this.vehicle_number).first();
        if (checkVehicleNumber) {
            throw global.badRequestError("Vehicle registration number already exists!");
        }

        let checkChasingNumber = await this.constructor.query().skipUndefined().select('vehicle_id').where('chasing_number', this.chasing_number).first();
        if (checkChasingNumber) {
            throw global.badRequestError("Vehicle chasing number already exists!");
        }


        this.created_at = new Date().toISOString();
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);

        if (this.color) {
            this.color = this.color.toUpperCase();
        }

        if (this.engine_capacity && !validator.isNumeric(this.engine_capacity)) {
            throw global.badRequestError("Only numbers are allowed.");
        }

        if (this.engine_number && !validator.isAlphanumeric(this.engine_number)) {
            throw global.badRequestError("Only alphabets and numbers are allowed.");
        }

        if (this.price && !validator.isNumeric(this.price)) {
            throw global.badRequestError("Only numbers are allowed.");
        }

        if (this.mileage && !validator.isNumeric(this.mileage)) {
            throw global.badRequestError("Only numbers are allowed.");
        }

        if (this.engine_number) {
            let checkEngineNumber = await this.constructor.query().skipUndefined().select('vehicle_id').where('engine_number', this.engine_number).whereNot('vehicle_id', this.vehicle_id).first();
            if (checkEngineNumber) {
                throw global.badRequestError("Engine number already exists!");
            }

        }

        if (this.vehicle_number) {
            let checkVehicleNumber = await this.constructor.query().skipUndefined().select('vehicle_id').where('vehicle_number', this.vehicle_number).whereNot('vehicle_id', this.vehicle_id).first();
            if (checkVehicleNumber) {
                throw global.badRequestError("Vehicle Registration number already exists!");
            }
        }

        if (this.chasing_number) {
            let checkChasingNumber = await this.constructor.query().skipUndefined().select('vehicle_id').where('chasing_number', this.chasing_number).whereNot('vehicle_id', this.vehicle_id).first();
            if (checkChasingNumber) {
                throw global.badRequestError("Vehicle chasing number already exists!");
            }
        }


        this.updated_at = new Date().toISOString();
    }
}

module.exports = Vehicle;