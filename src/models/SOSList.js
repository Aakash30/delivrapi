'use strict';

const Model = require('objection').Model;

class SOSList extends Model {

    // Table name is the only required property.
    static get tableName() {
        return 'sos_list';
    }

    static get idColumn() {
        return 'id';
    }
}

module.exports = SOSList;