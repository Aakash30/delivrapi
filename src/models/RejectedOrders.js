'use strict';

const Model = require('objection').Model;

class RejectedOrders extends Model {
    // Table name is the only required property.
    static get tableName() {
        return 'rejected_order_history';
    }

    static get idColumn() {
        return 'history_id';
    }

    static get relationMappings() {
        return {
            rejected: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Order',

                join: {
                    from: 'rejected_order_history.order_id',
                    to: 'order.order_id'
                }
            }
        };
    }

    async $beforeInsert() {
        await super.$beforeInsert();

        this.rejected_on = new Date().toISOString();

    }
}
module.exports = RejectedOrders;