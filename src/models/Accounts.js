'use strict';
const Model = require('objection').Model;

class Accounts extends Model {
    static get tableName() {
        return 'business_accounts';
    }
    static get idColumn() {
        return 'account_id';
    }

    static get relationMappings() {
        return {
            country_currency: {
                relation: Model.ManyToManyRelation,
                modelClass: __dirname + "/Countries",
                join: {
                    from: 'business_accounts.business_id',
                    through: {
                        from: 'business_address_area.business_id',
                        to: 'business_address_area.country'
                    },
                    to: 'country_list.country_id'
                }
            },
        };
    }

    async $beforeInsert() {
        this.created_at = new Date().toISOString();
    }
    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);
        return json;
    }
}
module.exports = Accounts;