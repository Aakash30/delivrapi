'use strict';
const Model = require('objection').Model;

class Batches extends Model {
    static get tableName() {
        return 'batch';
    }

    static get relationMappings() {
        return {
            business: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/Business",
                join: {
                    from: 'batch.business_id',
                    to: 'business.business_id'
                }
            },
            vehicleMapping: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + "/VehicleMapping",
                join: {
                    from: 'batch.id',
                    to: 'vehicle_mapping.batch_id'
                }
            },

            payments: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + "/Payment",
                join: {
                    from: 'batch.id',
                    to: 'payments.batch_id'
                }
            },
            businessUser: {
                relation: Model.ManyToManyRelation,
                modelClass: __dirname + "/Users",
                join: {
                    from: 'batch.business_id',
                    through: {
                        from: 'business.business_id',
                        to: 'business.business_id'
                    },
                    to: 'user_login.business_id'
                }
            },
            vehicles: {
                relation: Model.ManyToManyRelation,
                modelClass: __dirname + "/Vehicle",
                join: {
                    from: 'batch.id',
                    through: {
                        from: 'vehicle_mapping.batch_id',
                        to: 'vehicle_mapping.vehicle_id'
                    },
                    to: 'vehicle.vehicle_id'

                }
            },
        };
    }

}
module.exports = Batches;