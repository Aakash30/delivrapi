'use strict';

const Model = require('objection').Model;


class VehicleImages extends Model {
    // return table Name
    static get tableName() {
        return 'vehicle_images';
    }

    static get idColumn() {
        return 'image_entry_id';
    }

    static get relationMappings() {
        return {
            vehicles: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/Vehicle",
                join: {
                    from: 'vehicle_images.vehicle_id',
                    to: 'vehicle.vehicle_id'
                }
            }
        }
    }

}
module.exports = VehicleImages;