'use strict';

const Model = require('objection').Model;

class Services extends Model {

    // Table name is the only required property.
    static get tableName() {
        return 'service_list';
    }

    static get idColumn() {
        return 'list_id';
    }

    //This object defines the relations to other models.
    static get relationMappings() {
        return {

            serviceCategory: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/ServiceCategory',
                join: {
                    from: 'service_list.service_category',
                    to: 'service_category.service_category_id'
                }
            },
            servicePricing: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/ServicePricing',
                join: {
                    from: 'service_list.list_id',
                    to: 'service_pricing.service_id'
                }
            }
        }
    }

    async $beforeInsert() {
        await super.$beforeInsert();

        let result = await this.constructor.query().skipUndefined().select('list_id').where('service_name', this.service_name).first();
        if (result) {
            throw badRequestError("This service category already exists.");
        }
        this.created_at = new Date().toISOString();
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);

        let result = await this.constructor.query().skipUndefined().select('list_id').where('service_name', this.service_name).where("list_id", "!=", this.list_id).first();
        if (result) {
            throw badRequestError("This service category already exists.");
        }
        this.updated_at = new Date().toISOString();
    }
}

module.exports = Services;