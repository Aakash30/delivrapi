'use strict';

const Model = require('objection').Model;

class ServiceCategory extends Model {

    // Table name is the only required property.
    static get tableName() {
        return 'service_category';
    }

    static get idColumn() {
        return 'service_category_id';
    }

    async $beforeInsert() {
        await super.$beforeInsert();

        let result = await this.constructor.query().skipUndefined().select('service_category_id').where('service_category_name', this.service_category_name).first();
        if (result) {
            throw global.badRequestError("This service category already exists.");
        }
        this.created_at = new Date().toISOString();
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);

        if (this.service_category_name) {
            let result = await this.constructor.query().skipUndefined().select('service_category_id').where('service_category_name', this.service_category_name).where("service_category_id", "!=", this.service_category_id).first();
            if (result) {
                throw global.badRequestError("This service category already exists.");
            }
        }

        this.updated_at = new Date().toISOString();
    }
}

module.exports = ServiceCategory;