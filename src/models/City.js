'use strict';

const Model = require('objection').Model;
const Country = require('../models/Countries');
const users = require('../models/Users');

const validator = require('validator');


class Cities extends Model {
    // return table Name
    static get tableName() {
        return 'city_list';
    }

    static get idColumn() {
        return 'city_id';
    }

    static get relationMappings() {
        return {
            Country: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/Countries',

                join: {
                    from: 'city_list.country_id',
                    to: 'country_list.country_id'
                }
            },
            businessBranch: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + "/BusinessBranchArea",
                join: {
                    from: 'city_list.city_id',
                    to: 'business_address_area.city'
                }
            },
        }
    }

    async $beforeInsert() {
        await super.$beforeInsert();

        if (this.city) {

            if (validator.isEmpty(this.city.trim())) {
                throw badRequestError('Please enter Country name.');
            }

            if (!(/^[a-zA-Z ]*$/).test(this.city)) {
                throw badRequestError('Only alphabet and space are allowd.');
            }


            let result = await this.constructor.query().skipUndefined().select('city_id').where('city', this.city).first();
            if (result) {
                throw badRequestError("This city already exists.");
            }


        }
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);

        if (opt.patch) {

        }

        if (queryContext.city_id != undefined) {
            if (validator.isEmpty(this.city.trim())) {
                throw badRequestError('Please enter Country name.');
            }

            if (!(/^[a-zA-Z ]*$/).test(this.city)) {
                throw badRequestError('Only alphabet and space are allowd.');
            }


            let result = await this.constructor.query().skipUndefined().select('city_id').where('city', this.city).whereNot("city_id", queryContext.city_id).first();
            if (result) {
                throw badRequestError("This country already exists.");
            }
        }

    }
    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);

        return json
    }
}

module.exports = Cities;