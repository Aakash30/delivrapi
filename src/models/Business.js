'use strict';

const Model = require('objection').Model;
const validator = require('validator');


class Business extends Model {

    // return table Name
    static get tableName() {
        return 'business';
    }

    static get idColumn() {
        return 'business_id';
    }

    static get relationMappings() {
        return {
            businessType: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/BusinessTypeList',

                join: {
                    from: 'business.business_type',
                    to: 'business_type_list.id'
                }
            },
            businessUsers: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/Users',
                join: {
                    from: 'business.business_id',
                    to: 'user_login.business_id'
                }
            },
            businessBranch: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/BusinessBranchArea',
                join: {
                    from: 'business.business_id',
                    to: 'business_address_area.business_id'
                }
            },
            vehicleMapping: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/VehicleMapping',

                join: {
                    from: 'business.business_id',
                    to: 'vehicle_mapping.business_id'
                }
            },
        }
    }

    async $beforeInsert() {
        await super.$beforeInsert();

        if (this.email_id) {
            if (!validator.isEmail(this.email_id || '')) {

                throw badRequestError("Not a valid email address!");
            }

            //console.log(this.userTypeId)
            let result = await this.constructor.query().skipUndefined().select('business_id').where('email_id', this.email_id).first();
            if (result) {
                throw badRequestError("Business with this email already exists!");
            }

        }
        if (this.mobile_number) {

            let mobile = this.mobile_number.split('-');
            if (!validator.isNumeric(mobile[1])) {
                throw badRequestError("Please enter valid mobile number!");
            }

            // if (!validator.isLength(this.mobile_number, {
            //         min: 10,
            //         max: 10
            //     })) {

            //     throw badRequestError('Please enter valid mobile number!');

            // }

            let result = await this.constructor.query().skipUndefined().select('business_id').where('mobile_number', this.mobile_number).first();
            if (result) {
                throw badRequestError("Business with this mobile number already exists!");
            }
        }

        if (this.business_name) {
            if (validator.isEmpty(this.business_name.trim())) {
                throw badRequestError('Please add Business name.');
            }

            if (!(/^[a-zA-Z0-9 ]*$/).test(this.business_name)) {
                throw badRequestError('Only alphabet, numbers and space are allowd.');
            }

            let result = await this.constructor.query().skipUndefined().select('business_id').where('business_name', this.business_name).first();
            if (result) {
                throw badRequestError("Business with this name already exists!");
            }
        }
        this.created_at = new Date().toISOString();
        this.status = "active";
    }
    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);

        if (this.email_id) {
            if (!validator.isEmail(this.email_id || '')) {

                throw badRequestError("Not a valid email address!");
            }

            //console.log(this.userTypeId)
            let result = await this.constructor.query().skipUndefined().select('business_id').where('email_id', this.email_id).whereNot('business_id', this.business_id).first();


            if (result) {

                throw badRequestError("Business with this email already exists!");
            }

        }
        if (this.mobile_number) {

            let mobile = this.mobile_number.split('-');
            if (!validator.isNumeric(mobile[1])) {
                throw badRequestError("Please enter valid mobile number!");
            }

            // if (!validator.isLength(this.mobile_number, {
            //         min: 10,
            //         max: 10
            //     })) {

            //     throw badRequestError('Please enter valid mobile number!');

            // }

            let result = await this.constructor.query().skipUndefined().select('business_id').where('mobile_number', this.mobile_number).whereNot('business_id', this.business_id).first();
            if (result) {
                throw badRequestError("Business with this mobile number already exists!");
            }
        }

        if (this.business_name) {
            if (validator.isEmpty(this.business_name.trim())) {
                throw badRequestError('Please add Business name.');
            }

            if (!(/^[a-zA-Z0-9 ]*$/).test(this.business_name)) {
                throw badRequestError('Only alphabet, numbers and space are allowd.');
            }

            let result = await this.constructor.query().skipUndefined().select('business_id').where('business_name', this.business_name).whereNot('business_id', this.business_id).first();
            if (result) {
                throw badRequestError("Business with this name already exists!");
            }
        }
        this.updated_at = new Date().toISOString();
    }
    // static get jsonSchema() {
    //     return {
    //         type: 'object',
    //         required: ['business_type', 'business_name', 'email_id', 'agreement', 'license'],

    //         properties: {
    //             business_type: {
    //                 type: 'integer'
    //             },
    //             business_name: {
    //                 type: 'string'
    //             },
    //             email_id: {
    //                 type: 'string'
    //             },
    //             agreement: {
    //                 type: 'text'
    //             },
    //             license: {
    //                 type: 'text'
    //             }
    //         }
    //     }
    // }
    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);
        return json
    }
}

module.exports = Business;