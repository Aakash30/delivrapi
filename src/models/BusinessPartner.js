'use strict';

const Model = require('objection').Model;


class BusinessPartner extends Model {

    // return table Name
    static get tableName() {
        return 'business_partner';
    }

    static get idColumn() {
        return 'partner_id';
    }

    static get relationMappings() {
        return {
            branch: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/BusinessBranchArea',
                join: {
                    from: 'business_partner.partner_id',
                    to: 'business_address_area.partner_id'
                }
            },
        }
    }

    async $beforeInsert() {
        await super.$beforeInsert();
        this.created_at = new Date().toISOString();
    }
}

module.exports = BusinessPartner;