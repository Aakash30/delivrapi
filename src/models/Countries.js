'use strict';

const Model = require('objection').Model;

const validator = require('validator');

class Countries extends Model {
    // return table Name
    static get tableName() {
        return 'country_list';
    }

    static get idColumn() {
        return 'country_id';
    }

    static get relationMappings() {
        return {
            cities: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/City',

                join: {
                    from: 'country_list.country_id',
                    to: 'city_list.country_id'
                }
            },
            businessBranch: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + "/BusinessBranchArea",
                join: {
                    from: 'country_list.country_id',
                    to: 'business_address_area.country'
                }
            },
        }
    }

    async $beforeInsert() {
        await super.$beforeInsert();

        if (this.country) {

            if (validator.isEmpty(this.country.trim())) {
                throw badRequestError('Please enter Country name.');
            }

            if (!(/^[a-zA-Z ]*$/).test(this.country)) {
                throw badRequestError('Only alphabet and space are allowd.');
            }

            if (validator.isEmpty(this.currency.trim())) {
                throw badRequestError('Please add Currency.');
            }

            let result = await this.constructor.query().skipUndefined().select('country_id').where('country', this.country).first();
            if (result) {
                throw badRequestError("This country already exists.");
            }

        }
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);

        if (opt.patch) {

        }

        if (queryContext.country_id != undefined) {
            if (validator.isEmpty(this.country.trim())) {
                throw badRequestError('Please enter Country name.');
            }

            if (!(/^[a-zA-Z ]*$/).test(this.country)) {
                throw badRequestError('Only alphabet and space are allowd.');
            }

            if (validator.isEmpty(this.currency.trim())) {
                throw badRequestError('Please add Currency.');
            }

            let result = await this.constructor.query().skipUndefined().select('country_id').where('country', this.country).whereNot("country_id", this.country_id).first();
            if (result) {
                throw badRequestError("This country already exists.");
            }
        }

    }
    $formatJson(json, opt) {
        json = super.$formatJson(json, opt);

        return json
    }
}

module.exports = Countries;