'use strict';

const Model = require('objection').Model;

class RepairRequest extends Model {

    // Table name is the only required property.
    static get tableName() {
        return 'repair_request';
    }

    static get idColumn() {
        return 'id';
    }

    //This object defines the relations to other models.
    static get relationMappings() {
        return {

            requestedService: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/RepairRequestService',
                join: {
                    from: 'repair_request.id',
                    to: 'repair_request_service.request_id'
                }
            },
            driver: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/Users",
                join: {
                    from: 'repair_request.driver_id',
                    to: 'user_login.user_id'
                }
            },
            vehicleData: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/Vehicle",
                join: {
                    from: 'repair_request.vehicle_id',
                    to: 'vehicle.vehicle_id'
                }
            }
        }
    }

    async $beforeInsert(opt, queryContext) {
        await super.$beforeInsert(opt, queryContext);
        this.request_number = "RQ_" + Date.now();
        this.requested_on = new Date().toISOString();
    }

}
module.exports = RepairRequest;