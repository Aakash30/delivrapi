"use strict";
const Model = require("objection").Model;

class FuelUpdate extends Model {
  // return table Name
  static get tableName() {
    return "fuel_updates";
  }

  static get relationMappings() {
    return {
      // batch: {
      //   relation: Model.BelongsToOneRelation,
      //   modelClass: __dirname + "/Batch",

      //   join: {
      //     from: "payments.batch_id",
      //     to: "batch.id"
      //   }
      // }
    };
  }
}

module.exports = FuelUpdate;
