'use strict';

const Model = require('objection').Model;
const validator = require('validator');


class BusinessBranchArea extends Model {

    // return table Name
    static get tableName() {
        return 'business_address_area';
    }

    static get idColumn() {
        return 'branch_area_id';
    }

    static get relationMappings() {
        return {
            business: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/Business",
                join: {
                    from: 'business_address_area.business_id',
                    to: 'business.business_id'
                }
            },
            businessPartner: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + '/BusinessPartner',
                join: {
                    from: 'business_address_area.partner_id',
                    to: 'business_partner.partner_id'
                }
            },
            users: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/Users',
                join: {
                    from: 'business_address_area.branch_area_id',
                    to: 'user_login.business_branch_id'
                }
            },
            countries: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/Countries",
                join: {
                    from: 'business_address_area.country',
                    to: 'country_list.country_id'
                }
            },
            cities: {
                relation: Model.BelongsToOneRelation,
                modelClass: __dirname + "/City",
                join: {
                    from: 'business_address_area.city',
                    to: 'city_list.city_id'
                }
            }
        };
    }
    async $beforeInsert() {
        await super.$beforeInsert();
        this.created_at = new Date().toISOString();
    }

    async $beforeUpdate(opt, queryContext) {
        await super.$beforeUpdate(opt, queryContext);
        this.updated_at = new Date().toISOString();
    }
}

module.exports = BusinessBranchArea;