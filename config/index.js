require('dotenv').config();
require('./../src/global_constants').CONFIG;

// CONFIG.db_host = process.env.DB_HOST || 'localhost';
// CONFIG.db_name = process.env.DB_NAME || 'delivr';
// CONFIG.db_user = process.env.DB_USER || 'delivr';
// CONFIG.db_password = process.env.DB_PASSWORD || 'delivr';

CONFIG.db_host = process.env.DB_HOST || 'delivr.c7r14n3c5mkl.ap-south-1.rds.amazonaws.com';
CONFIG.db_name = process.env.DB_NAME || 'delivrebdb';
CONFIG.db_user = process.env.DB_USER || 'delivrebuser';
CONFIG.db_password = process.env.DB_PASSWORD || 'Year#2015';

// CONFIG.db_host = process.env.DB_HOST || 'delivr-production.c7r14n3c5mkl.ap-south-1.rds.amazonaws.com';
// CONFIG.db_name = process.env.DB_NAME || 'delivr_production';
// CONFIG.db_user = process.env.DB_USER || 'delivr_user';
// CONFIG.db_password = process.env.DB_PASSWORD || '6ES7EFHaF-V9';

const sendgrid = '';

module.exports = {
    development: {
        username: CONFIG.db_user,
        password: CONFIG.db_password,
        database: CONFIG.db_name,
        host: CONFIG.db_host,
        dialect: CONFIG.db_dialect
    },
    test: {
        username: CONFIG.db_user,
        password: CONFIG.db_password,
        database: CONFIG.db_name,
        host: CONFIG.db_host,
        dialect: CONFIG.db_dialect
    },
    production: {
        username: CONFIG.db_user,
        password: CONFIG.db_password,
        database: CONFIG.db_name,
        host: CONFIG.db_host,
        dialect: CONFIG.db_dialect
    },
    sendgrid: sendgrid,
    team_id: "H6P5YXD7HN",
    bundleIdentifier: "com.horsepowerllc.app",
    apn_key_id: "T5Y89399M5"
}